package ca.hellochat.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//import ca.hellochat.entity.TracecodeInfo;
import ca.hellochat.entity.domain.Page;

/**
 * 用户模块的测试
 */

@RunWith(SpringRunner.class)
@SpringBootTest
//@Transactional //支持事物，@SpringBootTest 事物默认自动回滚
//@Rollback // 事务自动回滚，不自动回滚@Rollback(false)
public class UserBusinessTest {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	//注入RabbitMQ的模板
    @Autowired
    private RabbitTemplate rabbitTemplate;
	
//	@Autowired
//	IUserBusiness userBusiness;
//	@Autowired
//	IDeptBusiness deptBusiness;
	
//	@Autowired
//	IDaySumBusiness daySumBusiness;
//	@Autowired
//	IRecycleDetailBusiness recycleDetailBusiness;
//	@Autowired
//	IRecycleInfoBusiness recycleInfoBusiness;
//	@Autowired
//	ISalecompanySumBusiness salecompanySumBusiness;
//	@Autowired
//	ISaleDetailBusiness saleDetailBusiness;
//	@Autowired
//	ISaleInfoBusiness saleInfoBusiness;
//	@Autowired
//	ITracecodeInfoBusiness tracecodeInfoBusiness;
//	
//	@Autowired
//	RedisCache redisCache;

    /**
     * 测试根据id查找用户
     * @throws Exception
     */
    @Test
    public void testInfo(){
    	log.info("开始执行...............................................");
    	
//    	for (int i = 0; i < 100; i++) {
//    		//从数据库查找二维码
//        	Page page=  new Page(1,10);
//        	Map<String,String> param = new HashMap<String,String>();
//        	List<TracecodeInfo> tiList = tracecodeInfoBusiness.selectPageByParam(param, page);
//        	for (TracecodeInfo ti : tiList) {
//				System.out.println();
//			}
//		}
//    	
    	
    	
    	log.info("模拟销售数据");
    	
    }
    
    /**
     * 测试根据id查找用户
     * @throws Exception
     */
//    @Test
    public void testadd() throws Exception{
//    	log.info("-----------------");
//    	for (int i = 0; i < 100000; i++) {
//    		User user = new User();
//        	user.setAge(new Random().nextInt(100));
//        	user.setUserName(StringUtil.getRandomStr(new Random().nextInt(5)+3,0));
//        	user.setNickName(StringUtil.getRandomStr(new Random().nextInt(5)+3,0));
//        	user.setUserPwd(StringUtil.getRandomStr(new Random().nextInt(5)+3,0));
//        	user.setSeq(new Random().nextInt(1000));
//        	user.setSex(new Random().nextInt(1));
//        	user.setState(new Random().nextInt(1));
//        	user.setDeptId(new Random().nextInt(100000));
//        	user.setRemark(StringUtil.getRandomStr(new Random().nextInt(10000)+300,0));
//        	userBusiness.insertSelective(user);
//		}
//    	
    }
    
    /**
     * 测试根据id查找用户
     * @throws Exception
     */
    @Test
    public void testDept() throws Exception{
//    	log.info("-----------------");
//    	for (int i = 0; i < 10000; i++) {
//    		Dept dept = new Dept();
//    		dept.setDeptNo(StringUtil.getRandomStr(6,1));
//    		dept.setPdeptNo(StringUtil.getRandomStr(3,0));
//    		dept.setSeq(new Random().nextInt(1000));
//    		dept.setState(new Random().nextInt(2));
//    		dept.setTitle(StringUtil.getRandomStr(new Random().nextInt(10)+10,0));
//    		deptBusiness.insertSelective(dept);
//		}
//    	
    }
}
