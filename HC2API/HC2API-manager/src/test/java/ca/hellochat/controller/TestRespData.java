package ca.hellochat.controller;
/**
 * 统一返回数据载体
 * @author yl
 * 2019-07-05
 */
public class TestRespData {
	/** 错误码. */
    private String resCode;
    /** 提示信息. */
    private String resMsg;

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	@Override
	public String toString() {
		return "RespData [" + (resCode != null ? "resCode=" + resCode + ", " : "")
				+ (resMsg != null ? "resMsg=" + resMsg + ", " : "")
				+ "]";
	}

	
	
}
