package ca.hellochat.business.impl;
import ca.hellochat.entity.ChatNo;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import ca.hellochat.entity.domain.Page;
import javax.annotation.Resource;
import ca.hellochat.business.IChatNoBusiness;
import ca.hellochat.service.IChatNoService;
/**
 * 
 * 业务实现
 * 
 **/
@Component
public class ChatNoBusiness implements IChatNoBusiness{
	@Autowired
	private IChatNoService chatNoService;

	/** 查询（根据主键查询）**/
	public ChatNo selectById(String id){
		return chatNoService.selectById(id);
	}
	public ChatNo selectByParam(Map<String,String> param) {
		return chatNoService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<ChatNo>  selectSimpleListByParam(Map<String,String> param){
		List<ChatNo> chatNoList = chatNoService.selectListByParam(param);
		return chatNoList;
	}

	/** 查询（根据条件查询）**/
	public List<ChatNo>  selectListByParam(Map<String,String> param){
		List<ChatNo> chatNoList = selectSimpleListByParam(param);
		return chatNoList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<ChatNo> selectSimplePageByParam(Map<String,String> param,Page page){
		List<ChatNo> chatNoList = chatNoService.selectPageByParam(param, page);
		page.setRows(chatNoList);
		return chatNoList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<ChatNo> selectPageByParam(Map<String,String> param,Page page){
		List<ChatNo> chatNoList = selectSimplePageByParam(param, page);
		page.setRows(chatNoList);
		return chatNoList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return chatNoService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return chatNoService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return chatNoService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatNo chatNo){
		return chatNoService.insertSelective(chatNo);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatNo chatNo){
		return chatNoService.updateById(chatNo);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(ChatNo chatNo,Map<String,String> param){
		return chatNoService.updateByParam(chatNo,param);
	}

}