package ca.hellochat.business;
import ca.hellochat.entity.ChatGroup;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * ChatGroup业务实现接口
 * 
 **/
public interface IChatGroupBusiness{

	/** 查询（根据主键查询）**/
	public ChatGroup selectById(String id);

	/** 根据条件获取实体**/
	public ChatGroup selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<ChatGroup>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<ChatGroup>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<ChatGroup>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<ChatGroup>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatGroup chatGroup);

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatGroup chatGroup);

	/** 根据条件修改**/
	public int updateByParam(ChatGroup chatGroup,Map<String,String> param);

}