package ca.hellochat.business;
import ca.hellochat.entity.ChatNo;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * ChatNo业务实现接口
 * 
 **/
public interface IChatNoBusiness{

	/** 查询（根据主键查询）**/
	public ChatNo selectById(String id);

	/** 根据条件获取实体**/
	public ChatNo selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<ChatNo>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<ChatNo>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<ChatNo>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<ChatNo>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatNo chatNo);

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatNo chatNo);

	/** 根据条件修改**/
	public int updateByParam(ChatNo chatNo,Map<String,String> param);

}