package ca.hellochat.business.impl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ca.hellochat.business.IVcodeBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.entity.Vcode;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IVcodeService;
/**
 * 
 * 验证码信息业务实现
 * 
 **/
@Component
public class VcodeBusiness implements IVcodeBusiness{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private IVcodeService vcodeService;
	
	//range天的时间范围内最多允许发几次
	@Value("${helloChat.validatMobile.sendNum}")
	Integer sendNum = 4;
	//rangeTime秒的时间范围内做多允许发送sendNum次
	@Value("${helloChat.validatMobile.rangeTime}")
	Integer rangeTime = 30 * 50;

	/** 查询（根据主键查询）**/
	public Vcode selectById(String id){
		return vcodeService.selectById(id);
	}
	public Vcode selectByParam(Map<String,String> param) {
		return vcodeService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<Vcode>  selectSimpleListByParam(Map<String,String> param){
		List<Vcode> vcodeList = vcodeService.selectListByParam(param);
		return vcodeList;
	}

	/** 查询（根据条件查询）**/
	public List<Vcode>  selectListByParam(Map<String,String> param){
		List<Vcode> vcodeList = selectSimpleListByParam(param);
		return vcodeList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Vcode> selectSimplePageByParam(Map<String,String> param,Page page){
		List<Vcode> vcodeList = vcodeService.selectPageByParam(param, page);
		page.setRows(vcodeList);
		return vcodeList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Vcode> selectPageByParam(Map<String,String> param,Page page){
		List<Vcode> vcodeList = selectSimplePageByParam(param, page);
		page.setRows(vcodeList);
		return vcodeList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return vcodeService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return vcodeService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return vcodeService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Vcode vcode){
		return vcodeService.insertSelective(vcode);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(Vcode vcode){
		return vcodeService.updateById(vcode);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(Vcode vcode,Map<String,String> param){
		return vcodeService.updateByParam(vcode,param);
	}
	
	
	/** 添加（匹配有值的字段）**/
	public Vcode sendCode(String mobile,Integer vtype){
		log.info("验证手机号发送，rangeTime="+rangeTime+",sendNum="+sendNum);
		//计算时间：获取当前时间
		Date nowTime = DateUtil.getNowDate();
		//开始时间
		String endTime = DateUtil.formatDate(nowTime);
		//结束时间
		String startTime = DateUtil.formatDate(DateUtil.addSecond(nowTime, 0-rangeTime));
		Map<String,String> params = new HashMap<String,String>();
		params.put("mobile", mobile);
		params.put("le_sendTime",endTime);
		params.put("ge_sendTime",startTime);
		params.put("sts","0");
		int sendCount = vcodeService.selectCountByParam(params);
		log.info("从数据库中获取到的验证码发送次数为："+sendCount);
		if(sendCount>sendNum) {
			throw new IChatException(ExceptionType.UNMATCHED_RESPONSE,"验证码发送次数过多");
		}
		
		String vcodestr = StringUtil.getRandStr(4);
		Vcode vcode = new Vcode();
		vcode.setVcode(vcodestr);
		vcode.setMobile(mobile);
		vcode.setVtype(vtype);
		vcode.setSts(0);
		vcode.setSendTime(DateUtil.getNowDate());
		vcode.setEndTime(DateUtil.addMinute(nowTime, 5));//当前时间加5分钟
		vcodeService.insertSelective(vcode);
		return vcode;
	}
	
	/** 添加（匹配有值的字段）**/
	public boolean validateCode(String mobile,Integer vtype,String vcode){
		//计算时间：获取当前时间
		Date nowTime = DateUtil.getQueryDate();
		//开始时间
		String startTime = DateUtil.formatDate(nowTime);
		//结束时间
		String endTime = DateUtil.formatDate(DateUtil.addSecond(nowTime, rangeTime));
		Map<String,String> params = new HashMap<String,String>();
		params.put("mobile", mobile);
		params.put("le_endTime",endTime);
		params.put("ge_endTime",startTime);
		params.put("sts","0");
		params.put("vtype",vtype+"");
		params.put("orderBy","sendTime desc");
		List<Vcode> vcodeList = vcodeService.selectListByParam(params);
		if(vcodeList.size()<=0) {
			log.info("验证码不匹配：没有找到数据库记录");
			throw new IChatException(ExceptionType.UNMATCHED_RESPONSE,"验证码不匹配");
		}
		Vcode vcodeEntity = vcodeList.get(0);
		if(vcodeEntity==null || !vcodeEntity.getVcode().equals(vcode)) {
			log.info("验证码不匹配：验证码验证失败：vcodeEntity="+vcodeEntity+";vcode="+vcode);
			throw new IChatException(ExceptionType.UNMATCHED_RESPONSE,"验证码不匹配");
		}
		
		for (Vcode vcodeDb : vcodeList) {
			vcodeDb.setSts(1);
			vcodeDb.setVtime(DateUtil.getNowDate());
			vcodeService.updateById(vcodeDb);
		}
		
		//如果验证成功了、将他之前的验证码验证状态都置为1
//		params.clear();
//		params.put("mobile", mobile);
//		params.put("sts","0");
//		params.put("le_endTime", DateUtil.formatDate(nowTime));
//		vcodeService.deleteByParam(params);
		
		return true;
	}
	
	

}