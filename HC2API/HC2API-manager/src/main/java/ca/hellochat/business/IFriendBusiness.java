package ca.hellochat.business;
import ca.hellochat.entity.Friend;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * Friend业务实现接口
 * 
 **/
public interface IFriendBusiness{

	/** 查询（根据主键查询）**/
	public Friend selectById(String id);
	public  int delFriend(Friend fi);

	/** 根据条件获取实体**/
	public Friend selectByParam(Map<String,String> param);
	
	/** 查询（根据主键查询）**/
	public Friend selectSimpleById(String id);

	/** 根据条件获取实体**/
	public Friend selectSimpleByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<Friend>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<Friend>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<Friend>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<Friend>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Friend friend);

	/** 修改（根据主键ID修改）**/
	public int updateById(Friend friend);

	/** 根据条件修改**/
	public int updateByParam(Friend friend,Map<String,String> param);
	
	public boolean isFriend(String userId,String friendId);

}