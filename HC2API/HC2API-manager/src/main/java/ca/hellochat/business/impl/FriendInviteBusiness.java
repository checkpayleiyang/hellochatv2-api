package ca.hellochat.business.impl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.hellochat.business.IFriendBusiness;
import ca.hellochat.business.IFriendInviteBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.Friend;
import ca.hellochat.entity.FriendInvite;
import ca.hellochat.entity.UserMsg;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IFriendInviteService;
import ca.hellochat.service.IFriendService;
import ca.hellochat.service.IUserMsgService;
import ca.hellochat.service.IUserService;
/**
 * 
 * 新朋友信息业务实现
 * 
 **/
@Component
public class FriendInviteBusiness implements IFriendInviteBusiness{
	
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IFriendInviteService friendInviteService;
//	@Autowired
//	private IFriendService friendService;
	@Autowired
	private IFriendBusiness friendBusiness;
	@Autowired
	private IUserMsgService userMsgService;
	
	@Autowired
	private IUserService userService;

	/** 查询（根据主键查询）**/
	public FriendInvite selectById(String id){
		FriendInvite fi =  this.selectSimpleById(id);
		packageEntity(fi);
		return fi;
	}
	public FriendInvite selectByParam(Map<String,String> param) {
		FriendInvite fi = this.selectSimpleByParam(param);
		packageEntity(fi);
		return fi;
	}
	/** 查询（根据主键查询）**/
	public FriendInvite selectSimpleById(String id){
		return friendInviteService.selectById(id);
	}
	public FriendInvite selectSimpleByParam(Map<String,String> param) {
		return friendInviteService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<FriendInvite>  selectSimpleListByParam(Map<String,String> param){
		List<FriendInvite> friendInviteList = friendInviteService.selectListByParam(param);
		return friendInviteList;
	}

	/** 查询（根据条件查询）**/
	public List<FriendInvite>  selectListByParam(Map<String,String> param){
		List<FriendInvite> friendInviteList = selectSimpleListByParam(param);
		if(friendInviteList !=null && friendInviteList.size()>0) {
			for (FriendInvite fi : friendInviteList) {
				packageEntity(fi);
			}
		}
		return friendInviteList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<FriendInvite> selectSimplePageByParam(Map<String,String> param,Page page){
		List<FriendInvite> FriendInviteList = friendInviteService.selectPageByParam(param, page);
		page.setRows(FriendInviteList);
		return FriendInviteList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<FriendInvite> selectPageByParam(Map<String,String> param,Page page){
		List<FriendInvite> friendInviteList = selectSimplePageByParam(param, page);
		if(friendInviteList!=null && friendInviteList.size()>0) {
			for (FriendInvite fi : friendInviteList) {
				packageEntity(fi);
			}
		}
		return friendInviteList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return friendInviteService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return friendInviteService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return friendInviteService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(FriendInvite FriendInvite){
		return friendInviteService.insertSelective(FriendInvite);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(FriendInvite FriendInvite){
		return friendInviteService.updateById(FriendInvite);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(FriendInvite FriendInvite,Map<String,String> param){
		return friendInviteService.updateByParam(FriendInvite,param);
	}
	
	/** 修改（根据条件批量修改）**/
	@Transactional
	public void request(FriendInvite fi){
		//两个人已经是否已经是好友
		boolean isfriend = friendBusiness.isFriend(fi.getSendId(), fi.getReceiveId());
		if(isfriend) {
			throw new IChatException(ExceptionType.IM_FRIEND_EXIST,"已经是好友,无需添加");
		}
		Date expiredTime = DateUtil.addDate(DateUtil.getNowDate(),5);
		//判断以前是否发起过,对方不同意的
		Map<String,String> params = new HashMap<String,String>();
//		params.put("sendId",fi.getSendId());
//		params.put("receiveId",fi.getReceiveId());
//		params.put("orderBy","id desc");
//		FriendInvite fiDb = friendInviteService.selectByParam(params);
//		log.info("判断以前是否发起过,对方不同意的::fiDb::"+fiDb);
//		if(fiDb!=null && !StringUtil.isEmpty(fiDb.getId())) {
//			//表示之前有不同意的记录、这时候查询不同意的时间是否超过1天
//			if(DateUtil.diffDate(fiDb.getReqTime(),DateUtil.getNowDate())<1) {
//				//一天内有拒绝过
//				if(fiDb.getSts()==0) {
//					log.info("之前有不同意的记录并且在一天内,服务端返回！！");
//					throw new IChatException(ExceptionType.UNMATCHED_RESPONSE,"用户不存在或者无法添加好友");
//				}
//			}
//		}
		
		//删除以前所有的申请记录
		params.clear();
		params.put("sendId",fi.getSendId());
		params.put("receiveId",fi.getReceiveId());
		friendInviteService.deleteByParam(params);
		
		//发起好友的申请
		fi.setReqTime(DateUtil.getNowDate());
		fi.setExpiredTime(expiredTime);
		fi.setSts(1);
		friendInviteService.insertSelective(fi);
		//像添加者消息中发送一条消息
		
		//删除给此好友发送的所有好友申请
		params.clear();
		params.put("sendId",fi.getSendId());
		params.put("receiveId", fi.getReceiveId());
		params.put("mtype", "2");
		UserMsg us = new UserMsg();
		us.setSts(1);
		us.setShowNum(1);
		userMsgService.updateByParam(us, params);
		
		//添加新的好友记录
		UserMsg um = new UserMsg();
		um.setTitle("好友申请");
		um.setSts(0);
		um.setShowNum(1);
		um.setReceiveId(fi.getReceiveId());
		um.setSendId(fi.getSendId());
		um.setObjId(fi.getId());
		um.setMtype(2);
		um.setCreateTime(DateUtil.getNowDate());
		um.setExpiredTime(expiredTime);
		userMsgService.insertSelective(um);
		return;
	}
	
	/** 修改（根据条件批量修改）**/
	@Transactional
	public void inviteResp(FriendInvite fiUpdate){
		
		//两个人已经是否已经是好友
		boolean isfriend = friendBusiness.isFriend(fiUpdate.getReceiveId(), fiUpdate.getSendId());
		if(isfriend) {
			throw new IChatException(ExceptionType.IM_FRIEND_EXIST,"已经是好友,无需添加");
		}
		
        //处理邀请列表
		friendInviteService.updateById(fiUpdate);
		Friend fuser = new Friend();
		fuser.setSts(1);
		fuser.setUserId(fiUpdate.getSendId());
		fuser.setFriendId(fiUpdate.getReceiveId());
		fuser.setCreateTime(DateUtil.getNowDate());
		friendBusiness.insertSelective(fuser);
		
		
		Friend friend = new Friend();
		friend.setSts(1);
		friend.setFriendId(fiUpdate.getSendId());
		friend.setUserId(fiUpdate.getReceiveId());
		friend.setCreateTime(DateUtil.getNowDate());
		friendBusiness.insertSelective(friend);
	}
	
	public void packageEntity(FriendInvite fi) {
		if(fi==null || StringUtil.isEmpty(fi.getId())) {
			return;
		}
		if(StringUtil.isNotEmpty(fi.getReceiveId())) {
			fi.setReceive(userService.selectById(fi.getReceiveId()));
		}
		if(StringUtil.isNotEmpty(fi.getSendId())) {
			fi.setSend(userService.selectById(fi.getSendId()));
		}
		
	}

}