package ca.hellochat.business.impl;
import ca.hellochat.entity.Log;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import ca.hellochat.entity.domain.Page;
import javax.annotation.Resource;
import ca.hellochat.business.ILogBusiness;
import ca.hellochat.service.ILogService;
/**
 * 
 * 系统日志业务实现
 * 
 **/
@Component
public class LogBusiness implements ILogBusiness{
	@Autowired
	private ILogService logService;

	/** 查询（根据主键查询）**/
	public Log selectById(String id){
		return logService.selectById(id);
	}
	public Log selectByParam(Map<String,String> param) {
		return logService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<Log>  selectSimpleListByParam(Map<String,String> param){
		List<Log> logList = logService.selectListByParam(param);
		return logList;
	}

	/** 查询（根据条件查询）**/
	public List<Log>  selectListByParam(Map<String,String> param){
		List<Log> logList = selectSimpleListByParam(param);
		return logList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Log> selectSimplePageByParam(Map<String,String> param,Page page){
		List<Log> logList = logService.selectPageByParam(param, page);
		page.setRows(logList);
		return logList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Log> selectPageByParam(Map<String,String> param,Page page){
		List<Log> logList = selectSimplePageByParam(param, page);
		page.setRows(logList);
		return logList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return logService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return logService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return logService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Log log){
		return logService.insertSelective(log);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(Log log){
		return logService.updateById(log);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(Log log,Map<String,String> param){
		return logService.updateByParam(log,param);
	}

}