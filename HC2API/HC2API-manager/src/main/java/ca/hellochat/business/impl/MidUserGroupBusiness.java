package ca.hellochat.business.impl;
import ca.hellochat.entity.MidUserGroup;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import ca.hellochat.entity.domain.Page;
import javax.annotation.Resource;
import ca.hellochat.business.IMidUserGroupBusiness;
import ca.hellochat.service.IMidUserGroupService;
/**
 * 
 * 用户群组关系信息业务实现
 * 
 **/
@Component
public class MidUserGroupBusiness implements IMidUserGroupBusiness{
	@Autowired
	private IMidUserGroupService midUserGroupService;

	/** 查询（根据主键查询）**/
	public MidUserGroup selectById(String id){
		return midUserGroupService.selectById(id);
	}
	public MidUserGroup selectByParam(Map<String,String> param) {
		return midUserGroupService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<MidUserGroup>  selectSimpleListByParam(Map<String,String> param){
		List<MidUserGroup> midUserGroupList = midUserGroupService.selectListByParam(param);
		return midUserGroupList;
	}

	/** 查询（根据条件查询）**/
	public List<MidUserGroup>  selectListByParam(Map<String,String> param){
		List<MidUserGroup> midUserGroupList = selectSimpleListByParam(param);
		return midUserGroupList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<MidUserGroup> selectSimplePageByParam(Map<String,String> param,Page page){
		List<MidUserGroup> midUserGroupList = midUserGroupService.selectPageByParam(param, page);
		page.setRows(midUserGroupList);
		return midUserGroupList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<MidUserGroup> selectPageByParam(Map<String,String> param,Page page){
		List<MidUserGroup> midUserGroupList = selectSimplePageByParam(param, page);
		page.setRows(midUserGroupList);
		return midUserGroupList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return midUserGroupService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return midUserGroupService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return midUserGroupService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(MidUserGroup midUserGroup){
		return midUserGroupService.insertSelective(midUserGroup);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(MidUserGroup midUserGroup){
		return midUserGroupService.updateById(midUserGroup);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(MidUserGroup midUserGroup,Map<String,String> param){
		return midUserGroupService.updateByParam(midUserGroup,param);
	}

}