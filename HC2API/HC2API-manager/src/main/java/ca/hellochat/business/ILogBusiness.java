package ca.hellochat.business;
import ca.hellochat.entity.Log;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * Log业务实现接口
 * 
 **/
public interface ILogBusiness{

	/** 查询（根据主键查询）**/
	public Log selectById(String id);

	/** 根据条件获取实体**/
	public Log selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<Log>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<Log>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<Log>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<Log>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Log log);

	/** 修改（根据主键ID修改）**/
	public int updateById(Log log);

	/** 根据条件修改**/
	public int updateByParam(Log log,Map<String,String> param);

}