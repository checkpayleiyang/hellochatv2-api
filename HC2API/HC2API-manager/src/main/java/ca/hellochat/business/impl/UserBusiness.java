package ca.hellochat.business.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.hellochat.business.IUserBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.rongCloud.RCUtil;
import ca.hellochat.entity.ChatNo;
import ca.hellochat.entity.User;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IChatNoService;
import ca.hellochat.service.IUserService;
/**
 * 
 * 用户信息业务实现
 * 
 **/
@Component
public class UserBusiness implements IUserBusiness{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private IChatNoService chatNoService;
	@Autowired
	private IUserService userService;

	/** 查询（根据主键查询）**/
	public User selectById(String id){
		User user = selectSimpleById(id);
		packageEntity(user);
		return user;
	}
	public User selectByParam(Map<String,String> param) {
		User user = selectSimpleByParam(param);
		packageEntity(user);
		return user;
	}
	
	/** 查询（根据主键查询）**/
	public User selectSimpleById(String id){
		return userService.selectById(id);
	}
	public User selectSimpleByParam(Map<String,String> param) {
		return userService.selectByParam(param);
	}
	/** 查询（根据条件查询）**/
	public List<User>  selectSimpleListByParam(Map<String,String> param){
		List<User> userList = userService.selectListByParam(param);
		if(userList!=null &&userList.size()>0) {
			for (User user : userList) {
				packageEntity(user);
			}
		}
		return userList;
	}

	/** 查询（根据条件查询）**/
	public List<User>  selectListByParam(Map<String,String> param){
		List<User> userList = selectSimpleListByParam(param);
		return userList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<User> selectSimplePageByParam(Map<String,String> param,Page page){
		List<User> userList = userService.selectPageByParam(param, page);
		page.setRows(userList);
		return userList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<User> selectPageByParam(Map<String,String> param,Page page){
		List<User> userList = selectSimplePageByParam(param, page);
		if(userList!=null &&userList.size()>0) {
			for (User user : userList) {
				packageEntity(user);
			}
		}
		page.setRows(userList);
		return userList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return userService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return userService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return userService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(User user){
		return userService.insertSelective(user);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(User user){
		return userService.updateById(user);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(User user,Map<String,String> param){
		return userService.updateByParam(user,param);
	}
	
	public void packageEntity(User user){
		if(user==null) {
			return;
		}
//		if(user.getCustomerId()!=null) {
//			Customer customer = customerService.selectById(user.getCustomerId());
//			user.setCustomer(customer);
//		}
	}
	public User findUserByMobile(String mobile) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("sts", "1");
		params.put("mobile", mobile);
		return this.selectByParam(params);
	}
	public User findUserByChatNo(String chatNo) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("sts", "1");
		params.put("chno", chatNo);
		return this.selectByParam(params);
	}
	
	public int refreshRongCloudToken(User user) {
		//在融云中添加user
		//已经验证过了，重复获取token信息也是可以的
		log.debug("在融云中注册IM用户信息");
		RCUtil rcUtil =  new RCUtil();
		boolean flag = rcUtil.register(user);
		log.debug("注册IM用户结果flag:["+flag+"]：user:"+user);
		userService.updateById(user);
		return 1;
	}
	
	@Transactional
	public User registByMobile(String mobile) {
		String chatNoStr = "";
		synchronized(User.class) {
			Map<String,String> params =new HashMap<String,String>();
			params.put("sts", "0");
			params.put("orderBy", "id asc");
			ChatNo chatNo = chatNoService.selectByParam(params);
			if(chatNo==null || StringUtil.isEmpty(chatNo.getId())) {
				 throw new IChatException(ExceptionType.FAILED_RESPONSE,"服务器已无可用的HelloChatNo资源");
			}
			chatNoStr = chatNo.getChatNo();
			chatNo.setSts(1);
			chatNoService.updateById(chatNo);
		}
		//注册用户
		//创建用户
		User user = new User();
		user.setMobile(mobile);
		user.setHcno(chatNoStr);
		user.setUname(chatNoStr);
		user.setSts(1);
		user.setCreateTime(DateUtil.getNowDate());
		userService.insertSelective(user);
		//重新获取一次user
		User userDb = this.selectById(user.getId());
		return userDb;
	}
}