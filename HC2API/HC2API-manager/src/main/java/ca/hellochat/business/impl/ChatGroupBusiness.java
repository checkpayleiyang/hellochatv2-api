package ca.hellochat.business.impl;
import ca.hellochat.entity.ChatGroup;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import ca.hellochat.entity.domain.Page;
import javax.annotation.Resource;
import ca.hellochat.business.IChatGroupBusiness;
import ca.hellochat.service.IChatGroupService;
/**
 * 
 * 群组信息业务实现
 * 
 **/
@Component
public class ChatGroupBusiness implements IChatGroupBusiness{
	@Autowired
	private IChatGroupService chatGroupService;

	/** 查询（根据主键查询）**/
	public ChatGroup selectById(String id){
		return chatGroupService.selectById(id);
	}
	public ChatGroup selectByParam(Map<String,String> param) {
		return chatGroupService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<ChatGroup>  selectSimpleListByParam(Map<String,String> param){
		List<ChatGroup> chatGroupList = chatGroupService.selectListByParam(param);
		return chatGroupList;
	}

	/** 查询（根据条件查询）**/
	public List<ChatGroup>  selectListByParam(Map<String,String> param){
		List<ChatGroup> chatGroupList = selectSimpleListByParam(param);
		return chatGroupList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<ChatGroup> selectSimplePageByParam(Map<String,String> param,Page page){
		List<ChatGroup> chatGroupList = chatGroupService.selectPageByParam(param, page);
		page.setRows(chatGroupList);
		return chatGroupList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<ChatGroup> selectPageByParam(Map<String,String> param,Page page){
		List<ChatGroup> chatGroupList = selectSimplePageByParam(param, page);
		page.setRows(chatGroupList);
		return chatGroupList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return chatGroupService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return chatGroupService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return chatGroupService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatGroup chatGroup){
		return chatGroupService.insertSelective(chatGroup);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatGroup chatGroup){
		return chatGroupService.updateById(chatGroup);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(ChatGroup chatGroup,Map<String,String> param){
		return chatGroupService.updateByParam(chatGroup,param);
	}

}