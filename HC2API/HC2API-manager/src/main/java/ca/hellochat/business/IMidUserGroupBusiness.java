package ca.hellochat.business;
import ca.hellochat.entity.MidUserGroup;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * MidUserGroup业务实现接口
 * 
 **/
public interface IMidUserGroupBusiness{

	/** 查询（根据主键查询）**/
	public MidUserGroup selectById(String id);

	/** 根据条件获取实体**/
	public MidUserGroup selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<MidUserGroup>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<MidUserGroup>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<MidUserGroup>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<MidUserGroup>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(MidUserGroup midUserGroup);

	/** 修改（根据主键ID修改）**/
	public int updateById(MidUserGroup record);

	/** 根据条件修改**/
	public int updateByParam(MidUserGroup midUserGroup,Map<String,String> param);

}