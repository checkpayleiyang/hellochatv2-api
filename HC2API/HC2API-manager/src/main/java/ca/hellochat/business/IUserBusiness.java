package ca.hellochat.business;
import ca.hellochat.entity.User;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * User业务实现接口
 * 
 **/
public interface IUserBusiness{

	/** 查询（根据主键查询）**/
	public User selectById(String id);

	/** 根据条件获取实体**/
	public User selectByParam(Map<String,String> param);
	
	/** 查询（根据主键查询）**/
	public User selectSimpleById(String id);

	/** 根据条件获取实体**/
	public User selectSimpleByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<User>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<User>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<User>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<User>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(User user);

	/** 修改（根据主键ID修改）**/
	public int updateById(User user);

	/** 根据条件修改**/
	public int updateByParam(User user,Map<String,String> param);
	
	public User findUserByMobile(String mobile);
	public User findUserByChatNo(String chatNo);
	
	public int refreshRongCloudToken(User user);
	
	public User registByMobile(String mobile);

}