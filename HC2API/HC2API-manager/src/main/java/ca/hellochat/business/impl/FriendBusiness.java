package ca.hellochat.business.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.hellochat.business.IFriendBusiness;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.entity.Friend;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IFriendService;
import ca.hellochat.service.IUserService;
/**
 * 
 * 通讯录信息业务实现
 * 
 **/
@Component
public class FriendBusiness implements IFriendBusiness{
	@Autowired
	private IFriendService friendService;
	
	@Autowired
	private IUserService userService;

	/** 查询（根据主键查询）**/
	public Friend selectById(String id){
		Friend fi = this.selectSimpleById(id);
		packageEntity(fi);
		return fi;
	}
	
	public  int delFriend(Friend fi){
		Friend fiuser = findFriendByIds(fi.getFriendId(),fi.getUserId());
		if(fiuser !=null && StringUtil.isEmpty(fiuser.getId())) {
			fiuser.setSts(0);
			updateById(fiuser);
		}
	    deleteById(fi.getId());
		return 1;
	}
	
	public Friend findFriendByIds(String userId,String friendId) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("userId", userId);
		params.put("friendId", friendId);
		Friend fi = selectByParam(params);
		if(fi==null || StringUtil.isEmpty(fi.getId())) {
			throw new IChatException(ExceptionType.IM_FRIEND_NOTFOUD, "服务器响应异常");
		}
		return fi;
	}
	public Friend selectByParam(Map<String,String> param) {
		Friend fi = this.selectSimpleByParam(param);
		packageEntity(fi);
		return fi;
	}
	
	/** 查询（根据主键查询）**/
	public Friend selectSimpleById(String id){
		return friendService.selectById(id);
	}
	public Friend selectSimpleByParam(Map<String,String> param) {
		return friendService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<Friend>  selectSimpleListByParam(Map<String,String> param){
		List<Friend> friendList = friendService.selectListByParam(param);
		return friendList;
	}

	/** 查询（根据条件查询）**/
	public List<Friend>  selectListByParam(Map<String,String> param){
		List<Friend> friendList = selectSimpleListByParam(param);
		if(friendList!=null && friendList.size()>0) {
			for (Friend fi : friendList) {
				packageEntity(fi);
			}
		}
		return friendList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Friend> selectSimplePageByParam(Map<String,String> param,Page page){
		List<Friend> friendList = friendService.selectPageByParam(param, page);
		page.setRows(friendList);
		return friendList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<Friend> selectPageByParam(Map<String,String> param,Page page){
		List<Friend> friendList = selectSimplePageByParam(param, page);
		if(friendList!=null && friendList.size()>0) {
			for (Friend fi : friendList) {
				packageEntity(fi);
			}
		}
		return friendList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return friendService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return friendService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return friendService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Friend friend){
		return friendService.insertSelective(friend);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(Friend friend){
		return friendService.updateById(friend);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(Friend friend,Map<String,String> param){
		return friendService.updateByParam(friend,param);
	}
	
	public void packageEntity(Friend friend){
		if(friend==null || StringUtil.isEmpty(friend.getId())) {
			return;
		}
		if(StringUtil.isNotEmpty(friend.getFriendId())) {
			friend.setFriend(userService.selectById(friend.getFriendId()));
		}
		
	}
	
	public boolean isFriend(String userId,String friendId) {
		Map<String,String> params = new HashMap<String,String>();
		params.put("userId",userId);
		params.put("friendId",friendId);
		int count = friendService.selectCountByParam(params);
		return count>0;
	}
	

}