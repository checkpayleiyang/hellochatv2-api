package ca.hellochat.business;
import ca.hellochat.entity.Vcode;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * Vcode业务实现接口
 * 
 **/
public interface IVcodeBusiness{

	/** 查询（根据主键查询）**/
	public Vcode selectById(String id);

	/** 根据条件获取实体**/
	public Vcode selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<Vcode>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<Vcode>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<Vcode>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<Vcode>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Vcode vcode);

	/** 修改（根据主键ID修改）**/
	public int updateById(Vcode vcode);

	/** 根据条件修改**/
	public int updateByParam(Vcode vcode,Map<String,String> param);
	
	public Vcode sendCode(String mobile,Integer vtype);
	
	public boolean validateCode(String mobile,Integer vtype,String vcode);

}