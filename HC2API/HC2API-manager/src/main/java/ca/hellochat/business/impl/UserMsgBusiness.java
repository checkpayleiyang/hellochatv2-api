package ca.hellochat.business.impl;
import ca.hellochat.entity.UserMsg;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import ca.hellochat.entity.domain.Page;
import javax.annotation.Resource;
import ca.hellochat.business.IUserMsgBusiness;
import ca.hellochat.service.IUserMsgService;
/**
 * 
 * 用户消息表业务实现
 * 
 **/
@Component
public class UserMsgBusiness implements IUserMsgBusiness{
	@Autowired
	private IUserMsgService userMsgService;

	/** 查询（根据主键查询）**/
	public UserMsg selectById(String id){
		return userMsgService.selectById(id);
	}
	public UserMsg selectByParam(Map<String,String> param) {
		return userMsgService.selectByParam(param);
	}

	/** 查询（根据条件查询）**/
	public List<UserMsg>  selectSimpleListByParam(Map<String,String> param){
		List<UserMsg> userMsgList = userMsgService.selectListByParam(param);
		return userMsgList;
	}

	/** 查询（根据条件查询）**/
	public List<UserMsg>  selectListByParam(Map<String,String> param){
		List<UserMsg> userMsgList = selectSimpleListByParam(param);
		return userMsgList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<UserMsg> selectSimplePageByParam(Map<String,String> param,Page page){
		List<UserMsg> userMsgList = userMsgService.selectPageByParam(param, page);
		page.setRows(userMsgList);
		return userMsgList;
	}

	/** 查询（根据条件查询 分页）**/
	public List<UserMsg> selectPageByParam(Map<String,String> param,Page page){
		List<UserMsg> userMsgList = selectSimplePageByParam(param, page);
		page.setRows(userMsgList);
		return userMsgList;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return userMsgService.selectCountByParam(param);
	}

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id){
		return userMsgService.deleteById(id);
	}

	/** 批量删除（根据条件批量删除），先查询后单条删除**/
	public int deleteByParam(Map<String,String> param){
		return userMsgService.deleteByParam(param);
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(UserMsg userMsg){
		return userMsgService.insertSelective(userMsg);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(UserMsg userMsg){
		return userMsgService.updateById(userMsg);
	}

	/** 修改（根据条件批量修改）**/
	public int updateByParam(UserMsg userMsg,Map<String,String> param){
		return userMsgService.updateByParam(userMsg,param);
	}

}