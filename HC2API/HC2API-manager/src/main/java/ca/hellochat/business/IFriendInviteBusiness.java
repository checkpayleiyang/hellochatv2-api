package ca.hellochat.business;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.FriendInvite;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * FriendInvite业务实现接口
 * 
 **/
public interface IFriendInviteBusiness{

	/** 查询（根据主键查询）**/
	public FriendInvite selectById(String id);

	/** 根据条件获取实体**/
	public FriendInvite selectByParam(Map<String,String> param);

	/** 根据条件简单查询**/
	public List<FriendInvite>  selectSimpleListByParam(Map<String,String> param);

	/** 根据条件查询**/
	public List<FriendInvite>  selectListByParam(Map<String,String> param);

	/** 分页查询（根据条件简单查询）**/
	public List<FriendInvite>  selectSimplePageByParam(Map<String,String> param,Page page);

	/** 分页查询（根据条件查询）**/
	public List<FriendInvite>  selectPageByParam(Map<String,String> param,Page page);

	/** 总数查询（根据条件查询）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件批量删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(FriendInvite FriendInvite);

	/** 修改（根据主键ID修改）**/
	public int updateById(FriendInvite FriendInvite);

	/** 根据条件修改**/
	public int updateByParam(FriendInvite FriendInvite,Map<String,String> param);
	public void request(FriendInvite fi);
	
	public void inviteResp(FriendInvite fiUpdate);

}