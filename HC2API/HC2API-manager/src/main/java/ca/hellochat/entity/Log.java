package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 
 * 系统日志
 * 
 **/
@Data
@SuppressWarnings("serial")
public class Log implements Serializable {
	private String id;//id
	private Integer ltype;//操作类型
	private String title;//日志标题
	private String objId;//操作的具体某一个用户id、或者某一个表id
	private Integer ctype;//用户类型1表示用户2表示商户
	private String coreData;//关键数据
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//创建时间
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date optTime;//日志发生时间
}
