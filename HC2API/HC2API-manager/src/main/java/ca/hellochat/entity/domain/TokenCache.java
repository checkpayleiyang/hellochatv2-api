package ca.hellochat.entity.domain;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

public class TokenCache {
	private TokenCache(){}
	public static Map<String,Long> tokenMap = new HashMap<String,Long>(); 
	
	/**对外暴露唯一接口  提供单例对象*/
    public static Map<String,Long> geteSingleton(){
        return tokenMap;
    }
    
    @Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}
}
