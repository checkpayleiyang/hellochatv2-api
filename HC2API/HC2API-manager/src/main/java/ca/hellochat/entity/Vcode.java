package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 
 * 验证码信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class Vcode implements Serializable {
	private String id;//主键
	private String mobile;//手机号
	private Date sendTime;//发送时间
	private Integer vtype;//1注册用户2用户登录3忘记密码
	private String vcode;//验证码
	private Date endTime;//结束时间
	private Integer sts;//状态0未检验1已校验
	private Date vtime;//验证时间
	private String objId;//用户id
	private Integer utype;//1hellochat用户 2 商户
}
