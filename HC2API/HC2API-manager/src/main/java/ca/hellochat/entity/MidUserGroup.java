package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 
 * 用户群组关系信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class MidUserGroup implements Serializable {
	private String id;//
	private String goupId;//群组id
	private String userId;//用户id
	private String rname;//重命名
	private Integer wakeType;//是否免打扰
	private Integer seq;//加入顺序
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//创建时间
	private String bgImg;//背景图
	private Integer utype;//用户类型0表示管理员1表示普通用户
	private Integer sts;//状态0表示不可用1表示可用
}
