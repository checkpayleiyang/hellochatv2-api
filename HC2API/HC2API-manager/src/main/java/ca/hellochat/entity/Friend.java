package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import lombok.Data;

/**
 * 
 * 通讯录信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class Friend implements Serializable {
	private String id;//
	private String userId;//用户id
	private String friendId;//朋友id
	private String rname;//备注
	private Integer sts;//0被删除 1好友
	private Integer wakeFlag;//免打扰0否1是
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//添加好友时间
	private String remark;//其他
	
	private User user;
	private User friend;
	public String getRname() {
		if(StringUtil.isEmpty(rname)) {
			if(friend!=null && StringUtil.isNotEmpty(friend.getUname())) {
				return friend.getUname();
			}
			
		}
		return rname;
	}
}
