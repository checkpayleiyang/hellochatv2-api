package ca.hellochat.entity.domain;
import java.io.Serializable;

import ca.hellochat.entity.User;
import lombok.Data;  
/**
 * 
 * 用户信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class IMUser implements Serializable {
	private String userId;//主键
	private String hcno;//hellochat编号
	private String rname;//备注
	private String uname;//昵称
	private String headImg;//头像
	private String bgImg;//头像的背景
	private String declaration;//签名
	private String display;//显示
	private Integer relation;//状态0非好友，1好友
	
	public void convert(User user) {
		this.setUserId(user.getId());
		this.setHcno(user.getHcno());
		this.setUname(user.getUname());
		this.setHeadImg(user.getHeadImg());
		this.setBgImg(user.getBgImg());
		this.setDeclaration(user.getDeclaration());
		this.setDisplay(user.getDisplay());
	}
}
