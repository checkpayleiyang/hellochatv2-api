package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;  
/**
 * 
 * 用户信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class User implements Serializable {
	private String id;//主键
	private String hcno;//hellochat编号
	private String mobile;//手机号
	private String firstName;//名
	private String lastName;//姓
	@JsonIgnore
	private String upwd;//密码
	private String uname;//昵称
	private String headImg;//头像
	private String bgWallImg;//背景墙
	private String addr;//地址
	private String bgImg;//背景图像
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//创建时间
	private Integer sex;//性别
	private String declaration;//签名
	private String display;//显示
	private String languge;//语言
	private Integer sts;//状态0不可用1表示可用
	private String email;//邮箱
	private long loginNo=0;//登录的编号，没登录一次都会生成一次
	public void initEdit() {
		this.setSts(null);
		this.setHcno(null);
		this.setCreateTime(null);
	}
}
