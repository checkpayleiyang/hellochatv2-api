package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 
 * 群组信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class ChatGroup implements Serializable {
	private String id;//id
	private String userId;//创建人
	private String gname;//群组名称
	private Integer unum;//用户数量
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//创建时间
	private String bgImg;//背景图像
	private String remark;//公告
}
