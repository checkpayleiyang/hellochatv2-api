package ca.hellochat.entity;
import java.io.Serializable;

import lombok.Data;

/**
 * hellochat账号字典表
 **/
@SuppressWarnings("serial")
@Data
public class ChatNo implements Serializable {
	private String id;//id
	private String chatNo;//chat账号
	private Integer sts;//状态 0不可用 1

}
