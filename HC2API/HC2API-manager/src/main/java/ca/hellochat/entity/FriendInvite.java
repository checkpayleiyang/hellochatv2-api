package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import ca.hellochat.common.DateUtil;
import lombok.Data;

/**
 * 
 * 新朋友信息
 * 
 **/
@Data
@SuppressWarnings("serial")
public class FriendInvite implements Serializable {
	private String id;//主键
	private String receiveId;//发起邀请的用户
	private String sendId;//发起邀请的人
	private Integer sts;//状态0不同意 1已申请 2同意 3已过期
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date reqTime;//申请时间
	private String reqMsg;//申请消息
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date resTime;//反馈时间
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date expiredTime;//反馈时间
	private String resMsg;//反馈消息
	private Integer sendDel;//发送方 删除 0已删除 1可见
	private Integer receiveDel;//接收方 删除 0已删除 1可见
	private User receive;
	private User send;
	
	public Integer getSts() {
		try {
			if(expiredTime!=null) {
				if(DateUtil.compareDate(DateUtil.getNowDate(), expiredTime)==-1) {
					return 3;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return sts;
		}
		return sts;
	}
}
