package ca.hellochat.entity;
import java.io.Serializable;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 
 * 用户消息表
 * 
 **/
@Data
@SuppressWarnings("serial")
public class UserMsg implements Serializable {
	private String id;//主键
	private String receiveId;//消息接收者
	private String sendId;//发送消息用户
	private Integer mtype;//0活动消息 1好朋友消息
	private String objId;//关联对象
	private String title;//标题
	private Integer showNum;//显示数量
	private Integer sts;//状态
	private String umsg;//用户显示的消息
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;//创建时间
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date expiredTime;//创建时间
}
