package ca.hellochat.entity.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
public class Page<T> implements Serializable {
	private static final long serialVersionUID = -437730590552391597L;
	
	private boolean result=true;

	private int nowPage = 1; // 当前页数

	private int pageSize = 10; // 每页显示记录的条数

	private int totalPage; // 总的页数
	
	private int totalCount;//总记录数

	private int startCount; // 开始位置，从0开始

	private boolean hasFirst;// 是否有首页

	private boolean hasPre;// 是否有前一页

	private boolean hasNext;// 是否有下一页

	private boolean hasLast;// 是否有最后一页
	
	private List<T> rows;
	public Page() {
		
	}
	
	public Page(Integer nowPage,Integer pageSize){
		if(nowPage <= 0){
			nowPage = 1;
		}
		if(pageSize <= 0){
			pageSize = 10;
		}
		this.nowPage = nowPage;
		this.pageSize = pageSize;
	}
	
	/*public Page(Map<String,String> map){
		Integer pageSize = 0;
		Integer nowPage =0;
		try{
			pageSize = Integer.valueOf(map.get("pageSize"));
			nowPage = Integer.valueOf(map.get("nowPage"));
		}catch(Exception ex){
			
			pageSize = 10;
			nowPage = 1;
		}
		this.nowPage = nowPage;
		this.pageSize = pageSize;
	}*/
	public Page(Map<String,String> map){
		Integer pageSize = 0;
		Integer nowPage =0;
		try{
			pageSize = Integer.valueOf(map.get("pageSize"));	// 每页个数
			nowPage = Integer.valueOf(map.get("nowPage"));//当前页数
			System.out.println("pageSize==============="+pageSize);
			System.out.println("nowPage==============="+nowPage);
		}catch(Exception ex){
			pageSize = 10;
			nowPage = 1;
		}
		this.nowPage = nowPage;
		this.pageSize = pageSize;
	}
	
	public Page(String nowPageStr,String pageSizeStr){
		Integer pageSize = 0;
		Integer nowPage =0;
		try{
			nowPage = Integer.valueOf(nowPageStr);
		}catch(Exception ex){
			nowPage = 1;
		}
		try{
			pageSize = Integer.valueOf(pageSizeStr);
		}catch(Exception ex){
			pageSize = 10;
		}
		this.nowPage = nowPage;
		this.pageSize = pageSize;
	}
	/**
	 * 通过构造函数 传入 总记录数 和 当前页
	 * 
	 * @param totalCount
	 * @param nowPage
	 */
	public Page(int nowPage,int pageSize,int totalCount) {
		this.totalCount = totalCount;
		this.nowPage = nowPage;
		this.pageSize = pageSize;
	}

	/**
	 * 取得总页数，总页数=总记录数/总页数
	 * 
	 * @return
	 */
	public int getTotalPage() {
		totalPage = totalCount / pageSize;
		return (totalCount % pageSize == 0) ? totalPage
				: totalPage + 1;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getNowPage() {
		return nowPage;
	}

	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 取得选择记录的初始位置
	 * 
	 * @return
	 */
	public int getStartCount() {
		return (nowPage - 1) * pageSize;
	}

	public void setStartCount(int startCount) {
		this.startCount = startCount;
	}

	/**
	 * 是否有首页
	 * 
	 * @return
	 */
	public boolean getHasFirst() {
		return (nowPage == 1) ? false : true;
	}

	public void setHasFirst(boolean hasFirst) {
		this.hasFirst = hasFirst;
	}

	/**
	 * 是否有首页
	 * 
	 * @return
	 */
	public boolean getHasPre() {
		// 如果有首页就有前一页，因为有首页就不是第一页
		return getHasFirst() ? true : false;
	}

	public void setHasPre(boolean hasPre) {
		this.hasPre = hasPre;
	}

	/**
	 * 是否有下一页
	 * 
	 * @return
	 */
	public boolean getHasNext() {
		// 如果有尾页就有下一页，因为有尾页表明不是最后一页
		return getHasLast() ? true : false;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	/**
	 * 是否有尾页
	 * 
	 * @return
	 */
	public boolean getHasLast() {
		// 如果不是最后一页就有尾页
		return (nowPage == totalPage) ? false : true;
	}

	public void setHasLast(boolean hasLast) {
		this.hasLast = hasLast;
	}

	
	
	
	public List<T> getRows() {
		return rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	
	
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		this.totalCount=totalCount;
	}
	
	
	public boolean getResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}
	
	
	
}
