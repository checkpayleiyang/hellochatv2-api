package ca.hellochat.controller.web;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import ca.hellochat.business.IUserBusiness;
import ca.hellochat.business.IVcodeBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.MD5;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.User;
import ca.hellochat.entity.Vcode;
/**
 * 
 * 数据接受控制器
 * 
 **/
@Controller
@RequestMapping("/guest")
public class GuestController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IUserBusiness userBusiness;
	
	@Autowired
	IVcodeBusiness vcodeBusiness;
	@Autowired
	RedisTemplate redisTemplate;
	/**
 	* 
 	* @param id
 	* @return User对象
 	**/
	@RequestMapping("/isMobile")
	@ResponseBody
	public RespData isMobile (String mobile){
		log.info("验证手机号接口：mobile："+ mobile);
		if(StringUtils.isEmpty(mobile)){
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:id");
		}
		return success(1);
	}
	
	/**
 	* 
 	* 发送验证码
 	**/
	@RequestMapping("/sendCode")
	@ResponseBody
	public RespData sendCode(String mobile,Integer vtype){
		log.info("开始给手机号发送验证码："+ mobile);
		if(StringUtils.isEmpty(mobile)){
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:mobile");
		}
		if(StringUtils.isEmpty(vtype)){
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:vtype");
		}
		Vcode vcode = vcodeBusiness.sendCode(mobile, vtype);
		return success(vcode);
	}
	
	/**
 	* 
 	* 用户登录
 	**/
	@RequestMapping("/login")
	@ResponseBody
	public RespData login(@RequestParam Map<String,String> params){
		log.debug("登录："+ StringUtil.mapToString(params));
		//authType 如果等于1 表示验证码登录 2表示手机号/HelloChat号码+密码登录
		String authType = params.get("authType");
		User user = null;
		if("1".equals(authType)) {//手机号登录
			log.debug("手机号验证码方式登录........");
			String mobile = params.get("mobile");
			String vcode = params.get("vcode");
			if(StringUtil.isEmpty(vcode)) {
				throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:vcode");
			}
			//查看手机号是否存在
			params.clear();
			params.put("mobile", mobile);
			params.put("sts", "1");
			user = userBusiness.selectSimpleByParam(params);
			log.debug("获取到的用户信息为："+user);
			if(user==null || StringUtil.isEmpty(user.getId())) {
				throw new IChatException(ExceptionType.USER_AUTH_NOTFOUND,"用户不存在");
			}
			//验证验证码
			vcodeBusiness.validateCode(mobile, 2,vcode);
			
			
		}else if("2".equals(authType)) {//其他方式登录
			log.debug("其他登录方式........");
			String loginName = params.get("loginName");
			String upwd = params.get("upwd");
			if(StringUtil.isEmpty(loginName)) {
				throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:loginName");
			}
			if(StringUtil.isEmpty(upwd)) {
				throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:upwd");
			}
			params.clear();
			if(loginName.length()==10) {//login by mobile
				params.put("mobile", loginName);
				params.put("sts", "1");
			}else {//login By hcno
				params.put("hcno", loginName);
				params.put("sts", "1");
			}
			user = userBusiness.selectSimpleByParam(params);
			log.debug("获取到的用户信息为："+user);
			if(user==null || StringUtil.isEmpty(user.getId())) {
				throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:mobile");
			}
			String md5_upwd = MD5.sign(upwd+user.getId()+user.getHcno());
			log.debug("md5_pwd:["+md5_upwd+"],upwd=["+upwd+"]");
			if(!user.getUpwd().equals(md5_upwd)) {//密码不匹配
				throw new IChatException(ExceptionType.USER_AUTH_PWDERROR,"用户名或者密码错误");
			}
		}else {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:mobile");
		}
		Map<String,Object> map = optLoginUser(user);
		return success(map);
	}
	
	private Map<String,Object> optLoginUser(User user) {
		long loginNo = Long.parseLong(DateUtil.formatDate(DateUtil.getNowDate(), "yyyyMMddHHmmssSSS"));
		user.setLoginNo(loginNo);
		redisTemplate.opsForHash().put("USER_TOKEN", user.getId()+"",user);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("user", user);
		String userStr = JSONObject.toJSONString(user);
		map.put("token", JWTUtil.createToken(userStr));
		return map;
	}
	
	/**
 	* 
 	* 用户注册
 	**/
	@RequestMapping("/regist")
	@ResponseBody
	public RespData regist(@RequestParam Map<String,String> params){
		log.debug("用户注册："+ StringUtil.mapToString(params));
		log.debug("手机号验证码方式登录........");
		String mobile = params.get("mobile");
		String vcode = params.get("vcode");
		if(StringUtil.isEmpty(mobile)) {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:mobile");
		}
		
		if(StringUtil.isEmpty(vcode)) {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:vcode");
		}
		//验证验证码
		vcodeBusiness.validateCode(mobile, 1,vcode);
		//如果验证成功则注册用户
		User user = userBusiness.registByMobile(mobile);
		Map<String,Object> map = optLoginUser(user);
		return success(map);
		
		
	}
	
	/**
 	* 
 	* 忘记密码
 	**/
	@RequestMapping("/forget")
	@ResponseBody
	public RespData forget(@RequestParam Map<String,String> params){
		log.debug("用户注册："+ StringUtil.mapToString(params));
		log.debug("手机号验证码方式登录........");
		String mobile = params.get("mobile");
		String vcode = params.get("vcode");
		String upwd = params.get("upwd");
		if(StringUtil.isEmpty(mobile)) {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:mobile");
		}
		
		if(StringUtil.isEmpty(vcode)) {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:vcode");
		}
		
		if(StringUtil.isEmpty(upwd)) {
			throw new IChatException(ExceptionType.USER_AUTH_ERROR,"缺少必要的参数:upwd");
		}
		
		User userMobile = userBusiness.findUserByMobile(mobile);
		log.debug("数据库获取到的手机号对应的用户为："+userMobile);
		if(userMobile==null || StringUtil.isEmpty(userMobile.getId())) {
			throw new IChatException(ExceptionType.USER_AUTH_ERRORMOBILE,"没有手机号对应的用户："+mobile);
		}
		//验证验证码
		vcodeBusiness.validateCode(mobile, 3,vcode);
		//如果验证成功则注册用户
		userMobile.setUpwd(MD5.sign(userMobile.getUpwd()+userMobile.getId()+userMobile.getHcno()));
		userBusiness.updateById(userMobile);
		return success(userMobile);
	}
}
