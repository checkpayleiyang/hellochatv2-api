package ca.hellochat.controller.utils.config;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.web.RespData;

@RestControllerAdvice
public class GloablExceptionHandler {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	/**
     * 捕获全局异常信息，处理有可能发生的所有异常
     * @param exc
     * @param request
     * @return
     */
    @ExceptionHandler(value = Exception.class)//捕获一个父类异常Exception
    public RespData handlerExce(Exception exception, HttpServletRequest request){
    	RespData result = new RespData();
    	
    	if(exception instanceof IChatException) {
    		IChatException iexc = (IChatException)exception;
    		result.setResCode(iexc.getCode());
    		result.setResMsg(iexc.getMsg());
    		result.setResData(iexc.getMessage());
    	}else {
    		result.setResCode("500");
            result.setResMsg("服务器异常");
    	}
        log.error("------------------------------发生错误-打印开始------------------------------");
        log.error("错误消息内容：",exception);
        exception.printStackTrace();
        log.error("------------------------------发生错误-打印结束------------------------------");
        return result;
    }
}
