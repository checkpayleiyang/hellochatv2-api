package ca.hellochat.controller.web;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ca.hellochat.business.IFriendBusiness;
import ca.hellochat.business.IUserBusiness;
import ca.hellochat.business.IVcodeBusiness;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.Friend;
import ca.hellochat.entity.User;
import ca.hellochat.entity.domain.IMUser;
import ca.hellochat.entity.domain.Page;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * IM用户的信息
 * 
 **/
@Controller
@RequestMapping("/imUser")
public class IMUserController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IUserBusiness userBusiness;
	@Autowired
	IVcodeBusiness vcodeBusiness;
	
	@Autowired
	IFriendBusiness friendBusiness;
	
	@Value(value = "${helloChat.fileUpload.basePath}")
	private String basePath;
	
	@Autowired
	RedisTemplate redisTemplate;
	/**
 	* 
 	* 查看用户信息
 	**/
	@RequestMapping("/info")
	@ResponseBody
	public RespData info(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据id获取");
		User user = JWTUtil.getUserInfo(request);
		User userDB=null;
		if(params.containsKey("id") || params.containsKey("hcno") || params.containsKey("mobile")) {
			userDB = userBusiness.selectByParam(params);
		} else {
			userDB = userBusiness.selectById(user.getId());
		}
		
		if(userDB==null) {
			throw new IChatException(ExceptionType.USER_AUTH_NOTFOUND, "Token信息无效");
		}
		
		IMUser imUser = new IMUser();
		imUser.convert(user);
		params.clear();
		params.put("userId", user.getId());
		params.put("friendId", userDB.getId());
		Friend friend = friendBusiness.selectByParam(params);
		if(friend==null || StringUtil.isEmpty(friend.getId())) {
			imUser.setRelation(0);
			imUser.setRname(user.getUname());
		}else {
			imUser.setRname(friend.getRname());
			imUser.setRelation(1);
		}
		return success(user);
	}

	/**
 	* 
 	* @param Map类型的条件容器
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取User列表",httpMethod="POST")
	@RequestMapping("/list")
	@ResponseBody
	public RespData list (@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		Page page = new Page(params);
		List<User> userList = userBusiness.selectPageByParam(params, page);
		if(userList==null || userList.size()<=0){ 
			return super.success();
		}
		User user = JWTUtil.getUserInfo(request);
		List<IMUser> ilist = new ArrayList<IMUser>();
		
		for (User userDB : userList) {
			IMUser imUser = new IMUser();
			imUser.convert(user);
			params.clear();
			params.put("userId", user.getId());
			params.put("friendId", userDB.getId());
			Friend friend = friendBusiness.selectByParam(params);
			if(friend==null || StringUtil.isEmpty(friend.getId())) {
				imUser.setRelation(0);
				imUser.setRname(user.getUname());
			}else {
				imUser.setRname(friend.getRname());
				imUser.setRelation(1);
			}
		}
		page.setRows(ilist);
		return success(page);
	}
	
	
}
