package ca.hellochat.controller.utils.file;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.PathUtil;


public class UploadFile {
	 /**
     * @描述:上传文件到临时目录
     * 
     * @param file 上传的文件
     * @param tempPath 上传文件存放路径
     * @return 
     */
    public static FileTransferEntity fileUpload(MultipartFile file, String savePath) {
//    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	if (null == file) {
    		throw new IChatException(ExceptionType.SERVER_EXCEPTION, "获取上传文件失败,请检查file上传组件的名称是否正确");
		} else if (file.isEmpty()) {
			throw new IChatException(ExceptionType.SERVER_EXCEPTION, "没有选择文件");
        } else {
        	String tempPath = DateUtil.formatDate(DateUtil.getNowDate(), "yyyyMMdd");
        	boolean isWindows = PathUtil.getSystemType();
        	if(isWindows) {
        		savePath = "D:/"+savePath;
        	}
        	savePath = savePath + "/" + tempPath;
        	File saveDir = new File(savePath);
        	if (!saveDir.exists()) {
        		saveDir.mkdirs();
        	}
        	String oldfileName = file.getOriginalFilename();
        	String suffix = oldfileName.substring(oldfileName.lastIndexOf("."));
        	String fileName = StringUtil.getUUID()+suffix;
//        	fileName = saveDir +"/"+ fileName;
        	
        	File saveFile = new File(savePath +"/"+ fileName);
        	
        	//保存文件
        	try {
        		file.transferTo(saveFile);
        		FileTransferEntity fte = new FileTransferEntity();
        		fte.setCode(0);
        		fte.setMsg("成功");
        		fte.setFileName(fileName);
        		fte.setFilePath(savePath +"/"+ fileName);
        		return fte;
        	} catch (IOException e) {
        		e.printStackTrace();
        		throw new IChatException(ExceptionType.SERVER_EXCEPTION, "上传文件错误");
        	}
        }
    }
}
