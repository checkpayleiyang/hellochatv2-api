package ca.hellochat.controller.utils.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
	 //交换机名称
    public static final String ITEM_TOPIC_EXCHANGE = "ex.sms";
    //队列名称
    public static final String ITEM_QUEUE = "queue.sms.crops";

    //声明交换机
    @Bean("directExchange")
    public DirectExchange  directExchange(){
    	 return new DirectExchange(ITEM_TOPIC_EXCHANGE,false,false);
    }

    //声明队列
    @Bean("directQueue")
    public Queue directQueue(){
    	  //一般设置一下队列的持久化就好,其余两个就是默认false
        return new Queue(ITEM_QUEUE,true);
    }

    //绑定队列和交换机
    @Bean
    public Binding bindingDirect(){
    	return BindingBuilder.bind(directQueue()).to(directExchange()).with(ITEM_QUEUE);
    }
}
