package ca.hellochat.controller.web;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ca.hellochat.business.IFriendBusiness;
import ca.hellochat.business.IFriendInviteBusiness;
import ca.hellochat.business.IUserMsgBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.FriendInvite;
import ca.hellochat.entity.User;
import ca.hellochat.entity.domain.Page;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * 朋友业务相关
 * 
 **/
@Controller
@RequestMapping("/friendInvite")
public class FriendInviteController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IFriendInviteBusiness friendInviteBusiness;
	@Autowired
	IFriendBusiness friendBusiness;
	@Autowired
	IUserMsgBusiness userMsgBusiness;
	/**
 	* 
 	* @param 查询
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取FriendInvite列表",httpMethod="POST")
	@RequestMapping("/receives")
	@ResponseBody
	public RespData receives (@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		//这里注意：我是作为被邀请人来传参的
		params.put("receiveId", user.getId());
		params.put("receiveDel", "1");
		params.put("orderBy", "reqTime desc");
		Page page = new Page(params);
		List<FriendInvite> friendInviteList = friendInviteBusiness.selectPageByParam(params, page);
		if(friendInviteList==null || friendInviteList.size()<=0){ 
			return super.success();
		}
		return success(page);
	}
	
	/**
 	* 
 	* @param 查询
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取FriendInvite列表",httpMethod="POST")
	@RequestMapping("/sends")
	@ResponseBody
	public RespData sends (@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		//这里注意：我是作为被邀请人来传参的
		params.put("sendId", user.getId());
		params.put("sendDel", "1");
		params.put("orderBy", "reqTime desc");
		Page page = new Page(params);
		List<FriendInvite> friendInviteList = friendInviteBusiness.selectPageByParam(params, page);
		if(friendInviteList==null || friendInviteList.size()<=0){ 
			return super.success();
		}
		return success(page);
	}
	
	/**
 	* 
 	* @param 删除好友记录
 	* @return page对象
 	**/
	@RequestMapping("/del")
	@ResponseBody
	public RespData del(String id,HttpServletRequest request){
		log.info("删除好友申请记录，id::" + id);
		User user = JWTUtil.getUserInfo(request);
		FriendInvite fi = friendInviteBusiness.selectById(id);
		log.debug("从数据库获取到的fi:"+fi);
		if(fi==null) {
			throw new IChatException(ExceptionType.INVALID_AUTHORITY, "服务器响应异常");
		}
		if(fi.getReceiveId().equals(user.getId())) {//接收的人删除
			fi.setReceiveDel(0);
		}else if(fi.getSendId().equals(user.getId())) {
			fi.setSendDel(0);
		}else{
			throw new IChatException(ExceptionType.INVALID_AUTHORITY, "服务器响应异常");
		}
		friendInviteBusiness.updateById(fi);
		
		return success();
	}
}
