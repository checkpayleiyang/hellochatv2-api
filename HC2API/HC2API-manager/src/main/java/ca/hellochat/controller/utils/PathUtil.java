package ca.hellochat.controller.utils;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 类路径工具类
 * 
 */
public class PathUtil {
	private static Log log = LogFactory.getLog(PathUtil.class);
    /**
     * 检查系统类型
     * 
     * @return windows:true||linux:false
     */
    public static boolean getSystemType() {
        boolean isWindows = false;
        String system = System.getProperties().getProperty("os.name").toUpperCase();
        if (system.indexOf("WINDOWS") != -1) {
            isWindows = true;
        }
        return isWindows;
    }

    /**
     * 获取文件真实路径
     * 
     * @param filePath
     *            路径
     * @return 真实路径
     */
    public static String getRealFilePath(String filePath) {
        boolean isWindows = getSystemType();
        if (isWindows) {
            // WINDOWS
            filePath = filePath.replaceAll("[\\\\/]", File.separator + File.separator);
        }else {
            // LINUX
            filePath = filePath.replaceAll("[\\\\/]", File.separator);
        }
        return filePath;
    }

    /**
     * 根据系统类型获得文件真实全路径(如果fileName带后缀，filePrex参数传入null)
     * 
     * @param outputFilePath
     *            文件所在路径
     * @param fileName
     *            文件名称
     * @param filePrex
     *            文件后缀
     * @return 文件全路径
     */
    public static String getRealFilePath(String outputFilePath, String fileName, String filePrex) {
        if (!outputFilePath.endsWith(File.separator)) {
        	outputFilePath += File.separator;
        }
        outputFilePath += fileName;
        if (null != filePrex && !filePrex.isEmpty()) {
        	outputFilePath += "." + filePrex;
        }
        return getRealFilePath(outputFilePath);
    }

    /**
     * 路径累加
     * 
     * @param basePath
     *            基本路径
     * @param folderName
     *            文件夹名称（变长参数）
     * @return 累加后的路径
     */
    public static String pathSupplement(String basePath, String... folderNames) {
        for (String folderName : folderNames) {
            if (!basePath.endsWith(File.separator)) {
                basePath += File.separator + folderName + File.separator;
            }
            else {
                basePath += folderName + File.separator;
            }
        }
        return getRealFilePath(basePath);
    }
    
    public static void createDirectory(String pathString){
//		String outputFilePath = PathUtil.pathSupplement(ReadGeneral.zipPath,pathString);
//		File outputPathFile = new File(outputFilePath);
//        if (!outputPathFile.exists() && !outputPathFile.isDirectory()) {
//        	log.info("create directory");
//            // 判断创建目录是否成功
//            if (!outputPathFile.mkdirs()) {
//            	log.info("create directory failer!");
//            }
//        }
	}

}