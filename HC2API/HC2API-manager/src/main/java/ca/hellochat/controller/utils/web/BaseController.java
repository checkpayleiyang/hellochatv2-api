package ca.hellochat.controller.utils.web;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import ca.hellochat.common.StringUtil;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.entity.User;

//import ca.hellochat.entity.domain.User;

/**
 * Controller基类，返回数据封装
 */
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    
    public RespData success() {
    	RespData result = new RespData();
        result.setResCode("0");
        result.setResMsg("SUCCESS");//成功
        return result;
    }
    public RespData success(Object obj) {
    	RespData result = new RespData();
        result.setResCode("0");
        result.setResMsg("SUCCESS");//成功
        result.setResData(obj);
        return result;
    }
    public RespData success(String resCode,Object obj) {
    	RespData result = new RespData();
        result.setResCode(resCode);
        result.setResMsg("SUCCESS");//成功
        result.setResData(obj);
        return result;
    }
    public RespData success(String resCode,String resMsg) {
    	RespData result = new RespData();
        result.setResCode(resCode);
        result.setResMsg(resMsg);
        return result;
    }
    public RespData error() {
    	RespData result = new RespData();
        result.setResCode("500");
        result.setResMsg("Handling Error");//操作异常
        return result;
    }
    public RespData error(String resMsg) {
        return error("500",resMsg);
    }
    public RespData error(Object obj) {
    	RespData result = new RespData();
        result.setResCode("10000");
        result.setResMsg("Handling Error");//操作异常
        result.setResData(obj);
        return result;
    }
    public RespData error(String resCode,Object obj) {
    	RespData result = new RespData();
        result.setResCode(resCode);
        result.setResMsg("Handling Error");//操作异常
        result.setResData(obj);
        return result;
    }
    public RespData error(String resCode,String resMsg) {
    	RespData result = new RespData();
        result.setResCode(resCode);
        result.setResMsg(resMsg);
        return result;
    }
    public RespData respResult(String resCode,String resMsg,Object obj) {
    	RespData result = new RespData();
        result.setResCode(resCode);
        result.setResMsg(resMsg);
        result.setResData(obj);
        return result;
    }
    
    public static void error(HttpServletResponse response,String msg){
    	PrintWriter out = null;
        try {
            //设定类容为json的格式
        	response.setContentType("application/json;charset=UTF-8");
            out = response.getWriter();
            //写到客户端
            out.write("{code:\"500\",msg:\""+msg+"\"}");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(out != null){
                out.close();
            }
        }
    }
    
	public User getUserInfo(HttpServletRequest request){
		try {
			String userStr = (String)request.getAttribute(JWTUtil.USER_INFO_KEY);
			if(StringUtil.isEmpty(userStr)) {
				return null;
			}
			User user = JSONObject.parseObject(userStr,User.class);
			if(user==null) {
				return null;
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
//	
//	public void securityDeptIdData(Map<String,String> param,HttpSession session){
//		User user = SessionUtil.getLoginUser(session);
//		if(user!=null&&!user.adminFlag()){
//			param.put("deptId",user.getOptId());
//		}
//	}
    
}
