package ca.hellochat.controller.utils.Interceptor;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ca.hellochat.common.StringUtil;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.User;
import io.jsonwebtoken.Claims;

public class JwtInterceptor extends HandlerInterceptorAdapter {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	RedisTemplate redisTemplate;
	   @Override
	   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	       // 获取用户 token
	       String token = request.getHeader("token");
	       if (StringUtil.isEmpty(token)) {
	           token = request.getParameter("token");
	       }
	       //  token为空
	       if(StringUtil.isEmpty(token)) {
	    	   log.error("token已经失效、token为空");
	    	   this.writerErrorMsg("402","token已经失效10001",response);
	           return false;
	       }
	       //  校验并解析token，如果token过期或者篡改，则会返回null
	       Claims claims = JWTUtil.getTokenClaim(token);
	       if(null == claims){
	    	   log.error("token已经失效、根据token不能获取到对应的信息");
	           this.writerErrorMsg("402","token已经失效10002",response);
	           return false;
	       }
	       String userInfoStr = claims.getSubject();
		   User user = JSONObject.parseObject(userInfoStr,User.class);
	       log.error("user:"+user);

	       User redis_user = (User)redisTemplate.opsForHash().get("USER_TOKEN", user.getId()+"");
	       log.error("redis_user:"+redis_user);
	       if(redis_user.getLoginNo()<=0 || redis_user.getLoginNo() > user.getLoginNo()) {
	    	   //redis存储的用户和客户端存储的对比，如果redis存储的号码大于客户端的用户责需要重新登录
	    	   this.writerErrorMsg("402","token已经失效10003",response);
	           return false;
	       }
//	       userDb = userBusiness.selectById(user.getId());
	       
	       //  校验通过后，设置用户信息到request里，在Controller中从Request域中获取用户信息
//	       request.getSession().setAttribute(JWTUtil.USER_INFO_KEY, claims.getSubject());
	       request.setAttribute(JWTUtil.USER_INFO_KEY, user);
	       return true;
	   }
	 
	   /**
	 
	    * 利用response直接输出错误信息
	 
	    * @param code
	 
	    * @param msg
	 
	    * @param response
	 
	    * @throws IOException
	 
	    */
	 
	   private void writerErrorMsg(String code, String msg, HttpServletResponse response) throws IOException {
		   RespData resp = new RespData();
		   resp.setResCode(code);
		   resp.setResData(msg);
	       response.setContentType("application/json;charset=UTF-8");
	       response.setStatus(401,"token 失效");
	       response.getWriter().write(JSON.toJSONString(resp));
	 
	     }
}
