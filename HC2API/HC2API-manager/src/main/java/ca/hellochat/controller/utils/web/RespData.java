package ca.hellochat.controller.utils.web;
/**
 * 统一返回数据载体
 * @author yl
 * 2019-07-05
 */
public class RespData{
	/** 错误码. */
    private String resCode;
    /** 提示信息. */
    private String resMsg;
    /** 具体的内容. */
    private Object resData;
    
    public RespData() {
    	this.resCode = "0";
    	this.resMsg = "成功";
    }
    
    public RespData(String resCode) {
    	this.resCode = resCode;
    }
    public RespData(String resCode,String resMsg) {
    	this.resCode = resCode;
    	this.resMsg = resMsg;
    }

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public Object getResData() {
		return resData;
	}

	public void setResData(Object resData) {
		this.resData = resData;
	}

	@Override
	public String toString() {
		return "RespData [" + (resCode != null ? "resCode=" + resCode + ", " : "")
				+ (resMsg != null ? "resMsg=" + resMsg + ", " : "") + (resData != null ? "resData=" + resData : "")
				+ "]";
	}

	
	
}
