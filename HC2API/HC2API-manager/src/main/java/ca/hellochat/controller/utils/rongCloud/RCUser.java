package ca.hellochat.controller.utils.rongCloud;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;

public class RCUser {
	public RCUser() {}
	public RCUser(String id, String uname, String portrait, Date imCreateTime) {
		this.id = id;
		this.uname = uname;
		this.portrait = portrait;
		this.imCreateTime = imCreateTime;
	}
	//存放在融云的id
	public String id;
	//存放在融云的name
	public String uname;
	//存放到融云的token
	public String portrait;
	//在融云的创建时间
	public Date imCreateTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPortrait() {
		return portrait;
	}
	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	public Date getImCreateTime() {
		return imCreateTime;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public void setImCreateTime(Date imCreateTime) {
		this.imCreateTime = imCreateTime;
	}
	@Override
	public String toString() {
		return JSONObject.toJSONString(this);
	}
	
}
