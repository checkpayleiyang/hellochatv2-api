package ca.hellochat.controller.utils.jwt;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
public class JWTUtil {
	private static Logger log = LoggerFactory.getLogger(JWTUtil.class);
	private final static String secret="token";
    public final static long SessionTime=24 * 60 * 60 * 1000;
    public final static long RefreshSessionTime=60 * 60 * 1000;
    public static final String USER_INFO_KEY = "USERINFO";
    
    /**
     * 生成token
     * @param subject
     * @return
     */
    public static String createToken (String subject){
    	return createToken(subject,JWTUtil.SessionTime);
    }
    
    /**
     * 生成token
     * @param subject
     * @return
     */
    public static String createToken(String subject,Long expire){
    	Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + expire);//过期时间
 
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setSubject(subject)
                .setIssuedAt(nowDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    /**
     * 获取token中注册信息
     * @param token
     * @return
     */
    public static Claims getTokenClaim(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 验证token是否有效
     * @param expirationTime
     * @return
     * @throws Exception 
     */
    public static boolean isTokenExpired (Date expirationTime){
    	boolean isTrue = false;
    	try{
    		
    		isTrue = (DateUtil.compareDate(expirationTime, new Date())==1)?false:true;
    		return isTrue;
    	}catch(Exception ex){
    		return false;
    	}
    }
 
    /**
     * 获取token失效时间
     * @param token
     * @return
     */
    public static Date getExpirationDate(String token) {
        return getTokenClaim(token).getExpiration();
    }
    /**
     * 获取用户名从token中
     */
    public static String unsign(String token) {
        return getTokenClaim(token).getSubject();
    }
 
    /**
     * 获取jwt发布时间
     */
    public static Date getIssuedAtDate(String token) {
        return getTokenClaim(token).getIssuedAt();
    }
    
    public static User getUserInfo(String token){
    	Claims claims = JWTUtil.getTokenClaim(token);
		if(claims == null || !JWTUtil.isTokenExpired(claims.getExpiration())){
    		System.out.println("token失效");
    		return null;
    	}
		String userInfoStr = claims.getSubject();
		User newUserInfo = JSONObject.parseObject(userInfoStr,User.class);
		return newUserInfo;
    }
    
    public static User getUserInfo(HttpServletRequest request){
    	User user = (User)request.getAttribute(JWTUtil.USER_INFO_KEY);
    	log.debug("token中的user:::"+user);
    	if(user==null|| StringUtil.isEmpty(user.getId())) {
    		throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
    	}
		return user;
    }
    
    public static void main(String[] args) {
    	String token = JWTUtil.createToken("yanglei");
//    	
    	System.out.println("token="+token);
//    	
//    	String uname = JWTUtil.getUsernameFromToken(token);
//    	
//    	System.out.println("uname="+uname);
    	
    	
//    	String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5YW5nbGVpIiwiaWF0IjoxNTgxOTMxNTg3LCJleHAiOjE1ODE5MzE1OTJ9.Zy3t2KM3qoVG36E5msznvuPvhChoxPeS3xX2PRmfqCH1-gQTSLFqo1NmDniyxwR1_w6rGUHXlPgN1LYNZO4RdQ";
    	
    	Claims claims = JWTUtil.getTokenClaim(token);
//    	System.out.println("=="+JWTUtil.isTokenExpired(claims.getExpiration()));
    	if(claims == null || !JWTUtil.isTokenExpired(claims.getExpiration())){
    		System.out.println("token失效");
    	}else{
    		String str = unsign(token);
    		System.out.println("str="+str);
    	}
    	
//    	Date isTrue = JWTUtil.getExpirationDateFromToken(token);
//    	System.out.println(DateUtil.formatDateTime(isTrue));
//    	User userInfo = new User();
//		userInfo.setCreateTime(new Date());
//		userInfo.setName("杨磊");
//		userInfo.setPassword("yanlgei");
//		String userStr = JSONObject.toJSONString(userInfo);
//		//token为登录校验的token
//		String token = JWTUtil.createToken(userStr,30 * 60 * 1000L);
//		System.out.println("token="+token);
//		
//		Claims claims = JWTUtil.getTokenClaim(token);
//		if(claims == null || !JWTUtil.isTokenExpired(claims.getExpiration())){
//    		System.out.println("token失效");
////    		return super.error("token失效");
//    		
//    	}
//		
//		String userInfoStr = claims.getSubject();
//		User NewuserInfo = JSONObject.parseObject(userInfoStr,User.class);
//		
//		String newUserStr = JSONObject.toJSONString(NewuserInfo);
//		System.out.println("newUserStr="+newUserStr);
//    	
//    	
	}
}
