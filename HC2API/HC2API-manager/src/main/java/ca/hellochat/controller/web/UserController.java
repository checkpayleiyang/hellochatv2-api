package ca.hellochat.controller.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

import ca.hellochat.business.IUserBusiness;
import ca.hellochat.business.IVcodeBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.MD5;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.file.FileTransferEntity;
import ca.hellochat.controller.utils.file.UploadFile;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.User;
import ca.hellochat.entity.domain.Page;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * 用户信息，三户模型中的客户
 * 
 **/
@Controller
@RequestMapping("/user")
public class UserController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IUserBusiness userBusiness;
	@Autowired
	IVcodeBusiness vcodeBusiness;
	
	@Value(value = "${helloChat.fileUpload.basePath}")
	private String basePath;
	
	@Autowired
	RedisTemplate redisTemplate;
	
	/**
 	* 
 	* 编辑用户信息
 	**/
	@RequestMapping("/edit")
	@ResponseBody
	public RespData edit(@RequestBody User user,HttpServletRequest request){
		log.info("编辑用户信息开始"+user);
		User sessionUser = JWTUtil.getUserInfo(request);
		if(sessionUser==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		if(user==null || StringUtil.isEmpty(user.getId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:id");
		}
		log.info("user.getId().equals(sessionUser.getId())="+(user.getId().equals(sessionUser.getId())));
		if(!user.getId().equals(sessionUser.getId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"操作异常");
		}
		
		user.setId(sessionUser.getId());
		//肯定不让用户修改的属性
		user.initEdit();
		userBusiness.updateById(user);
		
		user = userBusiness.selectById(user.getId());
		return success(user);
	}
	
	/**
 	* 
 	* 修改密码：包含旧密码修改，验证码修改
 	**/
	@RequestMapping("/editPwd")
	@ResponseBody
	public RespData editPwd(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("修改密码："+StringUtil.mapToString(params));
		
		String hasPwd = params.get("hasPwd");
		if(StringUtil.isEmpty(hasPwd)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:hasPwd");
		}
		User sessionUser = JWTUtil.getUserInfo(request);
		User user = userBusiness.selectSimpleById(sessionUser.getId());
		if(user==null || StringUtil.isEmpty(user.getId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"用户数据异常");
		}
		if("0".equals(hasPwd)) {//无密码
			//通过验证码修改密码
			String mobile = params.get("mobile");
			if(StringUtil.isEmpty(mobile)) {
				throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:mobile");
			}
			String vcode = params.get("vcode");
			if(StringUtil.isEmpty(vcode)) {
				throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:vcode");
			}
			vcodeBusiness.validateCode(mobile, 4,vcode);
			String upwd = params.get("upwd");
			if(StringUtil.isEmpty(vcode)) {
				throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:upwd");
			}
			String md5_upwd = MD5.sign(upwd + sessionUser.getId() + sessionUser.getHcno());
			user.setUpwd(md5_upwd);
		}else {//有密码
			String upwd = params.get("upwd");
			String oldPwd = params.get("oldPwd");
			if(StringUtil.isEmpty(upwd)) {
				throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:upwd");
			}
			if(StringUtil.isEmpty(oldPwd)) {
				throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数:oldPwd");
			}
			
			String md5_old_upwd = MD5.sign(oldPwd + user.getId() + user.getHcno());
			//验证老密码
			if(!md5_old_upwd.equals(user.getUpwd())) {
				throw new IChatException(ExceptionType.USER_AUTH_ERROROLDPWD,"旧密码错误");
			}
			//修改密码
			String md5_new_upwd = MD5.sign(upwd + user.getId() + user.getHcno());
			user.setUpwd(md5_new_upwd);
		}
		userBusiness.updateById(user);
		return success(user);
	}
	
	/**
 	* 
 	* 查看用户信息
 	**/
	@RequestMapping("/info")
	@ResponseBody
	public RespData info(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据id获取");
		User sessionUser = JWTUtil.getUserInfo(request);
		if(sessionUser==null || sessionUser.getId()==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		User user=null;
		if(params.containsKey("id") || params.containsKey("hcno") || params.containsKey("mobile")) {
			user = userBusiness.selectByParam(params);
		} else {
			user = userBusiness.selectById(sessionUser.getId());
		}
		if(user==null) {
			throw new IChatException(ExceptionType.USER_AUTH_NOTFOUND, "Token信息无效");
		}
		return success(user);
	}
	
	/**
 	* 
 	* @param id
 	* @return User对象
 	**/
	@ApiOperation(value = "获取附带IMUser信息的user" ,notes="获取User信息",httpMethod="POST")
	@RequestMapping("/upload")
	@ResponseBody
	public RespData getUserAndIMUser (@RequestParam("image")  MultipartFile file,HttpServletRequest request){
		User user = JWTUtil.getUserInfo(request);
		if(user==null || user.getId()==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		
		String userImgPath =  basePath +"/headImg/"+ user.getId();
		FileTransferEntity fte = UploadFile.fileUpload(file, userImgPath);
		fte.setBasePath(basePath);
		return success(fte.getRequestPath());
	}
	
	/**
 	* 
 	* @param id
 	* @return User对象
 	**/
	@ApiOperation(value = "获取附带IMUser信息的user" ,notes="获取User信息",httpMethod="POST")
	@RequestMapping("/refreshToken")
	@ResponseBody
	public RespData refreshToken(HttpServletRequest request){
		User user = JWTUtil.getUserInfo(request);
		if(user==null || user.getId()==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		User userDB = userBusiness.selectSimpleById(user.getId());
		long loginNo = Long.parseLong(DateUtil.formatDate(DateUtil.getNowDate(), "yyyyMMddHHmmssSSS"));
		userDB.setLoginNo(loginNo);
		String userStr = JSONObject.toJSONString(userDB);
		String token = JWTUtil.createToken(userStr);
		redisTemplate.opsForHash().put("USER_TOKEN", user.getId()+"",userDB);
		return success(token);
	}
	
	/**
 	* 
 	* @param Map类型的条件容器
 	* @return page对象
 	**/
	@ApiOperation(value = "用户登录",notes="用户登录",httpMethod="POST")
	@RequestMapping("/login")
	@ResponseBody
	public RespData login(@RequestParam Map<String,String> params){
		log.info("用户登录开始"+StringUtil.mapToString(params));
		String uname = params.get("uname");
		String upwd = params.get("upwd");
		if(StringUtil.isEmpty(uname)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数：uname");
		}
		if(StringUtil.isEmpty(upwd)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数：upwd");
		}
		upwd = MD5.sign(upwd);
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("uname", uname);
		searchMap.put("sts","1");
		User user = userBusiness.selectByParam(searchMap);
		if(user==null || StringUtil.isEmpty(user.getId())){ 
			//如果没有找到用户则注册
			user = new User();
			user.setUname(uname);
			user.setUpwd(upwd);
			user.setCreateTime(DateUtil.getNowDate());
			user.setSex(1);
			user.setSts(1);
			userBusiness.insertSelective(user);
		}else {
			//如果根据名称可以找到，接着匹配用户的密码是否正确
			String upwd_DB = user.getUpwd();
			if(!upwd.equals(upwd_DB)) {
				//传递进来的密码和数据库的密码不匹配则返回错误
				throw new IChatException(ExceptionType.FAILED_AUTHORITY,"用户名或者密码错误");
			}
		}
		//根据用户刷新一次token,放到user中、然后更新到数据库
		userBusiness.refreshRongCloudToken(user);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("user",user);
		String userStr = JSONObject.toJSONString(user);
		map.put("token", JWTUtil.createToken(userStr));
		return success(map);
	}

	/**
 	* 
 	* @param Map类型的条件容器
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取User列表",httpMethod="POST")
	@RequestMapping("/list")
	@ResponseBody
	public RespData list (@RequestParam Map<String,String> params){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		Page page = new Page(params);
		List<User> userList = userBusiness.selectPageByParam(params, page);
		if(userList==null || userList.size()<=0){ 
			return super.success();
		}
		page.setRows(userList);
		return success(page);
	}
	
	
//	/**
// 	* 
// 	* @param Map类型的条件容器
// 	* @return page对象
// 	**/
//	@ApiOperation(value = "根据条件列表 - 分页",notes="获取User列表",httpMethod="POST")
//	@RequestMapping("/status")
//	@ResponseBody
//	public RespData status(String status,HttpServletRequest request){
//		log.info("修改用户在线状态开始.staus:"+status);
//		User user = JWTUtil.getUserInfo(request);
//		if(user==null) {
//			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
//		}
//		
//		Page page = new Page(params);
//		List<User> userList = userBusiness.selectPageByParam(params, page);
//		if(userList==null || userList.size()<=0){ 
//			return super.success();
//		}
//		page.setRows(userList);
//		return success(page);
//	}
	
}
