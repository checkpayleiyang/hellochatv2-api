package ca.hellochat.controller.utils.rongCloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;

import io.rong.RongCloud;
import io.rong.methods.chatroom.Chatroom;
import io.rong.methods.user.User;
import io.rong.models.chatroom.ChatroomModel;
import io.rong.models.response.ResponseResult;
import io.rong.models.response.TokenResult;
import io.rong.models.response.UserResult;
import io.rong.models.user.UserModel;
@Component
public class RCUtil {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Value("${rongCloud.config.appKey}")
	public String appKey="pwe86ga5psp26";	
	
	@Value("${rongCloud.config.appSecret}")
	public String appSecret = "8j0AUcmKnb";
	
	@Value("${rongCloud.config.api}")
	public String api = "http://api-sg01.ronghub.com";
	
	RongCloud rongCloud = null;
	
	public RCUtil() {
		rongCloud = RongCloud.getInstance(appKey, appSecret,api);
	}
	
	/**
	 * 注册
	 * @param appUser
	 * @return
	 */
	public boolean register(ca.hellochat.entity.User appUser) {
		  TokenResult result;
		try {
//			rongCloud = RongCloud.getInstance(appKey, appSecret,api);
			
//			rongCloud.
			User user = rongCloud.user;
			  /**
			  * API 文档: http://www.rongcloud.cn/docs/server_sdk_api/user/user.html#register
			  *
			  * 注册用户，生成用户在融云的唯一身份标识 Token
			  */
			  UserModel userModel = new UserModel()
			            .setId(appUser.getId()+"")
			            .setName(appUser.getUname())
			            .setPortrait("http://www.rongcloud.cn/images/logo.png");
			  result = user.register(userModel);
			  log.info("result="+JSONObject.toJSONString(result));
			  if(result.getCode()!=200) {
				  return false;
			  }
//			  appUser.setImtoken(result.getToken());
//			  IMUser imUser = getIMUser(appUser.getId()+"");
//			  log.info("imUser="+JSONObject.toJSONString(imUser));
//			  if(imUser==null) {
//				  return false;
//		      }
//			  appUser.setIMUser(imUser);
			  return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int destroyRoom(String roomId) {	
		log.info("销毁房间开始::roomId:"+roomId);
		try {
			ChatroomModel chatroomModel = new ChatroomModel().setId(roomId);
			Chatroom chatroom = rongCloud.chatroom;
			ResponseResult rr = chatroom.destroy(chatroomModel);
			log.info("rr="+JSONObject.toJSONString(rr));
			int code = rr.getCode();
			log.info("销毁房间结果::code:"+code);
			return code;
		} catch (Exception e) {
			e.printStackTrace();
			throw new IChatException(ExceptionType.FAILED_RESPONSE, "销毁房间错误");
		}
		
		

	}
	 

	
	/**
	 * 注册
	 * @param appUser
	 * @return
	 */
	public RCUser getIMUser(String id) {
		UserResult ur;
		try {
//			RongCloud rongCloud = RongCloud.getInstance(appKey, appSecret,api);
			User user = rongCloud.user;
			UserModel um = new UserModel();
			um.setId(id+"");
			ur = user.get(um);
			System.out.println("result="+JSONObject.toJSONString(ur));
			if(ur.getCode()!=200) {
				return null;
			 }
			RCUser rcUser = new RCUser(id+"",ur.getUserName(),ur.getUserPortrait(),DateUtil.str2Date(ur.getCreateTime(),"yyyy-MM-dd HH:mm:ss"));
			  return rcUser;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	public static void main(String[] args) {
		RCUtil rcutil = new RCUtil();
		ca.hellochat.entity.User user = new ca.hellochat.entity.User();
		user.setId("1");
		user.setUname("111");
		boolean flag = rcutil.register(user);
		System.out.println("user="+user);
		//sAWgz9lExUgk7RX9Oyua15hn9t58pfA+@gtq0.sg.rongnav.com;gtq0.sg.rongcfg.com
		//sAWgz9lExUgk7RX9Oyua14T3hAn4ozXd@gtq0.sg.rongnav.com;gtq0.sg.rongcfg.com
		//sAWgz9lExUgk7RX9Oyua15QB2Y8nEcmN@gtq0.sg.rongnav.com;gtq0.sg.rongcfg.com
	}
}
