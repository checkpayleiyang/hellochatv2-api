package ca.hellochat.controller.web;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ca.hellochat.business.IUserMsgBusiness;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.User;
import ca.hellochat.entity.UserMsg;
import ca.hellochat.entity.domain.Page;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * 用户消息
 * 
 **/
@Controller
@RequestMapping("/userMsg")
public class UserMsgController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IUserMsgBusiness userMsgBusiness;
	/**
 	* 
 	* @param Map类型的条件容器
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取UserMsg列表",httpMethod="POST")
	@RequestMapping("/sum")
	@ResponseBody
	public RespData sum(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		if(user==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		String userId = user.getId()+"";
		params.clear();
		params.put("receiveId", userId);
		params.put("mtype", "2");
		params.put("sts", "0");
		int friendRequestCount = userMsgBusiness.selectCountByParam(params);
		Map<String,Integer> map = new HashMap<String,Integer>();
		map.put("friendRequest", friendRequestCount);
		return success(map);
	}
	/**
 	* 
 	* @param已读消息操作
 	* @return page对象
 	**/
	@ApiOperation(value = "已读操作",notes="已读消息操作",httpMethod="POST")
	@RequestMapping("/readMsg")
	@ResponseBody
	public RespData readMsg(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("已读消息开始："+StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		if(user==null) {
			throw new IChatException(ExceptionType.INVALID_TOKEN, "Token信息无效");
		}
		String id = params.get("id");
		String mtype = params.get("mtype");
		params.put("receiveId", user.getId()+"");
		if(StringUtils.isEmpty(id)&&StringUtils.isEmpty(mtype)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数,userMsgId和mtype不能同时为空");
		}
		UserMsg um = new UserMsg();
		um.setSts(1);
		userMsgBusiness.updateByParam(um, params);
		return success();
	}

	/**
 	* 
 	* @param Map类型的条件容器
 	* @return page对象
 	**/
	@ApiOperation(value = "根据条件列表 - 分页",notes="获取UserMsg列表",httpMethod="POST")
	@RequestMapping("/list")
	@ResponseBody
	public RespData list (@RequestParam Map<String,String> params){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		Page page = new Page(params);
		List<UserMsg> userMsgList = userMsgBusiness.selectPageByParam(params, page);
		if(userMsgList==null || userMsgList.size()<=0){ 
			return super.success();
		}
		page.setRows(userMsgList);
		return success(page);
	}

}
