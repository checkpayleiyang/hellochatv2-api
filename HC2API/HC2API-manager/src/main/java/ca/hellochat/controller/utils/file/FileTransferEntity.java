package ca.hellochat.controller.utils.file;

import lombok.Data;

@Data
public class FileTransferEntity {
	private int code;
	private String msg;
	private String fileName;
	private String filePath;
	private String requestPath;
	private String basePath;
	
	
	public String getRequestPath() {
		requestPath = filePath.substring(basePath.length());
		 return requestPath;
	}
}
