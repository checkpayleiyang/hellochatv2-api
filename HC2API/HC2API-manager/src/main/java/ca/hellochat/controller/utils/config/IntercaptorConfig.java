package ca.hellochat.controller.utils.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import ca.hellochat.controller.utils.Interceptor.JwtInterceptor;
@Configuration
public class IntercaptorConfig implements WebMvcConfigurer {
	@Bean
    public JwtInterceptor JwtInterceptor(){
        return new JwtInterceptor();
    }
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(JwtInterceptor())
                //拦截的路径
                .addPathPatterns("/**")
                //排除登录接口
                .excludePathPatterns("/guest/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/room/event");
    }
}
