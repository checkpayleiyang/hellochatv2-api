package ca.hellochat.controller.utils.requestSign;

import com.alibaba.fastjson.JSONObject;
import ca.hellochat.common.StringUtil;

//import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

//@Slf4j
//@Component
public class SignAuthFilter implements Filter {
	private static Logger log = LoggerFactory.getLogger(SignAuthFilter.class);
	static List<String> excludeList = new ArrayList<String>();
	static {
		excludeList.add("/error");
		excludeList.add("swagger");
		excludeList.add("webjars");
		excludeList.add("v2");
	}

	@Override
	public void init(FilterConfig filterConfig) {
		log.info("签名拦截器：初始化 SignAuthFilter");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletResponse response = (HttpServletResponse) res;
		// 防止流读取一次后就没有了, 所以需要将流继续写出去
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletRequest requestWrapper = new BodyReaderHttpServletRequestWrapper(request);
		String redirectUri = requestWrapper.getRequestURI();
		log.debug("签名拦截器：获取到的redirectUri为：" + redirectUri);
		if(redirectUri.indexOf(".")>-1 || redirectUri.indexOf("swagger")>-1 || true){
			log.debug("签名拦截器：直接返回..");
			chain.doFilter(request, response);
			return;
		}
		boolean isFilter = true;
		for(String exUri:excludeList){
			if(redirectUri.indexOf(exUri)>-1){
				isFilter = false;
				break;
			}
		}
		log.debug("isFilter=" + isFilter);
		// 获取图标不需要验证签名
		if (!isFilter) {
			chain.doFilter(request, response);
			return;
		} else {
			// 获取全部参数(包括URL和body上的)
			SortedMap<String, String> allParams = HttpUtils.getAllParams(requestWrapper);
			log.info("获取参函数为：" + StringUtil.mapToString(allParams));
			// 对参数进行签名验证
			boolean isSigned = SignUtil.verifySign(allParams);
			if (isSigned) {
				log.info("签名通过");
				chain.doFilter(requestWrapper, response);
			} else {
				log.info("参数校验出错");
				// 校验失败返回前端
				response.setCharacterEncoding("UTF-8");
				response.setContentType("application/json; charset=utf-8");
				PrintWriter out = response.getWriter();
				JSONObject resParam = new JSONObject();
				resParam.put("msg", "参数校验出错");
				resParam.put("success", "false");
				out.append(resParam.toJSONString());
			}
			return;
		}
	}

	@Override
	public void destroy() {
		log.info("销毁 SignAuthFilter");
	}
}
