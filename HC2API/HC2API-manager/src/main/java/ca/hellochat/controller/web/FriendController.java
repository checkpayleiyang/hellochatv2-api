package ca.hellochat.controller.web;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ca.hellochat.business.IFriendBusiness;
import ca.hellochat.business.IFriendInviteBusiness;
import ca.hellochat.business.IUserBusiness;
import ca.hellochat.common.DateUtil;
import ca.hellochat.common.StringUtil;
import ca.hellochat.common.exception.ExceptionType;
import ca.hellochat.common.exception.IChatException;
import ca.hellochat.controller.utils.jwt.JWTUtil;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;
import ca.hellochat.entity.Friend;
import ca.hellochat.entity.FriendInvite;
import ca.hellochat.entity.User;
import ca.hellochat.entity.domain.Page;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * 数据接受控制器
 * 
 **/
@Controller
@RequestMapping("/friend")
public class FriendController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	IFriendBusiness friendBusiness;
	@Autowired
	IUserBusiness userBusiness;
	@Autowired
	IFriendInviteBusiness friendInviteBusiness;
	
	/**
 	* 
 	* @param 获取好友详情
 	* @return page对象
 	**/
	@RequestMapping("/info")
	@ResponseBody
	public RespData info(@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("获取好友详情params:" + StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		
		String id = params.get("id");
		String hcno = params.get("hcno");
		String mobile = params.get("mobile");
		
		if(StringUtil.isEmpty(id)&&StringUtil.isEmpty(hcno)&&StringUtil.isEmpty(mobile)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"id/hcno/mobile不能同时为空"); 
		}
		
		User fUser = userBusiness.selectByParam(params);
		if(fUser==null || StringUtil.isEmpty(fUser.getId())) {
			throw new IChatException(ExceptionType.USER_AUTH_NOTFOUND,"根据参数未查询到好友信息"); 
		}
        params.clear();
        params.put("userId",user.getId());
        params.put("friendId",fUser.getId());
		Friend friend = friendBusiness.selectByParam(params);
		return success(friend);
	}
	
	/**
 	* 
 	* @param 查询好友列表
 	* @return page对象
 	**/
	@RequestMapping("/list")
	@ResponseBody
	public RespData list (@RequestParam Map<String,String> params,HttpServletRequest request){
		log.info("根据条件获取列表列表 -分页开始"+StringUtil.mapToString(params));
		User user = JWTUtil.getUserInfo(request);
		params.put("userId", user.getId());
		Page page = new Page(params);
		friendBusiness.selectPageByParam(params, page);
		return success(page);
	}
	
	@ApiOperation(value = "编辑好友信息",notes="编辑好友信息",httpMethod="POST")
	@RequestMapping("/edit")
	@ResponseBody
	public RespData edit(Friend friend,HttpServletRequest request){
		log.debug("根据条件获取列表列表 -分页开始"+(friend));
		User user = JWTUtil.getUserInfo(request);
		//验证是否好友关系
		Friend friendDb = friendBusiness.selectById(friend.getId());
		log.debug("获取到的friendDb:"+friendDb);
		if(friendDb==null || !friendDb.getUserId().equals(friend.getUserId()) || !friendDb.getUserId().equals(user.getId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"好友关系异常"); 
		}
		if(friend.getWakeFlag()!=null) {
			friendDb.setWakeFlag(friend.getWakeFlag());//是否免打扰
		}
		if(StringUtil.isNotEmpty(friend.getRname())) {
			friendDb.setRname(friend.getRname());//备注
		}
		friendBusiness.updateById(friendDb);
		return success(friendDb);
	}
	/**
	 * 删除好友
	 * @param friend
	 * @param request
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public RespData del(String id,HttpServletRequest request){
		log.debug("删除好友关系：id::"+id);
		User user = JWTUtil.getUserInfo(request);
		//验证是否好友关系
		Friend friendDb = friendBusiness.selectById(id);
		log.debug("获取到的friendDb:"+friendDb);
		if(friendDb==null || !friendDb.getUserId().equals(user.getId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"好友关系异常"); 
		}
		friendBusiness.delFriend(friendDb);
		return success();
	}
	
	@ApiOperation(value = "查看好友关系",notes="查看好友关系",httpMethod="POST")
	@RequestMapping("/isFriend")
	@ResponseBody
	public RespData isFriend(String userId,HttpServletRequest request){
		log.debug("查看好友信息userId:"+userId);
		User user = JWTUtil.getUserInfo(request);
		
		if(StringUtil.isEmpty(userId)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"参数不能为空，userId"); 
		}
		boolean result = friendBusiness.isFriend(user.getId(),userId);
		return success(result?1:0);
	}
	/**
 	* 发送好友添加申请
 	**/
	@ApiOperation(value = "发送好友添加申请",notes="发送好友添加申请",httpMethod="POST")
	@RequestMapping("/request")
	@ResponseBody
	public RespData request(FriendInvite fi,HttpServletRequest request){
		log.info("发送好友添加申请 friendInvite:"+fi);
		User user = JWTUtil.getUserInfo(request);
		fi.setSendId(user.getId());//将session里面的发起人灌到)
		if(StringUtil.isEmpty(fi.getReceiveId())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数：receiveId");
		}
		if(user.getId().equals(fi.getReceiveId())) {
			throw new IChatException(ExceptionType.IM_FRIEND_ADDMYSELF,"不能添加自己为好友");
		}
		if(StringUtil.isEmpty(fi.getReqMsg())) {
			throw new IChatException(ExceptionType.INVALID_PARAMS,"缺少必要的参数：reqMsg");
		}
		friendInviteBusiness.request(fi);
		return success(fi);
	}
	
	/**
 	* 发送好友添加申请
 	**/
	@ApiOperation(value = "添加好友申请反馈",notes="发送好友添加申请",httpMethod="POST")
	@RequestMapping("/inviteResp")
	@ResponseBody
	public RespData inviteResp(FriendInvite fi,HttpServletRequest request){
		log.info("发送好友添加申请 friendInvite::"+fi);
		User user = JWTUtil.getUserInfo(request);
//		fi.setReceiveId(user.getId());
		String id = fi.getId();
		if(id==null || StringUtil.isEmpty(id)) {
			throw new IChatException(ExceptionType.INVALID_PARAMS, "缺少必要的参数，fi.id");
		}
		FriendInvite fiUpdate = friendInviteBusiness.selectById(id);
		log.info("数据库中获取到的好友申请：fiUpdate::"+fiUpdate);
		if(fiUpdate==null || StringUtil.isEmpty(fiUpdate.getId())) {
			throw new IChatException(ExceptionType.FAILED_RESPONSE, "服务器中未发现此id对应的好友申请信息");
		}
		if(fiUpdate.getSts()!=1) {//如果数据库中没有1则说明此消息已经过期
			throw new IChatException(ExceptionType.FAILED_RESPONSE, "请求已过期");
		}
		int sts = fi.getSts();
		if(!"1,2".contains(sts+"")) {
			throw new IChatException(ExceptionType.FAILED_PARAMS, "数据校验错误::sts");
		}
		if(StringUtil.isEmpty(fiUpdate.getReceiveId()) || !fiUpdate.getReceiveId().equals(user.getId())) {
			log.error("fi::"+fi+",user::"+user);
			throw new IChatException(ExceptionType.FAILED_AUTHORITY, "数据校验错误::userId");
		}
		fiUpdate.setId(fi.getId());
		fiUpdate.setSts(fi.getSts());
		fiUpdate.setResTime(DateUtil.getNowDate());
		fiUpdate.setResMsg(fi.getResMsg());
		friendInviteBusiness.inviteResp(fiUpdate);
		return success();
	}
	}
