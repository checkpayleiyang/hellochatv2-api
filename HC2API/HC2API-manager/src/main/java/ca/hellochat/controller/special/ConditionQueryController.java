package ca.hellochat.controller.special;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;

import io.swagger.annotations.ApiOperation;

/**
 * 通过条件  查询相关数据
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/conditionQuery")
public class ConditionQueryController extends BaseController{
	private Logger log = LoggerFactory.getLogger(this.getClass());
//	@Autowired
//	IOrgAreasBusiness orgAreasBusiness;
//	@Autowired
//	ITermBusiness termBusiness;
//	@Autowired
//	ITermTypeBusiness termTypeBusiness;
	

	/**
 	* 
 	* @param id
 	* @return Term对象
 	**/
	@ApiOperation(value = "获取所有运营商" ,notes="获取所有运营商",httpMethod="GET")
	@RequestMapping("/queryAllAreas")
	@ResponseBody
	public RespData queryAllAreas(){
//		log.info("获取所有运营商");
//		OrgAreas orgAreas = orgAreasBusiness.queryAllAreas();
//		if(StringUtils.isEmpty(orgAreas)){ 
//			return super.error("4");
//		}
		return success();
	}
	
	/**
 	* 
 	* 通过运营商查询 下面的所有机器 
 	* @return RespData对象  机器列表 + 机器类型列表
 	**/
	@ApiOperation(value = "通过运营商查询 下面的所有机器 ",notes=" 通过运营商查询 下面的所有机器 ",httpMethod="POST")
	@RequestMapping("/queryOreasTerm")
	@ResponseBody
	public RespData queryOreasTerm(){
//		log.info("通过运营商查询 下面的所有机器id:"+orgAreas.getLocalAreaId());
//		if(StringUtils.isEmpty(orgAreas) || StringUtils.isEmpty(orgAreas.getLocalAreaId())){
//			return super.error("1");
//		}
//		Map<String,String> param=new HashMap<String,String>();
//		param.put("localAreaId", ""+orgAreas.getLocalAreaId());
//		List<Term> termList=termBusiness.selectListByParam(param);
//		if(termList==null || termList.size()==0){
//			return super.success();
//		}
//		List<Map<String,String>> typeList=new ArrayList<Map<String,String>>();
//		List<Term> resultTermList=new ArrayList<Term>();
//		for(int i=0;i<termList.size();i++){
//			Term term=termList.get(i);
//			if(term!=null && term.getSts()!=0){
//				if(!hasContainKey(typeList,""+term.getTermType())){
//					if(term.getTtermType()!=null){
//						Map<String,String> termMap =new HashMap<String,String>();
//						termMap.put("termTypeId", ""+term.getTermType());
//						termMap.put("termTypeName", ""+term.getTtermType().getTypeName());
//						typeList.add(termMap);
//					}
//				}
//				resultTermList.add(term);
//			}
//		}
//		if(resultTermList.size()>0){
//			Map<String,Object> rMap=new HashMap<String,Object>();
//			rMap.put("termList", resultTermList);
//			rMap.put("typeList", typeList);
//			return success(rMap);
//		}
		return super.error("4");
	}
	/**
 	* 
 	* 通过运营商+类型 查询 下面的所有机器 
 	* @return RespData对象  机器列表 + 机器类型列表
 	**/
	@ApiOperation(value = "通过运营商+类型 查询 下面的所有机器  ",notes="通过运营商+类型 查询 下面的所有机器  ",httpMethod="POST")
	@RequestMapping("/queryOreasTermOnTermType")
	@ResponseBody
	public RespData queryOreasTermOnTermType(@RequestBody Map<String,String> param){
//		log.info("通过运营商+类型 查询 下面的所有机器 param:"+StringUtil.mapToString(param));
//		if(param==null || param.size()==0 
//				|| StringUtils.isEmpty(param.get("localAreaId"))
//				|| StringUtils.isEmpty(param.get("termType"))){
//			return super.error("1");
//		}
//		if("all".equals(param.get("termType"))){
//			 OrgAreas orgAreas =new OrgAreas();
//			 orgAreas.setLocalAreaId(Integer.valueOf(param.get("localAreaId")));
//			return queryOreasTerm(orgAreas);
//		}
//		List<Term> termList=termBusiness.selectListByParam(param);
		return super.success();
	}
	
	public static boolean hasContainKey(List<Map<String,String>> typeList,String info){
		if(typeList!=null && typeList.size()>0){
			for(Map<String,String> m :typeList){
				if(m!=null && m.size()>0){
					for(String key :m.keySet()){
						String value=m.get(key);
						if(value!=null && value.equals(info)){
							return true;
						}
					}
					
				}
			}
		}
		return false;
	}
}
