package ca.hellochat.controller.special;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//import ca.hellochat.business.IFileUploadBusiness;
import ca.hellochat.controller.utils.web.BaseController;
import ca.hellochat.controller.utils.web.RespData;

import io.swagger.annotations.ApiOperation;
/**
 * 上传文件
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/fileUpload")
public class FileUploadController extends BaseController{
//	@Autowired
//	private IFileUploadBusiness fileUploadBusiness;
	/**
 	* 
 	* @param id
 	* @return Term对象
 	**/
	@ApiOperation(value = "获取所有运营商" ,notes="获取所有运营商",httpMethod="POST")
	@RequestMapping("/uploadFile")
	@ResponseBody
	public RespData uploadFile(HttpServletRequest request, HttpServletResponse response) {
//		String result=fileUploadBusiness.uploadFile(request, response);
//		if("success".equals(result)){
//			return super.success();
//		}else{
			return super.error();
//		}
	}
	/**
 	* 
 	* @param id
 	* @return Term对象
 	**/
	@ApiOperation(value = "获取所有运营商" ,notes="获取所有运营商",httpMethod="POST")
	@RequestMapping("/uploadPic")
	@ResponseBody
	private RespData uploadPic(@RequestBody Map<String,String> param) {
//		String name=fileUploadBusiness.uploadPic(param);
//    	if(name==null){
    		return super.error();
//    	}else{
//    		return super.success(name);
//    	}
	}

}
