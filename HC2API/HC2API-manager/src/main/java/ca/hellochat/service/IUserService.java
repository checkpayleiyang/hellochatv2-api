package ca.hellochat.service;
import ca.hellochat.entity.User;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * IUserService服务接口
 * 
 **/
public interface IUserService{
	public User selectById(String id);

	/** 对象查询（根据条件查询）**/
	public User selectByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<User> selectListByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<User> selectPageByParam(Map<String,String> param,Page page);

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(User user);

	/** 修改（根据主键ID修改）**/
	public int updateById(User user);

	/** 批量修改（根据条件修改）**/
	public int updateByParam(User user,Map<String,String> param);

}