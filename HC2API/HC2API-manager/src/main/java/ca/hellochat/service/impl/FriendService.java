package ca.hellochat.service.impl;
import ca.hellochat.entity.Friend;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IFriendService;
import ca.hellochat.dao.IFriendDao;
/**
 * 
 * 通讯录信息服务实现类
 * 
 **/
@Service
public class FriendService implements IFriendService{
	@Autowired
	private IFriendDao friendDao;

	/** 修改（根据主键ID修改）**/
	public Friend selectById(String id){
		return friendDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public Friend selectByParam(Map<String,String> param){
		Page page=new Page();
		List<Friend> list = this.selectPageByParam(param,page);
		Friend friend = null;
		if(list != null && list.size() > 0){
			friend = list.get(0);
		}
		return friend;
	}

	/** 列表查询（根据条件查询）**/
	public List<Friend>  selectListByParam(Map<String,String> param){
		return friendDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<Friend> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<Friend> list = friendDao.selectListByParam(param);
		int totalCount = friendDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return friendDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return friendDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<Friend> friendList = this.selectListByParam(param);
		if(friendList==null || friendList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Friend friend:friendList){
			if(friend.getId()==null){continue;}
			count +=friendDao.deleteById(friend.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Friend friend){
		return friendDao.insertSelective(friend);
	}
	public int updateById(Friend friend){
		return friendDao.updateById(friend);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(Friend friend,Map<String,String> param){
		List<Friend> friendList = this.selectListByParam(param);
		if(friendList==null || friendList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Friend friendTemp:friendList){
			if(friendTemp.getId()==null){continue;}
			count +=this.updateById(friendTemp);
		}
		return count;
	}

}