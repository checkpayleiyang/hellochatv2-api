package ca.hellochat.service;
import ca.hellochat.entity.FriendInvite;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * IFriendInviteService服务接口
 * 
 **/
public interface IFriendInviteService{
	public FriendInvite selectById(String id);

	/** 对象查询（根据条件查询）**/
	public FriendInvite selectByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<FriendInvite> selectListByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<FriendInvite> selectPageByParam(Map<String,String> param,Page page);

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(FriendInvite FriendInvite);

	/** 修改（根据主键ID修改）**/
	public int updateById(FriendInvite FriendInvite);

	/** 批量修改（根据条件修改）**/
	public int updateByParam(FriendInvite FriendInvite,Map<String,String> param);

}