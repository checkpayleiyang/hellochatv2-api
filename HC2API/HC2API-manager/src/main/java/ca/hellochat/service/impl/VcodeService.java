package ca.hellochat.service.impl;
import ca.hellochat.entity.Vcode;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IVcodeService;
import ca.hellochat.dao.IVcodeDao;
/**
 * 
 * 验证码信息服务实现类
 * 
 **/
@Service
public class VcodeService implements IVcodeService{
	@Autowired
	private IVcodeDao vcodeDao;

	/** 修改（根据主键ID修改）**/
	public Vcode selectById(String id){
		return vcodeDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public Vcode selectByParam(Map<String,String> param){
		Page page=new Page();
		List<Vcode> list = this.selectPageByParam(param,page);
		Vcode vcode = null;
		if(list != null && list.size() > 0){
			vcode = list.get(0);
		}
		return vcode;
	}

	/** 列表查询（根据条件查询）**/
	public List<Vcode>  selectListByParam(Map<String,String> param){
		return vcodeDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<Vcode> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<Vcode> list = vcodeDao.selectListByParam(param);
		int totalCount = vcodeDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return vcodeDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return vcodeDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<Vcode> vcodeList = this.selectListByParam(param);
		if(vcodeList==null || vcodeList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Vcode vcode:vcodeList){
			if(vcode.getId()==null){continue;}
			count +=vcodeDao.deleteById(vcode.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Vcode vcode){
		return vcodeDao.insertSelective(vcode);
	}
	public int updateById(Vcode vcode){
		return vcodeDao.updateById(vcode);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(Vcode vcode,Map<String,String> param){
		List<Vcode> vcodeList = this.selectListByParam(param);
		if(vcodeList==null || vcodeList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Vcode vcodeTemp:vcodeList){
			if(vcodeTemp.getId()==null){continue;}
			count +=this.updateById(vcodeTemp);
		}
		return count;
	}

}