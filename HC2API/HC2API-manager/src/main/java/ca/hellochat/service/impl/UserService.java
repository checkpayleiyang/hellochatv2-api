package ca.hellochat.service.impl;
import ca.hellochat.entity.User;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IUserService;
import ca.hellochat.dao.IUserDao;
/**
 * 
 * 用户信息服务实现类
 * 
 **/
@Service
public class UserService implements IUserService{
	@Autowired
	private IUserDao userDao;

	/** 修改（根据主键ID修改）**/
	public User selectById(String id){
		return userDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public User selectByParam(Map<String,String> param){
		Page page=new Page();
		List<User> list = this.selectPageByParam(param,page);
		User user = null;
		if(list != null && list.size() > 0){
			user = list.get(0);
		}
		return user;
	}

	/** 列表查询（根据条件查询）**/
	public List<User>  selectListByParam(Map<String,String> param){
		return userDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<User> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<User> list = userDao.selectListByParam(param);
		int totalCount = userDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return userDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return userDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<User> userList = this.selectListByParam(param);
		if(userList==null || userList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(User user:userList){
			if(user.getId()==null){continue;}
			count +=userDao.deleteById(user.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(User user){
		return userDao.insertSelective(user);
	}
	public int updateById(User user){
		return userDao.updateById(user);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(User user,Map<String,String> param){
		List<User> userList = this.selectListByParam(param);
		if(userList==null || userList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(User userTemp:userList){
			if(userTemp.getId()==null){continue;}
			count +=this.updateById(userTemp);
		}
		return count;
	}

}