package ca.hellochat.service.impl;
import ca.hellochat.entity.Log;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.ILogService;
import ca.hellochat.dao.ILogDao;
/**
 * 
 * 系统日志服务实现类
 * 
 **/
@Service
public class LogService implements ILogService{
	@Autowired
	private ILogDao logDao;

	/** 修改（根据主键ID修改）**/
	public Log selectById(String id){
		return logDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public Log selectByParam(Map<String,String> param){
		Page page=new Page();
		List<Log> list = this.selectPageByParam(param,page);
		Log log = null;
		if(list != null && list.size() > 0){
			log = list.get(0);
		}
		return log;
	}

	/** 列表查询（根据条件查询）**/
	public List<Log>  selectListByParam(Map<String,String> param){
		return logDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<Log> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<Log> list = logDao.selectListByParam(param);
		int totalCount = logDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return logDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return logDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<Log> logList = this.selectListByParam(param);
		if(logList==null || logList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Log log:logList){
			if(log.getId()==null){continue;}
			count +=logDao.deleteById(log.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(Log log){
		return logDao.insertSelective(log);
	}
	public int updateById(Log log){
		return logDao.updateById(log);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(Log log,Map<String,String> param){
		List<Log> logList = this.selectListByParam(param);
		if(logList==null || logList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(Log logTemp:logList){
			if(logTemp.getId()==null){continue;}
			count +=this.updateById(logTemp);
		}
		return count;
	}

}