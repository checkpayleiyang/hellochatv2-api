package ca.hellochat.service.impl;
import ca.hellochat.entity.UserMsg;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IUserMsgService;
import ca.hellochat.dao.IUserMsgDao;
/**
 * 
 * 用户消息表服务实现类
 * 
 **/
@Service
public class UserMsgService implements IUserMsgService{
	@Autowired
	private IUserMsgDao userMsgDao;

	/** 修改（根据主键ID修改）**/
	public UserMsg selectById(String id){
		return userMsgDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public UserMsg selectByParam(Map<String,String> param){
		Page page=new Page();
		List<UserMsg> list = this.selectPageByParam(param,page);
		UserMsg userMsg = null;
		if(list != null && list.size() > 0){
			userMsg = list.get(0);
		}
		return userMsg;
	}

	/** 列表查询（根据条件查询）**/
	public List<UserMsg>  selectListByParam(Map<String,String> param){
		return userMsgDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<UserMsg> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<UserMsg> list = userMsgDao.selectListByParam(param);
		int totalCount = userMsgDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return userMsgDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return userMsgDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<UserMsg> userMsgList = this.selectListByParam(param);
		if(userMsgList==null || userMsgList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(UserMsg userMsg:userMsgList){
			if(userMsg.getId()==null){continue;}
			count +=userMsgDao.deleteById(userMsg.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(UserMsg userMsg){
		return userMsgDao.insertSelective(userMsg);
	}
	public int updateById(UserMsg userMsg){
		return userMsgDao.updateById(userMsg);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(UserMsg userMsg,Map<String,String> param){
		List<UserMsg> userMsgList = this.selectListByParam(param);
		if(userMsgList==null || userMsgList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(UserMsg userMsgTemp:userMsgList){
			if(userMsgTemp.getId()==null){continue;}
			userMsg.setId(userMsgTemp.getId());
			count +=this.updateById(userMsg);
		}
		return count;
	}

}