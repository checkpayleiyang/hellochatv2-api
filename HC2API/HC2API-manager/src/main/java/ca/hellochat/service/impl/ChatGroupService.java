package ca.hellochat.service.impl;
import ca.hellochat.entity.ChatGroup;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IChatGroupService;
import ca.hellochat.dao.IChatGroupDao;
/**
 * 
 * 群组信息服务实现类
 * 
 **/
@Service
public class ChatGroupService implements IChatGroupService{
	@Autowired
	private IChatGroupDao chatGroupDao;

	/** 修改（根据主键ID修改）**/
	public ChatGroup selectById(String id){
		return chatGroupDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public ChatGroup selectByParam(Map<String,String> param){
		Page page=new Page();
		List<ChatGroup> list = this.selectPageByParam(param,page);
		ChatGroup chatGroup = null;
		if(list != null && list.size() > 0){
			chatGroup = list.get(0);
		}
		return chatGroup;
	}

	/** 列表查询（根据条件查询）**/
	public List<ChatGroup>  selectListByParam(Map<String,String> param){
		return chatGroupDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<ChatGroup> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<ChatGroup> list = chatGroupDao.selectListByParam(param);
		int totalCount = chatGroupDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return chatGroupDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return chatGroupDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<ChatGroup> chatGroupList = this.selectListByParam(param);
		if(chatGroupList==null || chatGroupList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(ChatGroup chatGroup:chatGroupList){
			if(chatGroup.getId()==null){continue;}
			count +=chatGroupDao.deleteById(chatGroup.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatGroup chatGroup){
		return chatGroupDao.insertSelective(chatGroup);
	}
	public int updateById(ChatGroup chatGroup){
		return chatGroupDao.updateById(chatGroup);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(ChatGroup chatGroup,Map<String,String> param){
		List<ChatGroup> chatGroupList = this.selectListByParam(param);
		if(chatGroupList==null || chatGroupList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(ChatGroup chatGroupTemp:chatGroupList){
			if(chatGroupTemp.getId()==null){continue;}
			count +=this.updateById(chatGroupTemp);
		}
		return count;
	}

}