package ca.hellochat.service.impl;
import ca.hellochat.entity.ChatNo;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IChatNoService;
import ca.hellochat.dao.IChatNoDao;
/**
 * 
 * 服务实现类
 * 
 **/
@Service
public class ChatNoService implements IChatNoService{
	@Autowired
	private IChatNoDao chatNoDao;

	/** 修改（根据主键ID修改）**/
	public ChatNo selectById(String id){
		return chatNoDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public ChatNo selectByParam(Map<String,String> param){
		Page page=new Page();
		List<ChatNo> list = this.selectPageByParam(param,page);
		ChatNo chatNo = null;
		if(list != null && list.size() > 0){
			chatNo = list.get(0);
		}
		return chatNo;
	}

	/** 列表查询（根据条件查询）**/
	public List<ChatNo>  selectListByParam(Map<String,String> param){
		return chatNoDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<ChatNo> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<ChatNo> list = chatNoDao.selectListByParam(param);
		int totalCount = chatNoDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return chatNoDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return chatNoDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<ChatNo> chatNoList = this.selectListByParam(param);
		if(chatNoList==null || chatNoList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(ChatNo chatNo:chatNoList){
			if(chatNo.getId()==null){continue;}
			count +=chatNoDao.deleteById(chatNo.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatNo chatNo){
		return chatNoDao.insertSelective(chatNo);
	}
	public int updateById(ChatNo chatNo){
		return chatNoDao.updateById(chatNo);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(ChatNo chatNo,Map<String,String> param){
		List<ChatNo> chatNoList = this.selectListByParam(param);
		if(chatNoList==null || chatNoList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(ChatNo chatNoTemp:chatNoList){
			if(chatNoTemp.getId()==null){continue;}
			count +=this.updateById(chatNoTemp);
		}
		return count;
	}

}