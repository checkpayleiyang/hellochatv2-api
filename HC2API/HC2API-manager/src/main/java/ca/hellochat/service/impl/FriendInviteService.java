package ca.hellochat.service.impl;
import ca.hellochat.entity.FriendInvite;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IFriendInviteService;
import ca.hellochat.dao.IFriendInviteDao;
/**
 * 
 * 新朋友信息服务实现类
 * 
 **/
@Service
public class FriendInviteService implements IFriendInviteService{
	@Autowired
	private IFriendInviteDao FriendInviteDao;

	/** 修改（根据主键ID修改）**/
	public FriendInvite selectById(String id){
		return FriendInviteDao.selectById(id);
	}

	/** 对象查询（根据条件查询对象）**/
	public FriendInvite selectByParam(Map<String,String> param){
		Page page=new Page();
		List<FriendInvite> list = this.selectPageByParam(param,page);
		FriendInvite FriendInvite = null;
		if(list != null && list.size() > 0){
			FriendInvite = list.get(0);
		}
		return FriendInvite;
	}

	/** 列表查询（根据条件查询）**/
	public List<FriendInvite>  selectListByParam(Map<String,String> param){
		return FriendInviteDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<FriendInvite> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<FriendInvite> list = FriendInviteDao.selectListByParam(param);
		int totalCount = FriendInviteDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return FriendInviteDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return FriendInviteDao.deleteById(id);
	}


	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<FriendInvite> FriendInviteList = this.selectListByParam(param);
		if(FriendInviteList==null || FriendInviteList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(FriendInvite FriendInvite:FriendInviteList){
			if(FriendInvite.getId()==null){continue;}
			count +=FriendInviteDao.deleteById(FriendInvite.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(FriendInvite FriendInvite){
		return FriendInviteDao.insertSelective(FriendInvite);
	}
	public int updateById(FriendInvite FriendInvite){
		return FriendInviteDao.updateById(FriendInvite);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(FriendInvite FriendInvite,Map<String,String> param){
		List<FriendInvite> FriendInviteList = this.selectListByParam(param);
		if(FriendInviteList==null || FriendInviteList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(FriendInvite FriendInviteTemp:FriendInviteList){
			if(FriendInviteTemp.getId()==null){continue;}
			count +=this.updateById(FriendInviteTemp);
		}
		return count;
	}

}