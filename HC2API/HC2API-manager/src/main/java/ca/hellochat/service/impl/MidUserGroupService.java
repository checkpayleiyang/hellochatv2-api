package ca.hellochat.service.impl;
import ca.hellochat.entity.MidUserGroup;
import java.util.Map;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.hellochat.entity.domain.Page;
import ca.hellochat.service.IMidUserGroupService;
import ca.hellochat.dao.IMidUserGroupDao;
/**
 * 
 * 用户群组关系信息服务实现类
 * 
 **/
@Service
public class MidUserGroupService implements IMidUserGroupService{
	@Autowired
	private IMidUserGroupDao midUserGroupDao;

	/** 修改（根据主键ID修改）**/
	/** 对象查询（根据条件查询对象）**/
	public MidUserGroup selectById(String id){
		MidUserGroup midUserGroup = midUserGroupDao.selectById(id);
		return midUserGroup;
	}

	/** 对象查询（根据条件查询对象）**/
	public MidUserGroup selectByParam(Map<String,String> param){
		Page page=new Page();
		List<MidUserGroup> list = this.selectPageByParam(param,page);
		MidUserGroup midUserGroup = null;
		if(list != null && list.size() > 0){
			midUserGroup = list.get(0);
		}
		return midUserGroup;
	}

	/** 列表查询（根据条件查询）**/
	public List<MidUserGroup>  selectListByParam(Map<String,String> param){
		return midUserGroupDao.selectListByParam(param);
	}

	/** 分页查询（根据条件查询）**/
	public List<MidUserGroup> selectPageByParam(Map<String,String> param,Page page){
		param.put("offset", String.valueOf(page.getStartCount()));
		param.put("limit", String.valueOf(page.getPageSize()));
		List<MidUserGroup> list = midUserGroupDao.selectListByParam(param);
		int totalCount = midUserGroupDao.selectCountByParam(param);
		page.setTotalCount(totalCount);
		return list;
	}

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param){
		return midUserGroupDao.selectCountByParam(param);
	}

	/** 查询（根据主键查询）**/
	public int deleteById(String id){
		return midUserGroupDao.deleteById(id);
	}

	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param){
		List<MidUserGroup> midUserGroupList = this.selectListByParam(param);
		if(midUserGroupList==null || midUserGroupList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(MidUserGroup midUserGroup:midUserGroupList){
			if(midUserGroup.getId()==null){continue;}
			count +=midUserGroupDao.deleteById(midUserGroup.getId());
		}
		return count;
	}

	/** 添加（匹配有值的字段）**/
	public int insertSelective(MidUserGroup midUserGroup){
		return midUserGroupDao.insertSelective(midUserGroup);
	}

	/** 修改（根据主键ID修改）**/
	public int updateById(MidUserGroup midUserGroup){
		return midUserGroupDao.updateById(midUserGroup);
	}


	/** 批量修改（根据条件批量修改）**/
	public int updateByParam(MidUserGroup midUserGroup,Map<String,String> param){
		List<MidUserGroup> midUserGroupList = this.selectListByParam(param);
		if(midUserGroupList==null || midUserGroupList.isEmpty()){
			return 0;
		}
		int count = 0;
		for(MidUserGroup midUserGroupTemp:midUserGroupList){
			if(midUserGroupTemp.getId()==null){continue;}
			count +=this.updateById(midUserGroupTemp);
		}
		return count;
	}

}