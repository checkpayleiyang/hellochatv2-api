package ca.hellochat.service;
import ca.hellochat.entity.ChatNo;
import java.util.Map;
import java.util.List;
import ca.hellochat.entity.domain.Page;
/**
 * 
 * IChatNoService服务接口
 * 
 **/
public interface IChatNoService{
	public ChatNo selectById(String id);

	/** 对象查询（根据条件查询）**/
	public ChatNo selectByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<ChatNo> selectListByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<ChatNo> selectPageByParam(Map<String,String> param,Page page);

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键ID删除）**/
	public int deleteById(String id);

	/** 删除（根据条件删除）**/
	public int deleteByParam(Map<String,String> param);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatNo chatNo);

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatNo chatNo);

	/** 批量修改（根据条件修改）**/
	public int updateByParam(ChatNo chatNo,Map<String,String> param);

}