package ca.hellochat.dao;
import ca.hellochat.entity.User;
import java.util.Map;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
/**
 * 
 * 用户信息数据库操作接口类
 * 
 **/
@Mapper
public interface IUserDao{
	/** 查询（根据主键查询对象）**/
	public User selectById(String id);

	/** 查询（根据条件查询对象）**/
	public User selectByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<User>  selectListByParam(Map<String,String> param);

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键删除）**/
	public int deleteById(String id);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(User user);

	/** 修改（根据主键ID修改）**/
	public int updateById(User user);
}