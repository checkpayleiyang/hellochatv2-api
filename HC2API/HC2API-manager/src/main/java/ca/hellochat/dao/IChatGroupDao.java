package ca.hellochat.dao;
import ca.hellochat.entity.ChatGroup;
import java.util.Map;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
/**
 * 
 * 群组信息数据库操作接口类
 * 
 **/
@Mapper
public interface IChatGroupDao{
	/** 查询（根据主键查询对象）**/
	public ChatGroup selectById(String id);

	/** 查询（根据条件查询对象）**/
	public ChatGroup selectByParam(Map<String,String> param);

	/** 查询（根据条件查询）**/
	public List<ChatGroup>  selectListByParam(Map<String,String> param);

	/** 查询（根据条件查询数量）**/
	public int selectCountByParam(Map<String,String> param);

	/** 删除（根据主键删除）**/
	public int deleteById(String id);

	/** 添加（匹配有值的字段）**/
	public int insertSelective(ChatGroup chatGroup);

	/** 修改（根据主键ID修改）**/
	public int updateById(ChatGroup chatGroup);
}