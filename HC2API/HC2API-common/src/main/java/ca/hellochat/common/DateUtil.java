package ca.hellochat.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.apache.commons.lang3.time.DateUtils;

/**
 * 描述：日期处理常用类
 */
public class DateUtil {
	public static String yyyy_MM_ddHHmmss = "yyyy-MM-dd HH:mm:ss";
	public static String yyyy_MM_ddHHmm = "yyyy-MM-dd HH:mm";
	public static String yyyy_MM_dd = "yyyy-MM-dd";
	public static String yyyyMMddHHmmss = "yyyyMMddHHmmss";

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Date getNowDate() {
//		DateUtils.n
		return new Date();
	}
	
	public static Date getQueryDate() {
		return getUTCTimeStr();
	}
	public static Date getUTCTimeStr() {
		// 1、取得本地时间：
		Calendar cal = Calendar.getInstance() ;
//		// 2、取得时间偏移量：
		long zoneOffset = cal.get(java.util.Calendar.ZONE_OFFSET);
//		// 3、取得夏令时差：
		long dstOffset = cal.get(java.util.Calendar.DST_OFFSET);
//		// 4、从本地时间里扣除这些差量，即可以取得UTC时间：
		return new Date(new Date().getTime() -(zoneOffset + dstOffset));
	}


	/**
	 * 格式化时间
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDate(Date date) {
		return formatDate(date, yyyy_MM_ddHHmmss);
	}

	/**
	 * 格式化时间
	 * 
	 * @param date
	 * @return
	 */
	public static String formatDate(Date date, String format) {
		SimpleDateFormat dataFormat = new SimpleDateFormat(format);
		return dataFormat.format(date);
	}

	/**
	 * 格式化当前时间
	 * 
	 * @param date
	 * @return
	 */
	public static String formatNowDate() {
		return formatDate(getNowDate());
	}

	/**
	 * 将指定格式的字符串转换为时间
	 * 
	 * @param str
	 * @param format
	 * @return
	 */
	public static Date str2Date(String str, String format) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return getNowDate();
		}
	}

	/**
	 * 根据传过来的字符串型的字数，转换成对应的日期
	 * 
	 * @param str
	 * @return
	 */
	public static Date str2Date(String str) {
		if (str.length() == 10) {
			return str2Date(str, yyyy_MM_dd);
		}
		if (str.length() == 16) {
			return str2Date(str, yyyy_MM_ddHHmm);
		}
		if (str.length() == 19) {
			return str2Date(str, yyyy_MM_ddHHmmss);
		}
		return str2Date(str, yyyy_MM_ddHHmmss);
	}

	/**
	 * 计算两个日期之间的天数
	 * 
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @return
	 */
	public static int calcDays(String startDate, String endDate) {
		int days = 1;
		try {
			long start = str2Date(startDate).getTime();
			long end = str2Date(endDate).getTime();
			days = (int) ((end - start) / (24 * 60 * 60 * 1000));
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return days;
	}

	/**
	 * 计算两个日期之间的天数
	 *
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @return
	 */
	public static int calcDay(Date startDate, Date endDate) {
		int days = 1;
		try {
			long start = startDate.getTime();
			long end = endDate.getTime();
			days = (int) ((end - start) / (24 * 60 * 60 * 1000));
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return days;
	}
	
	/**
	 * 功能：指定日期加上指定个数的年份
	 *
	 * @param date
	 *            日期
	 * @param month
	 *            添加月的个数
	 * @return 返回相加后的日期
	 */
	public static Date addYear(Date date, int year) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.YEAR, year);
		return c.getTime();
	}

	/**
	 * 功能：指定日期加上指定个数的月份
	 *
	 * @param date
	 *            日期
	 * @param month
	 *            添加月的个数
	 * @return 返回相加后的日期
	 */
	public static Date addMonth(Date date, int month) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, month);
		return c.getTime();
	}

	/**
	 * 功能：指定日期加上指定天数
	 * 
	 * @param date
	 *            日期
	 * @param day
	 *            天数
	 * @return 返回相加后的日期
	 */
	public static Date addDate(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	/**
	 * 功能：指定日期加上指定分钟数
	 * 
	 * @param date
	 *            日期
	 * @param minute
	 *            分钟
	 * @return 返回相加后的日期
	 */
	public static Date addMinute(Date date, int minute) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) + ((long) minute) * 60 * 1000);
		return c.getTime();
	}

	/**
	 * 
	 * 功能：添加指定秒杀的时间
	 * 
	 * @param date
	 * @param second
	 * @return
	 */
	public static Date addSecond(Date date, int second) {
		long s = date.getTime();
		s = s + second * 1000;
		return new Date(s);
	}

	/**
	 * 功能：指定日期减去指定天数
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date diffDate(Date date, int day) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(getMillis(date) - ((long) day) * 24 * 3600 * 1000);
		return c.getTime();
	}

	/**
	 * 
	 * 功能：分钟相差 minute的时间值
	 * 
	 * @param date
	 * @param minute
	 * @return
	 */
	public static Date getDateByMinuteAdd(Date date, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}

	/**
	 * 功能:两个日期相隔天数
	 * 
	 * @param startDate
	 *            开始日期
	 * @param endDate
	 *            结束日期
	 * @return 返回相减后的日期
	 */
	public static int diffDate(Date startDate, Date endDate) {
		long endMillis = endDate.getTime();
		long startMillis = startDate.getTime();
		long s = (endMillis - startMillis) / (24 * 3600 * 1000);
		return (int) s;
	}

	/**
	 * 功能：返回年
	 * 
	 * @param date
	 * @return
	 */
	public static int getYear(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.YEAR);

	}

	/**
	 * 功能：返回月
	 * 
	 * @param date
	 * @return
	 */
	public static int getMonth(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH) + 1;
	}

	/**
	 * 功能：返回日
	 * 
	 * @param date
	 * @return
	 */
	public static int getDay(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 功能：返回小时
	 * 
	 * @param date
	 * @return
	 */
	public static int getHour(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 功能：返回分
	 * 
	 * @param date
	 * @return
	 */
	public static int getMinute(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MINUTE);
	}

	/**
	 * 功能：返回星期 1：星期一，2:星期二 ... 6:星期六 7:星期日
	 * 
	 * @param date
	 * @return
	 */
	public static int getChinaWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int week = c.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 0) {
			return 7;
		} else {
			return week;
		}
	}

	/**
	 * 功能：返回秒
	 * 
	 * @param date
	 * @return
	 */
	public static int getSecond2(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.SECOND);
	}

	/**
	 * 功能：返回毫秒
	 * 
	 * @param date
	 * @return
	 */
	public static long getMillis(Date date) {
		if (date == null) {
			date = new Date();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getTimeInMillis();
	}

	/**
	 * 功能：获取当前月的第一天日期
	 * 
	 * @return
	 */
	public static Date getMonFirstDay() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.set(getYear(date), getMonth(date) - 1, 1);
		return c.getTime();
	}

	/**
	 * 功能：获取当前月的最后一天日期
	 * 
	 * @return
	 */
	public static Date getMonLastDay() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.set(getYear(date), getMonth(date), 1);
		c.setTimeInMillis(c.getTimeInMillis() - (24 * 3600 * 1000));
		return c.getTime();
	}

	/**
	 * 功能：获取上个月的最后一天日期
	 * 
	 * @return
	 */
	public static Date getMonUpDay() {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.set(getYear(date), getMonth(date) - 1, 1);
		c.setTimeInMillis(c.getTimeInMillis() - (24 * 3600 * 1000));
		return c.getTime();
	}

	/***
	 * 获得本月的第一天的日期
	 * 
	 * @return
	 */
	public static String getCurrMonthFirstDay() {
		Calendar cal = Calendar.getInstance();
		String s = (getYear(cal)) + "-" + (getMonth(cal)) + "-01";
		return s;
	}

	/***
	 * 获得本月的最后一天的日期
	 * 
	 * @return
	 */
	public static String getCurrMonthLastDay() {
		Calendar cal = Calendar.getInstance();
		String s = (getYear(cal)) + "-" + (getMonth(cal)) + "-" + getDays(cal);
		return s;
	}

	/***
	 * 获得给定日期当月的天数
	 * 
	 * @param cal
	 * @return
	 */
	public static int getDays(Calendar cal) {
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/***
	 * 获得给定日历的年
	 * 
	 * @param cal
	 * @return
	 */
	public static int getYear(Calendar cal) {
		return cal.get(Calendar.YEAR);
	}

	/***
	 * 获得给定日历的月
	 * 
	 * @param cal
	 * @return
	 */
	public static int getMonth(Calendar cal) {
		return (cal.get(Calendar.MONTH) + 1);
	}

	/**
	 * 验证输入的日期是否合法
	 * 
	 * @param expDate
	 * @return
	 */
	public static boolean checkExpDate(String expDate) {
		int year = Integer.parseInt(expDate.substring(0, 4));
		int month = Integer.parseInt(expDate.substring(4, 6));
		int day = Integer.parseInt(expDate.substring(6, 8));
		if (month > 12 || month < 1) {
			return false;
		}

		int[] monthLengths = new int[] { 0, 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (isLeapYear(year)) {
			monthLengths[2] = 29;
		} else {
			monthLengths[2] = 28;
		}

		int monthLength = monthLengths[month];
		if (day < 1 || day > monthLength) {
			return false;
		}
		return true;
	}

	/**
	 * 是否是闰年
	 */
	private static boolean isLeapYear(int year) {
		return ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
	}

	/**
	 * 
	 * 方法用途: 结束时间（end）与start时间进行比较<br>
	 * 实现步骤: 如果相等返回0，如果end大于start返回1，如果end小于start返回-1<br>
	 */
	public static int compareEndAndStart(String start, String end) throws Exception {
		return compareDate(str2Date(start), str2Date(end));
	}

	/**
	 * 
	 * 方法用途: 结束时间（end）与start时间进行比较<br>
	 * 实现步骤: 如果相等返回0，如果end大于start返回1，如果end小于start返回-1<br>
	 */
	public static int compareDate(Date start, Date end) throws Exception {
		long s = start.getTime();
		long e = end.getTime();
		if (e > s) {
			return 1;
		} else if (e < s) {
			return -1;
		}
		return 0;
	}

	/**
	 * 获取当前日期是星期几
	 * 
	 * @return 当前日期是星期几
	 */
	public static String getWeekOfDate() {
		// String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
		String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		Calendar cal = Calendar.getInstance();
		// cal.setTime(dt);
		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}

	/**
	 * 功能：返回上旬/中旬/下旬 1 ：上旬 2： 中旬 3： 下旬
	 */
	public static int getEarlyMidLate(Date date) {
		int day = getDay(date);
		int earlyMidLate = 0;
		if (1 <= day && day <= 10) {
			earlyMidLate = 1;
		}
		if (11 <= day && day <= 20) {
			earlyMidLate = 2;
		}
		if (20 < day) {
			earlyMidLate = 3;
		}
		return earlyMidLate;
	}

	/**
	 * 返回当前月份的第一天
	 * 
	 */
	public static Date currentMonthFirstDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1); // 设置为1号，当前日期既为本月第一天
		return calendar.getTime();
	}

	/**
	 * 返回当前月份的最后一天
	 * 
	 */
	public static Date currentMonthLastDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return calendar.getTime();
	}

	/**
	 * 根据生日获取年龄
	 * 
	 */
	public static int getAge(Date birthDay) {
		Calendar cal = Calendar.getInstance();

		if (cal.before(birthDay)) {
			throw new IllegalArgumentException("unbelievable!! The birthDay is before Now.");
		}
		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH);
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(birthDay);

		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;

		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				age--;
			}
		}
		return age;
	}

	public static void main(String[] args) {
		System.out.println("data="+formatNowDate());
	}

}