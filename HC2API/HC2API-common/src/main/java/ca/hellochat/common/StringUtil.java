package ca.hellochat.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
public class StringUtil {
	// 时间对比
	public static boolean BalanceDate(String day1, String day2) throws Exception {

		boolean flag = true;
		DateFormat df = DateFormat.getDateInstance();
		try {
			flag = df.parse(day1).before(df.parse(day2));
		} catch (ParseException e) {
			throw e;
		}
		return flag;
	}

	/**
	 * 获取字符串的长度
	 * @param str
	 * @return
	 */
	public static int getRealLength(String str) {
		int len = 0;
		String[] arrayVal = str.split("");
		for (int i = 1; i < arrayVal.length; i++) {
			if (arrayVal[i].matches("[^\\x00-\\xff]")){ // 全角
				len += 2;
			}else{
				len += 1;
			}
		}
		return len;
	}

	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * 判断字符串是否为空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (null == str || "".equals(str)) {
			return true;
		}
		if(str.length()<=0){
			return true;
		}
		if ("".equals(str.trim())) {
			return true;
		}
		return false;
	}
	/**
	 * 判断字符串是否为数字
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str) {

		String checkPassWord = "^[0-9]+$";
		if (null == str) {
			return false;
		}
		if (!str.matches(checkPassWord)) {
			return false;
		}

		return true;
	}
    /**
     * 获取指定位数的随机字符串
     * @param n
     * @return
     */
	public static String getRandStr(int n) {
		Random random = new Random();
		String sRand = "";
		for (int i = 0; i < n; i++) {
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
		}
		return sRand;
	}
	
	/**
	 * 产生一个长度为n的随机字符串（有数字和字母(大小写)组成）
	 * 
	 * @param n 字符串长度
	 * @param state 0大小写混合  1表示只显示大写  2表示只显示小写
	 * @param flg 0字母 1数字  2混合
	 * @return
	 */
	public static String getRandomNum(int n,int flg) {
		Random random = new Random();
		String sRand = "";
		for (int i = 0; i < n; i++) {
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
		}
		return sRand;
	}
	/**
	 * 产生一个长度为n的随机字符串（有数字和字母(大小写)组成）
	 * 
	 * @param n 字符串长度
	 * @param flg 0大写，1小写
	 */
	public static String getRandomStr(int n,int flg) {
		Random r = new Random();
		String str = "";
		try {
			String chars[] = { 
					"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
					"m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x",
					"y", "z"};
			
			List<String> list = new ArrayList<String>();
			list.addAll(Arrays.asList(chars));
			Collections.shuffle(list);
			for (int i = 0; i < n; i++) {
				str += list.get(r.nextInt(list.size()));
			}
			if(flg==0){
				str = str.toUpperCase();
			}else{
				str = str.toLowerCase();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;

	}
	/***
	 * map转换成String
	 * @param params
	 * @return
	 */
	public static String mapToString(Map<String,String> params) {
		if(params==null || params.isEmpty()){
			return "";
		}
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String,String> entry : params.entrySet()) {
			sb.append(","+entry.getKey()+"="+String.valueOf(entry.getValue()));
		}
		if(sb.length()>0){
			return sb.substring(1);
		}

		return sb.toString();  
	}
	/***
	 * object的Map转换成String
	 * @param params
	 * @return
	 */
	public static String mapObjToString(Map<String,Object> params) {
		if(params==null || params.isEmpty()){
			return "";
		}
		StringBuffer sb = new StringBuffer();
		try {
			for (Map.Entry<String,Object> entry : params.entrySet()) {
				sb.append(","+entry.getKey()+"="+entry.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(sb.length()>0){
			return sb.substring(1);
		}
		return sb.toString();  
	}
	
	/***
	 * object的Map转换成String
	 * @param params
	 * @return
	 */
	public static String ListObjToString(List olist) {
		if(olist==null || olist.isEmpty()){
			return "";
		}
		StringBuffer sb = new StringBuffer("[");
		try {
			for (Object obj : olist) {
				sb.append("{"+obj.toString()+"}");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sb.append("]");
		return sb.toString();  
	}
	/**
	 * 判断是否是中文
	 * @param c
	 * @return
	 */
    private static boolean isChinese(char c) {  
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);  
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS  // 判断中文的。号  
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS// 
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A  
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION  // GENERAL_PUNCTUATION 判断中文的“号 
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION  
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) //HALFWIDTH_AND_FULLWIDTH_FORMS 判断中文的，号 
        {   
            return true;  
        }  
        return false;  
    }  
    /*
     * 判断字符串是否是中文
     */
    public static boolean isChinese(String strName) {  
        char[] ch = strName.toCharArray();  
        for (int i = 0; i < ch.length; i++) {  
            char c = ch[i];  
            if (isChinese(c)) {  
                return true;  
            }  
        }  
        return false;  
    }  
    
    public static String getUUID(){
    	return UUID.randomUUID().toString().replaceAll("-","");
    }

	public static void main(String[] args) {
		getUUID();
	}

}
