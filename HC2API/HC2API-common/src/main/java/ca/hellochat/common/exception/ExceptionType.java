package ca.hellochat.common.exception;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: zp
 * @Date: 2019-10-10 14:47
 * @Description:
 */
@Getter
@AllArgsConstructor
public enum  ExceptionType {
    /**
     * 错误类型
     */
	REPEAT_REQUEST("300","重复操作"),
	INVALID_TOKEN("402","token失效"),
    INVALID_PARAMS("403","参数无效或缺失"),
    INVALID_AUTHORITY("404","无效的访问权限"),
    FAILED_AUTHORITY("405","无效的数据"),
    SERVER_EXCEPTION("500","服务器异常"),
    FAILED_PARAMS("501","参数定义错误"),
    UNMATCHED_RESPONSE("502","服务器未找到匹配数据"),
    FAILED_RESPONSE("504","不正确的响应"),
    //用户相关的错误
    USER_AUTH_ERROR("1000","用户登录参数错误"),
    USER_AUTH_ERRORMOBILE("1003","根据手机号无法找到用户"),
    USER_AUTH_PWDERROR("1001","用户名或者密码错误"),
    USER_AUTH_NOTFOUND("1002","未找到用户"),
    USER_AUTH_ERROROLDPWD("1004","旧密码错误"),
//    FRIEND_NOTFOUND("1005","未查询到好友"),
    //业务异常
    ROOM_ACTIVATE("1100","用户没有从直播间退出，无法重建"),
    
    //IM相关的错误 2XXX
    IM_FRIEND_NOTFOUD("2001","未查询到好友"),
    IM_FRIEND_EXIST("2002","已经是好友"),
    IM_FRIEND_ADDMYSELF("2003","不能添加自己为好友")
   //钱包相关的错误 3XXX
    
    ;

    /**
     * 错误码
     */
    private String code;

    /**
     * 提示信息
     */
    private String etype;
}
