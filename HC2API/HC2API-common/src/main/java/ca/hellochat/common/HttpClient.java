package ca.hellochat.common;

import java.util.Map;

import org.apache.commons.codec.Charsets;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class HttpClient {
	
	public static String post(String url, Map<String, Object> map, Header... headers) throws Exception {
		org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		post.setConfig(config());
		post.setHeader("Content-Type", "application/json");
		if(headers != null) {
			for(int i = 0, len = headers.length; i < len; i++) {
				post.addHeader(headers[i]);
			}
		}
		if(map != null && map.size() > 0) {
			String json = JSONObject.toJSONString(map);
			post.setEntity(new StringEntity(json, Charsets.UTF_8));
		}
		HttpResponse response = client.execute(post);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return EntityUtils.toString(response.getEntity(),Charsets.UTF_8);
		}
		return null;
	}
	
	public static String post(String url, String mesg, boolean isJson, Header... headers) throws Exception {
		org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		post.setConfig(config());
		if(isJson) {
			post.setHeader("Content-Type", "application/json");
		}
		if(headers != null) {
			for(int i = 0, len = headers.length; i < len; i++) {
				post.addHeader(headers[i]);
			}
		}
		if(mesg != null && !"".equals(mesg)) {
			post.setEntity(new StringEntity(mesg, Charsets.UTF_8));
		}
		HttpResponse response = client.execute(post);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return EntityUtils.toString(response.getEntity(),Charsets.UTF_8);
		}
		return null;
	}
	
	public static String get(String url, Header... headers) throws Exception {
		org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);
		get.setConfig(config());
		if(headers != null) {
			for(int i = 0, len = headers.length; i < len; i++) {
				get.addHeader(headers[i]);
			}
		}
		HttpResponse response = client.execute(get);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return EntityUtils.toString(response.getEntity(),Charsets.UTF_8);
		}
		return null;
	}
	
	public static String put(String url, Map<String, Object> map, Header... headers) throws Exception {
		org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
		HttpPut put = new HttpPut(url);
		put.setConfig(config());
		put.setHeader("Content-Type", "application/json");
		if(headers != null) {
			for(int i = 0, len = headers.length; i < len; i++) {
				put.addHeader(headers[i]);
			}
		}
		if(map != null && map.size() > 0) {
			String json = JSONObject.toJSONString(map);
			put.setEntity(new StringEntity(json, Charsets.UTF_8));
		}
		HttpResponse response = client.execute(put);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return EntityUtils.toString(response.getEntity(),Charsets.UTF_8);
		}
		return null;
	}
	
	public static String delete(String url, Header... headers) throws Exception {
		org.apache.http.client.HttpClient client = HttpClientBuilder.create().build();
		HttpDelete delete = new HttpDelete(url);
		delete.setConfig(config());
		if(headers != null) {
			for(int i = 0, len = headers.length; i < len; i++) {
				delete.addHeader(headers[i]);
			}
		}
		HttpResponse response = client.execute(delete);
		if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			return EntityUtils.toString(response.getEntity(),Charsets.UTF_8);
		}
		return null;
	}
	
	private static RequestConfig config() {
		return RequestConfig.custom().setConnectTimeout(2000).setConnectionRequestTimeout(2000).setSocketTimeout(2000).build();
	}

}
