package ca.hellochat.common.generate;

public class Constant {
	public static String startStr_service = "I";
	public static String endStr_service= "Service";
	public static String startStr_service_impl = "";
	public static String endStr_service_impl= "Service";
	public static String startStr_business = "I";
	public static String endStr_business= "Business";
	public static String startStr_business_impl = "";
	public static String endStr_business_impl= "Business";
	public static String startStr_dao = "I";
	public static String endStr_dao= "Dao";
	public static String startStr_dao_xml = "I";
	public static String endStr_dao_xml= "Dao";
	
	public static String startStr_controller = "";
	public static String endStr_controller= "Controller";
	
	public static String getFirstUpper(String str){
		if(str==null || str.length()<=0){
			return null;
		}
		return str.substring(0,1).toUpperCase()+str.substring(1);
	}
	public static String getFirstLower(String str){
		if(str==null || str.length()<=0){
			return null;
		}
		return str.substring(0,1).toLowerCase()+str.substring(1);
	}
	public static String getLostFirstAndLower(String str){
		if(str==null || str.length()<=0){
			return null;
		}
		return str.substring(1,2).toLowerCase()+str.substring(2);
	}
}
