package ca.hellochat.common.exception;

import lombok.Data;

/**
 * 请求参数验证错误
 * @author yangl
 *
 */
@Data
public class IChatException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 926599750988367509L;
	private String code;
	private String msg;

    /**
     * 使用已有的错误类型
     * @param type 枚举类中的错误类型
     */
    public IChatException(ExceptionType exceptionType,String msg){
        super(exceptionType.getEtype());
        this.code = exceptionType.getCode();
        this.msg = msg;
    }
}
