package ca.hellochat.common.generate.entity;

public class Column {
	private String columnName;// 数据库字段名
	private String filedName;// 实体类字段名
	private String ctype;// 数据库字段类型
	private String ftype;// 实体类字段类型
	private String comment;//注释
	private String columnFlag;//字段标识 0标识普通字段 1标识特殊字段
	private String ckey;//主键？
	private String cextra;//扩展
	private String allFtype;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getFiledName() {
		return processField(columnName);
	}

	public void setFiledName(String filedName) {
		this.filedName = filedName;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getFtype() {
//		System.out.println("ctype"+ctype);
		return processType(ctype);
	}

	public void setFtype(String ftype) {
		this.ftype = ftype;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getColumnFlag() {
		return columnFlag;
	}

	public void setColumnFlag(String columnFlag) {
		this.columnFlag = columnFlag;
	}

	public String getCkey() {
		return ckey;
	}

	public void setCkey(String ckey) {
		this.ckey = ckey;
	}

	public String getCextra() {
		return cextra;
	}

	public void setCextra(String cextra) {
		this.cextra = cextra;
	}
	
	public String getGetterStr(){
		StringBuffer sb = new StringBuffer();
		if(getFiledName()==null || getFiledName().length()<=0){
			return null;
		}
		sb.append("\n");
		if("Date".equalsIgnoreCase(getFtype())){
			sb.append("\t@JSONField(format = \"yyyy-MM-dd HH:mm:ss\")");
			sb.append("\n");
        }
		sb.append("\tpublic "+this.getFtype()+" get"+getFiledName().substring(0, 1).toUpperCase() + getFiledName().substring(1)+"(){");
		sb.append("\n");
		sb.append("\t\treturn this."+getFiledName()+";");
		sb.append("\n");
		sb.append("\t}");
        sb.append("\n");
//        System.out.println("getterStr = " + sb.toString());
        return sb.toString();
	}
	
	public String getSetterStr(){
		StringBuffer sb = new StringBuffer();
		if(getFiledName()==null || getFiledName().length()<=0){
			return null;
		}
		sb.append("\n");
		if("Date".equalsIgnoreCase(getFtype())){
			sb.append("\t@DateTimeFormat(pattern=\"yyyy-MM-dd HH:mm:ss\")");
			sb.append("\n");
        }
		sb.append("\tpublic void set"+getFiledName().substring(0, 1).toUpperCase() + getFiledName().substring(1)+"("+this.getFtype()+" "+this.getFiledName()+"){");
		sb.append("\n");
		sb.append("\t\tthis." + getFiledName() + " = " + getFiledName() + ";");
		sb.append("\n");
		sb.append("\t}");
        sb.append("\n");
//        System.out.println("setterStr = " + sb.toString());
        return sb.toString();
	}
	
	@Override
	public String toString() {
		return "Column [columnName=" + columnName + ", filedName=" + getFiledName() + ", ctype=" + ctype + ", ftype=" + getFtype()
				+ ", comment=" + comment + ", columnFlag=" + columnFlag + ", ckey=" + ckey + ", cextra=" + cextra + "]";
	}




	/**
	  ***********************************************************
	  *
	  * 针对不同环境进行不同配置开始
	  * 
	  * 
	  ***********************************************************
	  */
   public static final String type_char = "char";

   public static final String type_date = "date";

   public static final String type_timestamp = "timestamp";
   
   public static final String type_time = "time";

   public static final String type_int = "int";

   public static final String type_bigint = "bigint";

   public static final String type_text = "text";

   public static final String type_bit = "bit";

   public static final String type_decimal = "decimal";

   public static final String type_blob = "blob";	//二进制大文件，如一张图或声音文件
   
   public static final String type_double = "double";	

	    private String processType(String type) {
	        if( type.indexOf(type_char) > -1 ) {
	            return "String";
	        } else if ( type.indexOf(type_bigint) > -1 ) {
	            return "Integer";
	        } else if ( type.indexOf(type_int) > -1 ) {
	            return "Integer";
	        } else if ( type.indexOf(type_date) > -1 || type.indexOf(type_timestamp) > -1) {
	        		return "Date";
	            
	        } else if ( type.indexOf(type_text) > -1 ) {
	            return "String";
	        } else if ( type.indexOf(type_bit) > -1 ) {
	            return "Boolean";
	        } else if ( type.indexOf(type_decimal) > -1 ) {
	        	return "BigDecimal";
	        } else if ( type.indexOf(type_blob) > -1 ) {
	            return "byte[]";
	        } else if ( type.indexOf(type_double) > -1 ) {
	        	return "Double";
	        }else{
	        	return "String";
	        }
	    }
	    
	    
	    private String processField( String field ) {
//	        StringBuffer sb = new StringBuffer();
//	        if(field.indexOf("_")>=0){
//	        	String[] fields = field.split("_");
//	        	String temp = null;
//	        	String first = fields[0].toLowerCase();
//	        	if("0123456789".contains(first.substring(0, 1))){
//	        		first = "x" +first.substring(1);
//	        	}
//		        sb.append(first);
//		        for ( int i = 1 ; i < fields.length ; i++ ) {
//		            temp = fields[i].trim();
//		            sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1).toLowerCase());
//		        }
//	        }else{
//	        	sb.append(field.toLowerCase());
//	        }
//	        
//	        return sb.toString();
	    	
	    	return field;
	    }

		public String getAllFtype() {
		   String type = this.getCtype();
			if( type.indexOf(type_char) > -1 ) {
	            return "java.lang.String";
	        } else if ( type.indexOf(type_bigint) > -1 ) {
	            return "java.lang.Integer";
	        } else if ( type.indexOf(type_int) > -1 ) {
	            return "java.lang.Integer";
	        } else if ( type.indexOf(type_date) > -1 || type.indexOf(type_timestamp) > -1) {
	        		return "java.util.Date";
	            
	        } else if ( type.indexOf(type_text) > -1 ) {
	            return "java.lang.String";
	        } else if ( type.indexOf(type_bit) > -1 ) {
	            return "java.lang.Boolean";
	        } else if ( type.indexOf(type_decimal) > -1 ) {
	        	return "java.lang.BigDecimal";
	        } else if ( type.indexOf(type_blob) > -1 ) {
	            return "java.lang.byte[]";
	        } else if ( type.indexOf(type_double) > -1 ) {
	        	return "java.lang.Double";
	        }else{
	        	return "java.lang.String";
	        }
		}

		public void setAllFtype(String allFtype) {
			this.allFtype = allFtype;
		}
	    
	    
}
