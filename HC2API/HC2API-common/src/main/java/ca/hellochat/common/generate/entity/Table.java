package ca.hellochat.common.generate.entity;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import ca.hellochat.common.generate.Constant;
import ca.hellochat.common.generate.EntityUtilNew;

public class Table {
	private String tableName;// 表名
	private String entityName;// 实体名
	private String comment;// 注释
	private List<Column> columnList;
	//变量名
	private String varName;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEntityName() {
		StringBuffer sb = new StringBuffer();
		  String tableName_temp = tableName.toLowerCase();
		 if(tableName_temp.indexOf("_")>=0){
	        	String[] tableNames = tableName_temp.split("_");
	            String temp = null;
	            for( int i = 1 ; i < tableNames.length;i++){
	                temp = tableNames[i].trim();
	                sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1));
	            }
	      }else{
	        	sb.append(tableName_temp.substring(0, 1).toUpperCase()).append(tableName.substring(1));
	      }
		 entityName = sb.toString();
		 if(EntityUtilNew.map.containsKey(entityName)){
			 entityName = EntityUtilNew.map.get(entityName);
		 }
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

	public List<Column> getColumnList() {
		return columnList;
	}

	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}
	public String getVarName() {
		String eName = getEntityName();
		varName = eName.substring(0,1).toLowerCase()+eName.substring(1);
		
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}
	
	public Column getPK(){
		Column column = null;
		List<Column> clist = getColumnList();
		if(clist==null || clist.size()<=0){
			return null;
		}
		for (Column c : clist) {
			String key = c.getCkey();
			if(key==null || "".equals(key)){
				continue;
			}
			if("PRI".equalsIgnoreCase(key)){
				column = c;
				break;
			}
		}
		return column;
		
	}
	
	public String getServiceImplName(){
		return Constant.startStr_service_impl+this.getEntityName()+Constant.endStr_service_impl;
	}
	public String getServiceName(){
		return Constant.startStr_service+this.getEntityName()+Constant.endStr_service;
	}
	
	public String getBusinessImplName(){
		return Constant.startStr_business_impl+this.getEntityName()+Constant.endStr_business_impl;
	}
	public String getBusinessName(){
		return Constant.startStr_business+this.getEntityName()+Constant.endStr_business;
	}
	public String getDaoXmlName(){
		return Constant.startStr_dao_xml+this.getEntityName()+Constant.endStr_dao_xml;
	}
	public String getDaoName(){
		return Constant.startStr_dao+this.getEntityName()+Constant.endStr_dao;
	}
	
	public String getControllerName(){
		return Constant.startStr_controller+this.getEntityName()+Constant.endStr_controller;
	}

	@Override
	public String toString() {
		return "Table [tableName=" + tableName + ", entityName=" + getEntityName() + ", comment=" + comment + ", columnList="
				+ JSONArray.toJSONString(columnList) + ", varName=" + getVarName() + "]";
	}

	
	
	

	

}
