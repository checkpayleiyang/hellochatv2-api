package ca.hellochat.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileZip {
	
	public static String fileToZip(String sourceFilePath,String zipFilePath,String fileName){
		
		File sourceFile = new File(sourceFilePath + "/" + fileName);  
		FileInputStream fis = null;  
		BufferedInputStream bis = null;  
		FileOutputStream fos = null;  
		ZipOutputStream zos = null;  
		String name = getRandomString(19);
        if(sourceFile.exists() == false){  
            System.out.println("待压缩的文件：" + sourceFilePath + "不存在.");  
        }else{  
            try {  
                File zipFile = new File(zipFilePath + "/" + name +".zip");  
				fos = new FileOutputStream(zipFile);  
				zos = new ZipOutputStream(new BufferedOutputStream(fos));  
				//创建ZIP实体，并添加进压缩包  
				ZipEntry zipEntry = new ZipEntry(sourceFile.getName());  
				zos.putNextEntry(zipEntry);  
				//读取待压缩的文件并写进压缩包里  
				fis = new FileInputStream(sourceFilePath + "/" + fileName);  
				bis = new BufferedInputStream(fis, 1024*10);  
				int read = 0;  
				byte[] bufs = new byte[1024*10];  
				while((read=bis.read(bufs, 0, 1024*10)) != -1){  
					zos.write(bufs,0,read);  
				}  
            } catch (Exception e) {  
                e.printStackTrace();  
                throw new RuntimeException(e);  
            } finally{  
                try {  
                    if(null != bis) bis.close();  
                    if(null != zos) zos.close();  
                } catch (Exception e) {  
                    e.printStackTrace();  
                    throw new RuntimeException(e);  
                }  
            }  
        }  
        return name;  
    }   
	
	private static String getRandomString(int length){
		String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random=new Random();
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<length;i++){
			int number=random.nextInt(62);
			sb.append(str.charAt(number));
		}
		return sb.toString();
	}
	
//    public static void main(String[] args){  
//        String sourceFilePath = "D:\\test111";  
//        String zipFilePath = "D:\\test111";  
//        String fileName = "test.txt"; 
//        boolean flag = fileToZip(sourceFilePath, zipFilePath, fileName);  
//        if(flag){  
//            System.out.println("文件打包成功!");  
//        }else{  
//            System.out.println("文件打包失败!");  
//        }  
//    }  
}
