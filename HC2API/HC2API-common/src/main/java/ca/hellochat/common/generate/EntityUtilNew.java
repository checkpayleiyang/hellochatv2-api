package ca.hellochat.common.generate;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import ca.hellochat.common.generate.entity.Column;
import ca.hellochat.common.generate.entity.Table;

/**
 * 自动生成MyBatis的实体类、实体映射XML文件、Mapper
 * @author WYS
 * @date 2014-11-8
 * @version v1.0
 */
public class EntityUtilNew {
 
    /**
     **********************************使用前必读*******************
     **
     ** 使用前请将moduleName更改为自己模块的名称即可（一般情况下与数据库名一致），其他无须改动。
     **
     ***********************************************************
     */
	public static Map<String,String> map = new HashMap<String,String>();
	static {
		 map.put("Usermsg", "UserMsg");
	 }
 
	 /**
	  ***********************************************************
	  *
	  * 针对不同环境进行不同配置开始
	  * 
	  * 
	  ***********************************************************
	  */
	private static String user = "root";
	   
	private static String password = "mysql";
	
	private static String moduleName = "hellochat_v2"; // 对应模块名称（根据自己模块做相应调整!!!务必修改^_^）
	 
	private static String url = "jdbc:mysql://localhost:3306/" + moduleName + "?serverTimezone=UTC&characterEncoding=utf8";
	
	private static String base_path = "D:/hellochat_v2";
	
	private static String base_package = "ca.hellochat";
		 
	/**
	  ***********************************************************
	  *
	  * 针对不同环境进行不同配置开始
	  * 
	  * 
	  ***********************************************************
	  */
    private final String bean_path = base_path+"/entity_bean";
 
    private final String mapper_path = base_path+"/entity_dao";
    
    private final String service_path = base_path+"/entity_service";
    
    private final String controller_path = base_path+"/entity_controller";
    
    private final String service_impl_path = base_path+"/entity_service/impl";
    
    private final String business_path = base_path+"/entity_business";
    
    private final String business_impl_path = base_path+"/entity_business/impl";
 
    private final String xml_path = base_path+"/entity_dao/mapper";
    
    private static String bean_package = base_package+".entity";
    private static String mapper_package = base_package+".dao";
    private static String service_package = base_package+".service";
    private static String controller_package = base_package+".controller";
    private static String service_impl_package = base_package+".service.impl";
    private static String business_package =base_package+".business";
    private static String business_impl_package = base_package+".business.impl";
    private static String driverName = "com.mysql.cj.jdbc.Driver";
    /**
     * 加载数据库驱动类，获得数据库连接
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static Connection getConnection(){
        Connection conn = null;
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
        return conn;
    }
    
    /**
     * 加载数据库驱动类，获得数据库连接
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static void closeCon(Connection con,PreparedStatement pstate,ResultSet results) {
    	try {
			if(results!=null && !results.isClosed()){
				results.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	try {
			if(pstate!=null && !pstate.isClosed()){
				pstate.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	try {
			if(con!=null && !con.isClosed()){
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
 
 
    /**
     *  获取所有的表（表名）
     *
     * @return
     * @throws SQLException 
     */
    private List<Table> getTables(){
        Connection conn = getConnection();
        PreparedStatement pstate = null;
        ResultSet results = null;
        List<Table> tableList = null;
		try {
			pstate = conn.prepareStatement("show tables");
			results = pstate.executeQuery();
			tableList = new ArrayList<Table>();
			while(results.next()) {
				Table table = new Table();
			    String tableName = results.getString(1);//获取表名（第一列的值）
			    table.setTableName(tableName);
			    String comment = getTableComment(tableName);
			    table.setComment(comment);
			    table.setColumnList(getColumns(tableName));
			    tableList.add(table);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			EntityUtilNew.closeCon(conn, pstate, results);
		}
        return tableList;
    }
    
    /**
     *  获取所有的数据库表注释
     *
     * @return
     * @throws SQLException 
     */
    private String getTableComment(String tableName) throws SQLException {
    	Connection conn = getConnection();
        PreparedStatement pstate = conn.prepareStatement("show table status WHERE NAME = ?");
        pstate.setString(1, tableName);
        ResultSet results = pstate.executeQuery();
        String comment = "";
        if(results.next()) {
           comment = results.getString("COMMENT");
        }
        EntityUtilNew.closeCon(conn, pstate, results);
        return comment;
        
    }
    
    
    
    private List<Column> getColumns(String tableName){
    	Connection conn = getConnection();
        PreparedStatement pstate = null;
        ResultSet results = null;
        List<Column> columnList = null;
		try {
			pstate = conn.prepareStatement("show full fields from "+tableName);
			results = pstate.executeQuery();
			columnList = new ArrayList<Column>();
			while(results.next()){
				Column column = new Column();
				String filedName = results.getString("Field");
				String comment = results.getString("Comment");
				String type = results.getString("Type");
				String key = results.getString("Key");
				String extra = results.getString("Extra");
				column.setColumnName(filedName);
				column.setComment(comment);
				column.setCtype(type);
				column.setCkey(key);
				column.setCextra(extra);
				columnList.add(column);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			EntityUtilNew.closeCon(conn, pstate, results);
		}
        return columnList;
    }
 
 
//    /**
//     * 根据表名生成对应的类或接口的名字
//     * @param table
//     */
//    private void processTable( String table ) {
//        StringBuffer sb = new StringBuffer(table.length());
//        String tableNew = table.toLowerCase();
//        
//        if(tableNew.indexOf("_")>=0){
//        	String[] tables = tableNew.split("_");
//            String temp = null;
//            for( int i = 1 ; i < tables.length ; i++ ) {
//                temp = tables[i].trim();
//                sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1));
////                sb.append(temp.substring(1));
//            }
//        }else{
//        	sb.append(tableNew.substring(0, 1).toUpperCase()).append(tableNew.substring(1));
////        	 sb.append(tableNew.substring(1));
//        }
//        
//        beanName = sb.toString();
//        String newStr = beanName;
//        if(map.containsKey(beanName)){
//        	beanName = map.get(beanName);
//        	
//        }
////        System.out.println("newStr=="+newStr+",oldStr="+beanName);
//        try {
//			Thread.sleep(200);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        mapperName = "I"+beanName + "Mapper";
//        serviceImplName = beanName + "Service";
//        serviceName = "I"+beanName + "Service";
//        businessImplName = beanName + "Business";
//        businessName = "I"+beanName + "Business";
//    }
 
 
//    private String processType(String type) {
//    	return processType(type,false);
//    }
    
//    private String processType( String type,boolean isSimple) {
//        if( type.indexOf(type_char) > -1 ) {
//            return "String";
//        } else if ( type.indexOf(type_bigint) > -1 ) {
//            return "Integer";
//        } else if ( type.indexOf(type_int) > -1 ) {
//            return "Integer";
//        } else if ( type.indexOf(type_date) > -1 || type.indexOf(type_timestamp) > -1) {
//        	if(isSimple){
//        		return "Date";
//        	}else{
//        		return "Date";
//        	}
//            
//        } else if ( type.indexOf(type_text) > -1 ) {
//            return "String";
//        } else if ( type.indexOf(type_bit) > -1 ) {
//            return "Boolean";
//        } else if ( type.indexOf(type_decimal) > -1 ) {
//        	if(isSimple){
//        		return "BigDecimal";
//        	}else{
//        		return "java.math.BigDecimal";
//        	}
//        } else if ( type.indexOf(type_blob) > -1 ) {
//            return "byte[]";
//        } else if ( type.indexOf(type_double) > -1 ) {
//        	return "Double";
//        }else{
//        	return "String";
//        }
//    }
 
 
//    private String processField( String field ) {
//        StringBuffer sb = new StringBuffer(field.length());
//        //field = field.toLowerCase();
//        String[] fields = field.split("_");
//        String temp = null;
//        sb.append(fields[0].toLowerCase());
//        for ( int i = 1 ; i < fields.length ; i++ ) {
//            temp = fields[i].trim();
//            sb.append(temp.substring(0, 1).toUpperCase()).append(temp.substring(1).toLowerCase());
//        }
//        return sb.toString();
//    }
// 
 
    /**
     *  将实体类名首字母改为小写
     *
     * @param beanName
     * @return 
     */
    private String processResultMapId( String beanName ) {
        return beanName.substring(0, 1).toLowerCase() + beanName.substring(1);
    }
 
 
    /**
     *  构建类上面的注释
     *
     * @param bw
     * @param text
     * @return
     * @throws IOException 
     */
    private BufferedWriter buildClassComment( BufferedWriter bw, String text ) throws IOException {
        bw.newLine();
        bw.write("/**");
        bw.newLine();
        bw.write(" * ");
        bw.newLine();
        bw.write(" * " + text);
        bw.newLine();
        bw.write(" * ");
        bw.newLine();
        bw.write(" **/");
        return bw;
    }
 
 
    /**
     *  构建方法上面的注释
     *
     * @param bw
     * @param text
     * @return
     * @throws IOException 
     */
    private BufferedWriter buildMethodComment( BufferedWriter bw, String text ) throws IOException {
        bw.newLine();
        bw.write("\t/**");
        bw.write(" " + text);
        bw.write("**/");
        return bw;
    }
    
    /**
     *  构建方法上面的注释
     *
     * @param bw
     * @param text
     * @return
     * @throws IOException 
     */
    private BufferedWriter buildMethodComments( BufferedWriter bw, String com, String par, String ret ) throws IOException {
        bw.newLine();
        bw.write("\t/**");
        bw.newLine();
        bw.write(" \t* ");
        bw.newLine();
        bw.write(" \t* @param " + par);
        bw.newLine();
        bw.write(" \t* @return " + ret);
        bw.newLine();
        bw.write(" \t**/");
        return bw;
    }
 
	private void processImport(Table table, BufferedWriter bw) throws Exception {
		List<Column> cList = table.getColumnList();
		boolean dateFlag = false;
		boolean decimalFlag = false;
		
		for (int i = 0; i < cList.size(); i++) {
			Column column = cList.get(i);
//			if(table.getEntityName().contains("OclInfo")){
//				System.out.println("column.getCtype()="+column.getCtype());
//			}
			if (column.getCtype().contains(Column.type_date)|| column.getCtype().contains("time")) {
				dateFlag = true;
			} 
			if (column.getCtype().equalsIgnoreCase(Column.type_decimal)||column.getCtype().contains(Column.type_decimal)) {
				decimalFlag = true;
			}
		}
//		System.out.println("dateFlag="+dateFlag+";decimalFlag="+decimalFlag);
		if(dateFlag){
			bw.write("import java.util.Date;");
			bw.newLine();
			bw.write("import org.springframework.format.annotation.DateTimeFormat;");
			bw.newLine();
			bw.write("import com.alibaba.fastjson.annotation.JSONField;");
			bw.newLine();
		}
		if(decimalFlag){
			bw.write("import java.math.BigDecimal;");
			bw.newLine();
		}
		
	}
	
	
	 /**
     *  生成实体类
     *
     * @param columns
     * @param types
     * @param comments
     * @throws IOException 
     */
    private void buildEntityBean(Table table) throws Exception {
        File folder = new File(bean_path);
        if ( !folder.exists() ) {
            folder.mkdir();
        }
        File beanFile = new File(bean_path, table.getEntityName() + ".java");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(beanFile)));
        bw.write("package " + bean_package + ";");
        bw.newLine();
        bw.write("import java.io.Serializable;");
        bw.newLine();
        bw.write("import lombok.Data;");
        bw.newLine();
        processImport(table,bw);
        bw = buildClassComment(bw, table.getComment());
        bw.newLine();
        bw.write("@Data");
        bw.newLine();
        bw.write("@SuppressWarnings(\"serial\")");
        bw.newLine();
        bw.write("public class " + table.getEntityName() + " implements Serializable {");
        bw.newLine();
        List<Column> cList = table.getColumnList();
        for ( int i = 0 ; i < cList.size() ; i++ ) {
        	Column column = cList.get(i);
        	if("Date".equalsIgnoreCase(column.getFtype())){
        		bw.write("\t@DateTimeFormat(pattern = \"yyyy-MM-dd HH:mm:ss\")");
        		bw.write("\n");
        		bw.write("\t@JSONField(format = \"yyyy-MM-dd HH:mm:ss\")");
        		bw.write("\n");
            }
            bw.write("\tprivate " + column.getFtype() + " " + column.getFiledName() + ";");
            bw.write("//" + column.getComment());
            bw.newLine();
        }
        // 生成get 和 set方法
        for ( int i = 0 ; i < cList.size() ; i++ ) {
           Column column = cList.get(i);
           bw.write(column.getGetterStr());
           bw.write(column.getSetterStr());
        }
        bw.newLine();
        bw.write("}");
        bw.newLine();
        bw.flush();
        bw.close();
    }
	
 
 
    /**
     *  构建Mapper文件
     *
     * @throws IOException 
     */
    private void buildMapper(Table table) throws IOException {
        File folder = new File(mapper_path);
        if ( !folder.exists() ) {
            folder.mkdirs();
        }
        String beanName = table.getEntityName();
        String mapperFileName = Constant.startStr_dao+beanName+Constant.endStr_dao;
        File mapperFile = new File(mapper_path, mapperFileName + ".java");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperFile), "utf-8"));
        bw.write("package " + mapper_package + ";");
        bw.newLine();
        bw.write("import " + bean_package + "." + table.getEntityName() + ";");
        bw.newLine();
    	bw.write("import java.util.Map;");
    	bw.newLine();
    	bw.write("import java.util.List;");
        bw.newLine();
        bw.write("import org.apache.ibatis.annotations.Mapper;");
        bw = buildClassComment(bw, table.getComment() + "数据库操作接口类");
        bw.newLine();
        bw.write("@Mapper");
        bw.newLine();
        bw.write("public interface " + mapperFileName + "{");
     // ----------定义Mapper中的方法Begin----------
        bw = buildMethodComment(bw,"查询（根据主键查询对象）");
        bw.newLine();
        Column column = table.getPK();
        if(column!=null){
        	bw.write("\tpublic " + beanName + " selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
            bw.newLine();
        }else{
        	 bw.write("\tpublic " + beanName + " selectById(Integer id);");
             bw.newLine();
        }
       
        
        bw = buildMethodComment(bw,"查询（根据条件查询对象）");
        bw.newLine();
        bw.write("\tpublic " + beanName + " selectByParam(Map<String,String> param);");
        bw.newLine();
        //根据条件查询
        bw = buildMethodComment(bw, "查询（根据条件查询）");
        bw.newLine();
        bw.write("\tpublic List<" + beanName + ">  selectListByParam(Map<String,String> param);");
        bw.newLine();
      //根据条件查询数量
        bw = buildMethodComment(bw, "查询（根据条件查询数量）");
        bw.newLine();
        bw.write("\tpublic int selectCountByParam(Map<String,String> param);");
        bw.newLine();
        
        bw = buildMethodComment(bw, "删除（根据主键删除）");
        
        if(column!=null){
        	bw.newLine();
        	bw.write("\tpublic int deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
        }else{
        	 bw.newLine();
             bw.write("\t" + "public int deleteById(Integer id);");
        }
        
        bw.newLine();
        bw = buildMethodComment(bw, "添加（匹配有值的字段）");
        bw.newLine();
        bw.write("\t" + "public int insertSelective(" + beanName +" "+  this.processResultMapId(beanName)+");");
        bw.newLine();
        bw = buildMethodComment(bw, "修改（根据主键ID修改）");
        if(column!=null){
        	bw.newLine();
        	bw.write("\tpublic int updateBy"+Constant.getFirstUpper(column.getFiledName())+"(" + beanName + " "+Constant.getFirstLower(beanName)+");");
        }else{
        	 bw.newLine();
             bw.write("\tpublic int updateById(" + beanName + " "+Constant.getFirstLower(beanName)+");");
        }
        // ----------定义Mapper中的方法End----------
        bw.newLine();
        bw.write("}");
        bw.flush();
        bw.close();
    }
 
    /**
     *  构建Service文件
     *
     * @throws IOException 
     */
    private void buildService(Table table) throws IOException {
        File folder = new File(service_path);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        
        String beanName = table.getEntityName();
        String serviceName = Constant.startStr_service+beanName+Constant.endStr_service;
        
        File serviceFile = new File(service_path, serviceName + ".java");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(serviceFile), "utf-8"));
        bw.write("package " + service_package + ";");
        bw.newLine();
        bw.write("import " + bean_package + "." + beanName + ";");
        bw.newLine();
        bw.write("import java.util.Map;");
        bw.newLine();
        bw.write("import java.util.List;");
        bw.newLine();
        bw.write("import "+base_package+".entity.domain.Page;");
        bw = buildClassComment(bw, serviceName + "服务接口");
        bw.newLine();
        //      bw.write("public interface " + mapperName + " extends " + mapper_extends + "<" + beanName + "> {");
        bw.write("public interface " + serviceName + "{");
        bw.newLine();
        // ----------定义Service中的方法Begin----------
        Column column = table.getPK();
        if(column!=null){
        	bw.write("\tpublic " + beanName + " selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
            bw.newLine();
        }
      //根据条件查询对象
        bw = buildMethodComment(bw, "对象查询（根据条件查询）");
        bw.newLine();
        bw.write("\tpublic " + beanName + " selectByParam(Map<String,String> param);");
        bw.newLine();
        //根据条件查询
        bw = buildMethodComment(bw, "查询（根据条件查询）");
        bw.newLine();
        bw.write("\tpublic List<" + beanName + "> selectListByParam(Map<String,String> param);");
        bw.newLine();
        //根据条件查询分页
        bw = buildMethodComment(bw, "查询（根据条件查询）");
        bw.newLine();
        bw.write("\tpublic List<" + beanName + "> selectPageByParam(Map<String,String> param,Page page);");
        bw.newLine();
      //根据条件查询数量
        bw = buildMethodComment(bw, "查询（根据条件查询数量）");
        bw.newLine();
        bw.write("\tpublic int selectCountByParam(Map<String,String> param);");
        bw.newLine();
        
        bw = buildMethodComment(bw, "删除（根据主键ID删除）");
        if(column!=null){
        	bw.newLine();
        	bw.write("\tpublic int deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
        }else{
        	 bw.newLine();
             bw.write("\t" + "public int deleteById(Integer id);");
        }
        
        bw.newLine();
        bw = buildMethodComment(bw, "删除（根据条件删除）");
        bw.newLine();
        bw.write("\t" + "public int deleteByParam(Map<String,String> param);");
        bw.newLine();
        
        bw = buildMethodComment(bw, "添加（匹配有值的字段）");
        bw.newLine();
        bw.write("\t" + "public int insertSelective(" + beanName +" "+  this.processResultMapId(beanName)+");");
        bw.newLine();
        bw = buildMethodComment(bw, "修改（根据主键ID修改）");  
        if(column!=null){
        	bw.newLine();
        	bw.write("\tpublic int updateBy"+Constant.getFirstUpper(column.getFiledName())+"(" + beanName + " "+Constant.getFirstLower(beanName)+");");
        }else{
        	 bw.newLine();
             bw.write("\tpublic int updateById(" + beanName + " "+Constant.getFirstLower(beanName)+");");
        }
        
        bw.newLine();
        bw = buildMethodComment(bw, "批量修改（根据条件修改）");
        bw.newLine();
        bw.write("\t" + "public int updateByParam(" + beanName + " "+  this.processResultMapId(beanName)+",Map<String,String> param);");
        bw.newLine();
 
        // ----------定义Mapper中的方法End----------
        bw.newLine();
        bw.write("}");
        bw.flush();
        bw.close();
    }
    /**
     *  构建Impl文件
     *
     * @throws IOException 
     */
    private void buildServiceImpl(Table table) throws IOException {
    	File folder = new File(service_impl_path);
    	if( !folder.exists() ) {
    		folder.mkdirs();
    	}
    	
    	String beanName = table.getEntityName();
    	File serviceImplFile = new File(service_impl_path, table.getServiceImplName() + ".java");
    	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(serviceImplFile), "utf-8"));
    	bw.write("package " + service_impl_package + ";");
    	bw.newLine();
    	bw.write("import " + bean_package + "." + beanName + ";");
    	bw.newLine();
    	bw.write("import java.util.Map;");
    	bw.newLine();
    	bw.write("import java.util.List;");
    	bw.newLine();
    	bw.write("import org.springframework.beans.factory.annotation.Autowired;");
    	bw.newLine();
    	bw.write("import org.springframework.stereotype.Service;");
    	bw.newLine();
    	bw.write("import "+base_package+".entity.domain.Page;");
    	bw.newLine();
    	bw.write("import "+base_package+"."+"service."+table.getServiceName()+ ";");
    	bw.newLine();
    	bw.write("import " + mapper_package + "." + table.getDaoName() + ";");
    	bw = buildClassComment(bw, table.getComment() + "服务实现类");
    	bw.newLine();
    	//      bw.write("public interface " + mapperName + " extends " + mapper_extends + "<" + beanName + "> {");
    	bw.write("@Service");
    	bw.newLine();
    	bw.write("public class " + table.getServiceImplName() + " implements "+table.getServiceName()+"{");
    	bw.newLine();
    	bw.write("\t@Autowired");
    	bw.newLine();
    	String beanDao =  Constant.getLostFirstAndLower(table.getDaoName());
    	bw.write("\tprivate "+table.getDaoName()+" "+ beanDao+";");
    	bw.newLine();
    	// ----------定义Mapper中的方法Begin----------
    	Column column = table.getPK();
    	bw = buildMethodComment(bw, "修改（根据主键ID修改）");
    	bw.newLine();
        if(column!=null){
        	bw.write("\tpublic " + beanName + " selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+"){");
            bw.newLine();
            bw.write("\t\treturn " + beanDao + ".selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFiledName()+");");
            bw.newLine();
        	bw.write("\t}");
        	bw.newLine();
        }
    	//根据条件查询对象
    	bw = buildMethodComment(bw, "对象查询（根据条件查询对象）");
    	bw.newLine();
    	bw.write("\tpublic " + beanName + " selectByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\tPage page=new Page();");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> list = "+beanDao+".selectPageByParam(param,page);");
    	bw.newLine();
    	bw.write("\t\t"+beanName+" "+Constant.getFirstLower(beanName)+" = null;");
    	bw.newLine();
    	bw.write("\t\tif(list != null && list.size() > 0){");
    	bw.newLine();
    	bw.write("\t\t\t"+Constant.getFirstLower(beanName)+" = list.get(0);");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件查询列表
    	bw = buildMethodComment(bw, "列表查询（根据条件查询）");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectListByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanDao+".selectListByParam(param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件查询 分页
    	bw = buildMethodComment(bw, "分页查询（根据条件查询）");
    	bw.newLine();
    	bw.write("\tpublic List<"+beanName+"> selectPageByParam(Map<String,String> param,Page page){");
    	bw.newLine();
    	bw.write("\t\tparam.put(\"offset\", String.valueOf(page.getStartCount()));");
    	bw.newLine();
    	bw.write("\t\tparam.put(\"limit\", String.valueOf(page.getPageSize()));");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> list = "+beanDao+".selectListByParam(param);");
    	bw.newLine();
    	bw.write("\t\tint totalCount = "+beanDao+".selectCountByParam(param);");
    	bw.newLine();
    	bw.write("\t\tpage.setTotalCount(totalCount);");
    	bw.newLine();
    	bw.write("\t\treturn list;");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件查询数量
    	bw = buildMethodComment(bw, "查询（根据条件查询数量）");
    	bw.newLine();
    	bw.write("\tpublic int selectCountByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanDao+".selectCountByParam(param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	//删除根据id
    	bw = buildMethodComment(bw, "查询（根据主键查询）");
     	bw.newLine();  
    	 if(column!=null){
         	bw.write("\tpublic int deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+"){");
            bw.newLine();
            bw.write("\t\treturn " + beanDao + ".deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFiledName()+");");
            bw.newLine();
         	bw.write("\t}");
         	bw.newLine();
         }else{
         	bw.write("\t" + "public int deleteById(Integer id){");
         	bw.newLine();
         	bw.write("\t\treturn "+beanDao+".deleteById(id);");
         	bw.newLine();
         	bw.write("\t}");
         }

    	//批量删除
    	bw.newLine();
    	bw = buildMethodComment(bw, "删除（根据条件删除）");
    	bw.newLine();
    	bw.write("\t" + "public int deleteByParam(Map<String,String> param){");
    	bw.newLine();
    	String listStr = Constant.getFirstLower(beanName)+"List";
    	bw.write("\t\tList<"+beanName+"> "+listStr+" = this.selectListByParam(param);");
    	bw.newLine();
    	bw.write("\t\tif("+listStr+"==null || "+listStr+".isEmpty()){");
    	bw.newLine();
    	bw.write("\t\t\treturn 0;");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();	
    	bw.write("\t\tint count = 0;");
    	bw.newLine();
    	bw.write("\t\tfor("+beanName+" "+Constant.getFirstLower(beanName)+":"+listStr+"){");
    	bw.newLine();
    	
    	if(column!=null){
	    	bw.write("\t\t\tif("+processResultMapId(beanName)+".get"+Constant.getFirstUpper(column.getFiledName())+"()==null){continue;}");
	    	bw.newLine();
	    	bw.write("\t\t\tcount +="+beanDao+".deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+Constant.getFirstLower(beanName)+".get"+Constant.getFirstUpper(column.getFiledName())+"());");
	    	bw.newLine();
    	}else{
    		bw.write("\t\t\tif("+Constant.getFirstLower(beanName)+".getId()==null){continue;}");
	    	bw.newLine();
	    	bw.write("\t\t\tcount +="+beanDao+".deleteById("+Constant.getFirstLower(beanName)+".getId());");
	    	bw.newLine();
    	}
    	
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t\treturn count;");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//添加方法
    	bw = buildMethodComment(bw, "添加（匹配有值的字段）");
    	bw.newLine();
    	bw.write("\t" + "public int insertSelective(" + beanName +" "+  this.processResultMapId(beanName)+"){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanDao+".insertSelective("+  this.processResultMapId(beanName)+");");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	if(column!=null){
	    	
	    	bw.write("\t" + "public int updateBy"+Constant.getFirstUpper(column.getFiledName())+"(" + beanName +" "+  this.processResultMapId(beanName)+"){");
	    	bw.newLine();
	    	bw.write("\t\treturn "+beanDao+".updateBy"+Constant.getFirstUpper(column.getFiledName())+"("+  this.processResultMapId(beanName)+");");
	    	bw.newLine();
	    	bw.write("\t}");
	    	bw.newLine();
    	}else{
    		bw = buildMethodComment(bw, "修改（根据主键ID修改）");
	    	bw.newLine();
	    	bw.write("\t" + "public int updateById(" + beanName +" "+  this.processResultMapId(beanName)+"){");
	    	bw.newLine();
	    	bw.write("\t\treturn "+beanDao+".updateById("+  this.processResultMapId(beanName)+");");
	    	bw.newLine();
	    	bw.write("\t}");
	    	bw.newLine();
    	}
    	
    	//批量修改
    	bw.newLine();
    	bw = buildMethodComment(bw, "批量修改（根据条件批量修改）");
    	bw.newLine();
    	bw.write("\t" + "public int updateByParam("+beanName+" "+processResultMapId(beanName)+",Map<String,String> param){");
    	bw.newLine();
//    	String listStr = processResultMapId(beanName)+"List";
    	bw.write("\t\tList<"+beanName+"> "+listStr+" = this.selectListByParam(param);");
    	bw.newLine();
    	bw.write("\t\tif("+listStr+"==null || "+listStr+".isEmpty()){");
    	bw.newLine();
    	bw.write("\t\t\treturn 0;");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();	
    	bw.write("\t\tint count = 0;");
    	bw.newLine();
    	
    	String tempName = processResultMapId(beanName)+"Temp";
    	bw.write("\t\tfor("+beanName+" "+tempName+":"+listStr+"){");
    	bw.newLine();
    	if(column!=null){
    		bw.write("\t\t\tif("+tempName+".get"+Constant.getFirstUpper(column.getFiledName())+"()==null){continue;}");
    		bw.newLine();
    		bw.write("\t\t\tcount +=this.updateBy"+Constant.getFirstUpper(column.getFiledName())+"("+tempName+");");
    		bw.newLine();
    	}else{
    		bw.write("\t\t\tif("+tempName+".getId()==null){continue;}");
        	bw.newLine();
        	bw.write("\t\t\tcount +=this.updateById("+tempName+");");
        	bw.newLine();
    	}
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t\treturn count;");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	// ----------定义Mapper中的方法End----------
    	bw.newLine();
    	bw.write("}");
    	bw.flush();
    	bw.close();
    }
    /**
     *  构建Business文件(implements)
     *
     * @throws IOException 
     */
    private void buildBusiness(Table table) throws IOException {
    	File folder = new File(business_path);
    	if( !folder.exists() ) {
    		folder.mkdirs();
    	}
    	String beanName = table.getEntityName();
    	File businessFile = new File(business_path, table.getBusinessName() + ".java");
    	
    	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(businessFile), "utf-8"));
    	bw.write("package " + business_package + ";");
    	bw.newLine();
    	bw.write("import " + bean_package + "." + beanName + ";");
    	bw.newLine();
    	bw.write("import java.util.Map;");
    	bw.newLine();
    	bw.write("import java.util.List;");
    	bw.newLine();
    	bw.write("import "+base_package+".entity.domain.Page;");
    	bw = buildClassComment(bw, beanName + "业务实现接口");
    	bw.newLine();
    	//      bw.write("public interface " + mapperName + " extends " + mapper_extends + "<" + beanName + "> {");
    	bw.write("public interface " + table.getBusinessName() + "{");
    	bw.newLine();
    	// ----------定义Mapper中的方法Begin----------
    	//根据id查询用户
    	Column column = table.getPK();
    	bw = buildMethodComment(bw, "查询（根据主键查询）简单对象");
    	bw.newLine();
        if(column!=null){
        	
        	bw.write("\tpublic " + beanName + " selectSimpleBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
        	bw.newLine();
        }else{
        	bw.write("\tpublic " + beanName + " selectSimpleById(Integer id);");
        	bw.newLine();
        }
        
        bw = buildMethodComment(bw, "查询（根据主键查询）对象");
    	bw.newLine();
        if(column!=null){
        	
        	bw.write("\tpublic " + beanName + " selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
        	bw.newLine();
        }else{
        	bw.write("\tpublic " + beanName + " selectById(Integer id);");
        	bw.newLine();
        }
    	
      //根据条件获取实体
    	bw = buildMethodComment(bw, "根据条件获取简单实体");
    	bw.newLine();
    	bw.write("\tpublic " + beanName + " selectSimpleByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	//根据条件获取实体
    	bw = buildMethodComment(bw, "根据条件获取实体");
    	bw.newLine();
    	bw.write("\tpublic " + beanName + " selectByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	
    	//根据条件简单查询
    	bw = buildMethodComment(bw, "根据条件简单查询");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectSimpleListByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	//根据条件查询
    	bw = buildMethodComment(bw, "根据条件查询");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectListByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	
    	//分页查询（根据条件查询）
    	bw = buildMethodComment(bw, "分页查询（根据条件简单查询）");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectSimplePageByParam(Map<String,String> param,Page page);");
    	bw.newLine();
    	//分页查询（根据条件查询）
    	bw = buildMethodComment(bw, "分页查询（根据条件查询）");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectPageByParam(Map<String,String> param,Page page);");
        bw.newLine();
        
    	//总数查询（根据条件查询）
    	bw = buildMethodComment(bw, "总数查询（根据条件查询）");
    	bw.newLine();
    	bw.write("\tpublic int selectCountByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "删除（根据主键ID删除）");
    	bw.newLine();
    	if(column!=null){
    		bw.write("\t" + "public int deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+");");
    	}else{
    		bw.write("\t" + "public int deleteById(Integer id);");
    	}
    	
    	bw.newLine();
    	
    	
    	bw = buildMethodComment(bw, "删除（根据条件批量删除）");
    	bw.newLine();
    	bw.write("\t" + "public int deleteByParam(Map<String,String> param);");
    	bw.newLine();
    	
    	
    	bw = buildMethodComment(bw, "添加（匹配有值的字段）");
    	bw.newLine();
    	bw.write("\t" + "public int insertSelective(" + beanName +" "+  this.processResultMapId(beanName)+");");
    	bw.newLine();
    	bw = buildMethodComment(bw, "修改（根据主键ID修改）");
    	bw.newLine();
    	if(column!=null){
    		bw.write("\t" + "public int updateBy"+Constant.getFirstUpper(column.getFiledName())+"(" + beanName +" "+Constant.getFirstLower(beanName)+");");
    	}else{
    		bw.write("\t" + "public int updateById(" + beanName + " record);");
    	}
    	
    	bw.newLine();
    	bw = buildMethodComment(bw, "根据条件修改");
    	bw.newLine();
    	bw.write("\t" + "public int updateByParam(" + beanName +" "+  this.processResultMapId(beanName)+",Map<String,String> param);");
    	bw.newLine();
    	
    	// ----------定义Mapper中的方法End----------
    	bw.newLine();
    	bw.write("}");
    	bw.flush();
    	bw.close();
    }
    /**
     *  构建Impl文件
     *
     * @throws IOException 
     */
    private void buildBusinessImpl(Table table) throws IOException {
    	File folder = new File(business_impl_path);
    	if( !folder.exists() ) {
    		folder.mkdirs();
    	}
    	String beanName  = table.getEntityName();
    	File businessImplFile = new File(business_impl_path, table.getBusinessImplName() + ".java");
    	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(businessImplFile), "utf-8"));
    	bw.write("package " + business_impl_package + ";");
    	bw.newLine();
    	bw.write("import " + bean_package + "." + beanName + ";");
    	bw.newLine();
    	bw.write("import java.util.Map;");
    	bw.newLine();
    	bw.write("import java.util.List;");
    	bw.newLine();
    	bw.write("import org.springframework.stereotype.Service;");
    	bw.newLine();
    	bw.write("import org.springframework.stereotype.Component;");
    	bw.newLine();
    	bw.write("import org.springframework.beans.factory.annotation.Autowired;");
    	bw.newLine();
    	bw.write("import "+base_package+".entity.domain.Page;");
    	bw.newLine();
    	bw.write("import javax.annotation.Resource;");
    	bw.newLine();
    	bw.write("import "+business_package+"."+table.getBusinessName()+";");
    	bw.newLine();
    	bw.write("import " +service_package + "." + table.getServiceName() + ";");
    	bw = buildClassComment(bw, table.getComment() + "业务实现");
    	bw.newLine();
    	//      bw.write("public interface " + mapperName + " extends " + mapper_extends + "<" + beanName + "> {");
    	bw.write("@Component");
    	bw.newLine();
    	bw.write("public class " + table.getBusinessImplName() + " implements "+table.getBusinessName()+"{");
    	bw.newLine();
    	bw.write("\t@Autowired");
    	bw.newLine();
    	String beanService =  Constant.getLostFirstAndLower(table.getServiceName());
    	bw.write("\tprivate "+table.getServiceName()+" "+ beanService+";");
    	bw.newLine();
    	// ----------定义Mapper中的方法Begin----------
    	
    	bw = buildMethodComment(bw, "查询（根据主键查询）简单对象");
    	bw.newLine();
    	Column column = table.getPK();
        if(column!=null){
    		bw.write("\tpublic " + beanName + " selectSimpleBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+"){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFiledName()+");");
        	bw.newLine();
    	}else{
    		bw.write("\tpublic " + beanName + " selectSimpleById(Integer id){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".selectById(id);");
        	bw.newLine();
    	}
    	bw.write("\t}");
    	bw.newLine();
    	
    	
    	bw = buildMethodComment(bw, "查询（根据主键查询）对象");
    	bw.newLine();
        if(column!=null){
    		bw.write("\tpublic " + beanName + " selectBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+"){");
        	bw.newLine();
        	bw.write("\t\t"+beanName+" "+Constant.getFirstLower(beanName)+" = selectSimpleBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFiledName()+");");
        	bw.newLine();
        	bw.write("\t\t\tpackageEntity("+Constant.getFirstLower(beanName)+");");
        	bw.newLine();
        	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+";");
        	bw.newLine();
    	}else{
    		bw.write("\tpublic " + beanName + " selectById(Integer id){");
        	bw.newLine();
        	bw.write("\t\t"+beanName+" "+Constant.getFirstLower(beanName)+" = selectSimpleById(Integer id);");
        	bw.newLine();
        	bw.write("\t\t\tpackageEntity("+Constant.getFirstLower(beanName)+");");
        	bw.newLine();
        	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+";");
        	bw.newLine();
    	}
    	bw.write("\t}");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "查询（根据条件查询）简单对象");
    	bw.newLine();
    	bw.write("\tpublic " + beanName + " selectSimpleByParam(Map<String,String> param) {");
    	bw.newLine();
    	bw.write("\t\treturn "+beanService+".selectByParam(param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "查询（根据条件查询对象）对象");
    	bw.newLine();
    	bw.write("\tpublic " + beanName + " selectByParam(Map<String,String> param) {");
    	bw.newLine();
    	bw.write("\t\t"+beanName+" "+Constant.getFirstLower(beanName)+" = selectSimpleByParam(param);");
    	bw.newLine();
    	bw.write("\t\t\tpackageEntity("+Constant.getFirstLower(beanName)+");");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件简单查询
    	bw = buildMethodComment(bw, "查询（根据条件查询）简单列表对象");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectSimpleListByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> "+Constant.getFirstLower(beanName)+"List = "+beanService+".selectListByParam(param);");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+"List"+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件查询
    	bw = buildMethodComment(bw, "查询（根据条件查询）列表对象");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + ">  selectListByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> "+Constant.getFirstLower(beanName)+"List = selectSimpleListByParam(param);");
    	bw.newLine();
    	bw.write("\t\tif("+Constant.getFirstLower(beanName)+"List!=null &&"+Constant.getFirstLower(beanName)+"List.size()>0) {");
    	bw.newLine();
    	bw.write("\t\t\tfor ("+beanName+" "+Constant.getFirstLower(beanName)+" : "+Constant.getFirstLower(beanName)+"List) {");
    	bw.newLine();
    	bw.write("\t\t\t\tpackageEntity("+Constant.getFirstLower(beanName)+");");
    	bw.newLine();
    	bw.write("\t\t\t}");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+"List"+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	//根据条件查询分页
    	bw = buildMethodComment(bw, "查询（根据条件查询 分页）");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + "> selectSimplePageByParam(Map<String,String> param,Page page){");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> "+Constant.getFirstLower(beanName)+"List = "+beanService+".selectPageByParam(param, page);");
    	bw.newLine();
    	bw.write("\t\tpage.setRows("+Constant.getFirstLower(beanName)+"List);");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+"List"+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	//根据条件查询分页
    	bw = buildMethodComment(bw, "查询（根据条件查询 分页）");
    	bw.newLine();
    	bw.write("\tpublic List<" + beanName + "> selectPageByParam(Map<String,String> param,Page page){");
    	bw.newLine();
    	bw.write("\t\tList<"+beanName+"> "+Constant.getFirstLower(beanName)+"List = selectSimplePageByParam(param, page);");
    	bw.newLine();
    	bw.write("\t\tif("+Constant.getFirstLower(beanName)+"List!=null &&"+Constant.getFirstLower(beanName)+"List.size()>0) {");
    	bw.newLine();
    	bw.write("\t\t\tfor ("+beanName+" "+Constant.getFirstLower(beanName)+" : "+Constant.getFirstLower(beanName)+"List) {");
    	bw.newLine();
    	bw.write("\t\t\t\tpackageEntity("+Constant.getFirstLower(beanName)+");");
    	bw.newLine();
    	bw.write("\t\t\t}");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t\tpage.setRows("+Constant.getFirstLower(beanName)+"List);");
    	bw.newLine();
    	bw.write("\t\treturn "+Constant.getFirstLower(beanName)+"List"+";");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	//根据条件查询数量
    	bw = buildMethodComment(bw, "查询（根据条件查询数量）");
    	bw.newLine();
    	bw.write("\tpublic int selectCountByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanService+".selectCountByParam(param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "删除（根据主键ID删除）");
    	
    	if(column!=null){
    		bw.newLine();
        	bw.write("\t" + "public int deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFtype()+" "+column.getFiledName()+"){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".deleteBy"+Constant.getFirstUpper(column.getFiledName())+"("+column.getFiledName()+");");
    	}else{
    		bw.newLine();
        	bw.write("\t" + "public int deleteById(Integer id){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".deleteById(id);");
    	}
    	
    	
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "批量删除（根据条件批量删除），先查询后单条删除");
    	bw.newLine();
    	bw.write("\t" + "public int deleteByParam(Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanService+".deleteByParam(param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	
    	bw = buildMethodComment(bw, "添加（匹配有值的字段）");
    	bw.newLine();
    	bw.write("\t" + "public int insertSelective(" + beanName +" "+  this.processResultMapId(beanName)+"){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanService+".insertSelective("+  this.processResultMapId(beanName)+");");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	bw = buildMethodComment(bw, "修改（根据主键ID修改）");
    	if(column!=null){
    		bw.newLine();
        	bw.write("\t" + "public int updateBy"+Constant.getFirstUpper(column.getFiledName())+"(" + beanName +" "+  this.processResultMapId(beanName)+"){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".updateBy"+Constant.getFirstUpper(column.getFiledName())+"("+  this.processResultMapId(beanName)+");");	
    	}else{
    		bw.newLine();
        	bw.write("\t" + "public int updateById(" + beanName +" "+  this.processResultMapId(beanName)+"){");
        	bw.newLine();
        	bw.write("\t\treturn "+beanService+".updateById("+  this.processResultMapId(beanName)+");");
    	}
    	
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	
    	bw = buildMethodComment(bw, "修改（根据条件批量修改）");
    	bw.newLine();
    	bw.write("\t" + "public int updateByParam(" + beanName +" "+  this.processResultMapId(beanName)+",Map<String,String> param){");
    	bw.newLine();
    	bw.write("\t\treturn "+beanService+".updateByParam(" + this.processResultMapId(beanName) +",param);");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();   	
    	
    	bw = buildMethodComment(bw, "封装（复杂的对象）");
    	bw.newLine();
    	bw.write("\t" + "public void packageEntity(" + beanName +" "+  this.processResultMapId(beanName)+"){");
    	bw.newLine();
    	bw.write("\t\tif("+this.processResultMapId(beanName) +"==null){");
    	bw.newLine();
    	bw.write("\t\t\treturn;");
    	bw.newLine();
    	bw.write("\t\t}");
    	bw.newLine();
    	bw.write("\t}");
    	bw.newLine();
    	// ----------定义Mapper中的方法End----------
    	bw.newLine();
    	bw.write("}");
    	bw.flush();
    	bw.close();
    }
    
 
    
    /**
     *  构建实体类映射XML文件
     *
     * @param columns
     * @param types
     * @param comments
     * @throws IOException 
     */
    private void buildMapperXml(Table table) throws IOException {
        File folder = new File(xml_path);
        if( !folder.exists() ) {
            folder.mkdirs();
        }
        String beanName = table.getEntityName();
        String mapXmlName = Constant.startStr_dao_xml+table.getEntityName()+Constant.endStr_dao_xml;
        File mapperXmlFile = new File(xml_path, mapXmlName + ".xml");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(mapperXmlFile)));
        bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        bw.newLine();
        bw.write("<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" ");
        bw.newLine();
        bw.write(" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">");
        bw.newLine();
        bw.write("<mapper namespace=\"" + mapper_package + "." + mapXmlName + "\">");
        bw.newLine();
        bw.newLine();
 
        /**/
        bw.write("\t<!--实体映射-->");
        bw.newLine();
        bw.write("\t<resultMap id=\"BaseResultMap\" type=\"" + bean_package+"."+ beanName + "\">");
        //注释
        /*
        bw.newLine();
        bw.write("\t\t<!--" + comments.get(0) + "-->");
        */
        Column column = table.getPK();
        bw.newLine();
        if(column!=null){
        	bw.write("\t\t<id property=\"" + column.getFiledName() + "\" column=\"" + column.getColumnName() + "\" />");
            bw.newLine();	
        }
        
        List<Column> clist = table.getColumnList();
        int size = clist.size();
        for( int i = 1 ; i < size ; i++ ) {
        	//注释
        	/*
            bw.write("\t\t<!--" + comments.get(i) + "-->");
            bw.newLine();
            */
        	Column column_temp = clist.get(i);
            bw.write("\t\t<result property=\""
                    + column_temp.getFiledName() + "\" column=\"" + column_temp.getColumnName()+ "\" />");
            bw.newLine();
        }
        bw.write("\t</resultMap>");
 
        bw.newLine();
        bw.newLine();
        bw.newLine();
 
        // 下面开始写SqlMapper中的方法
        // this.outputSqlMapperMethod(bw, columns, types);
        buildSQL(bw,table);
 
        bw.write("</mapper>");
        bw.flush();
        bw.close();
    }
 
 
    private void buildSQL( BufferedWriter bw, Table table) throws IOException {
    	List<Column> columns = table.getColumnList();
        int size = columns.size();
        // 通用结果列
        bw.write("\t<!-- 通用查询结果列-->");
        bw.newLine();
        bw.write("\t<sql id=\"Base_Column_List\">");
        bw.newLine();
 
        StringBuffer sb = new StringBuffer();
        for( int i = 0 ; i < size ; i++ ) {
            sb.append(",").append(columns.get(i).getColumnName());
        }
        bw.write("\t\t"+sb.toString().substring(1));
        bw.newLine();
        bw.write("\t</sql>");
        bw.newLine();
        bw.newLine();
       
        //生成Base_Conditions
        bw.write("\t<!-- 通用查询条件-->");
        bw.newLine();
        bw.write("\t<sql id=\""+ this.processResultMapId(table.getEntityName())+"_where_conditions\">");
        bw.newLine();
        bw.write("\t\t<trim prefix=\" WHERE \" suffix=\"\" prefixOverrides=\"AND|OR\" >");
        bw.newLine();
        String tempField = null;
        for(int i = 0 ; i < size ; i++ ) {
        	 Column column = columns.get(i);
        	 tempField = column.getFiledName();
             bw.write("\t\t\t<if test=\"" + tempField + " != null\">");
             bw.write(" AND " + column.getColumnName());
             bw.write(" = #{" + tempField + "}");
             bw.write("</if>");
             bw.newLine();
             String ptype= column.getFtype();
             if("Date".equals(ptype)){
            	 //小于
            	 bw.write("\t\t\t<!--" + tempField + "时间范围查询开始-->");
            	 bw.newLine();
            	 bw.write("\t\t\t<if test=\"lt_" + tempField + " != null\">");
                 bw.write(" AND " +column.getColumnName());
                 bw.write(" &lt; #{lt_" + tempField + "}");
                 bw.write("</if>");
                 bw.newLine();
                 //小于等于
                 bw.write("\t\t\t<if test=\"le_" + tempField + " != null\">");
                 bw.write(" AND " + column.getColumnName());
                 bw.write(" &lt;= #{le_" + tempField + "}");
                 bw.write("</if>");
                 bw.newLine();
                 //小于等于
                 bw.write("\t\t\t<if test=\"gt_" + tempField + " != null\">");
                 bw.write(" AND " + column.getColumnName());
                 bw.write(" &gt; #{gt_" + tempField + "}");
                 bw.write("</if>");
                 bw.newLine();
                 //小于等于
                 bw.write("\t\t\t<if test=\"ge_" + tempField + " != null\">");
                 bw.write(" AND " + column.getColumnName());
                 bw.write(" &gt;= #{ge_" + tempField + "}");
                 bw.write("</if>");
                 bw.newLine();
                 bw.write("\t\t\t<!--" + tempField + "时间范围查询结束-->");
                 bw.newLine();
             }
             if("String".equals(ptype)){
            	 bw.write("\t\t\t<!--" + tempField + "模糊查询-->");
                 bw.newLine();
            	 bw.write("\t\t\t<if test=\"l_" + tempField + " != null\">");
                 bw.write(" AND " + column.getColumnName() );
                 bw.write(" LIKE CONCAT('%',#{l_" + tempField + "},'%')");
                 bw.write("</if>");
                 bw.newLine();
             }
             if("sts".equalsIgnoreCase(tempField)){
            	 //添加公共的sts
                 bw.write("\t\t\t<if test=\"in_sts != null\"> AND FIND_IN_SET(STS,#{in_sts})</if>");
                 bw.newLine();
                 bw.write("\t\t\t<if test=\"no_sts != null\"> AND NOT FIND_IN_SET(STS,#{no_sts})</if>");
                 bw.newLine();
             }
        }
        bw.write("\t\t</trim>");
        bw.newLine();
        bw.write("\t</sql>");
        bw.newLine();
        
        
        //生成OrderByFragment
        bw.write("\t<!-- 通用排序-->");
        bw.newLine();
        bw.write("\t<sql id=\"OrderByFragment\">");
        bw.newLine();
        bw.write("\t\t<if test=\"orderBy!= null\">");
        bw.newLine();
	    bw.write("\t\t\tORDER BY " + "${orderBy} " );
	    bw.newLine();
        bw.write("\t\t</if>");
        bw.newLine();
        bw.write("\t</sql>");
        bw.newLine();
        
        
	    //生成LimitFragment
        bw.write("\t<!-- 通用limit-->");
        bw.newLine();
        bw.write("\t<sql id=\"LimitFragment\">");
        bw.newLine();
        bw.write("\t\t<if test=\"limit!= null\">");
        bw.newLine();
	    bw.write("\t\t\t LIMIT ${offset},${limit}" );
	    bw.newLine();
        bw.write("\t\t</if>");
        bw.newLine();
        bw.write("\t</sql>");
        bw.newLine();
 
        
        // 查询（根据主键ID查询）
        bw.write("\t<!-- 查询（根据主键查询） -->");
        bw.newLine();
        Column column = table.getPK();
        if(column!=null){
        	bw.write("\t<select id=\"selectBy"+Constant.getFirstUpper(column.getFiledName())+"\" resultMap=\"BaseResultMap\" parameterType=\"" + column.getAllFtype() + "\">");
        	bw.newLine();
            bw.write("\t\t SELECT");
//            bw.newLine();
            bw.write(" <include refid=\"Base_Column_List\" />");
//            bw.newLine();
            bw.write(" FROM " + table.getTableName());
            //bw.newLine();
            bw.write(" WHERE " + column.getColumnName() + " = #{" + column.getFiledName() + "}");
        }else{
        	bw.write("\t<select id=\"selectById\" resultMap=\"BaseResultMap\" parameterType=\"java.lang.Integer\">");
        	bw.newLine();
            bw.write("\t\t SELECT");
//            bw.newLine();
            bw.write(" <include refid=\"Base_Column_List\" />");
//            bw.newLine();
            bw.write(" FROM " + table.getTableName());
            //bw.newLine();
            bw.write(" WHERE id  = #{id}");
        }
        
        bw.newLine();
        bw.write("\t</select>");
        bw.newLine();
        bw.newLine();
        // 查询完
        
        //根据条件查询
       bw.write("\t<!--根据条件查询-->");
       bw.newLine();
       bw.write("\t<select id=\"selectListByParam\" resultMap=\"BaseResultMap\" parameterType=\"java.util.HashMap\">");
       bw.newLine();
       bw.write("\t\tSELECT");
       bw.newLine();
       bw.write("\t\t<include refid=\"Base_Column_List\"/>");
       bw.newLine();
       bw.write("\t\tFROM "+ table.getTableName() );
       bw.newLine();
       bw.write("\t\t<include refid=\""+ Constant.getFirstLower(table.getEntityName())+"_where_conditions\"/>");
       bw.newLine();
       bw.write("\t\t<include refid=\"OrderByFragment\"/>");
       bw.newLine();
       bw.write("\t\t<include refid=\"LimitFragment\"/>");
       bw.newLine();
       bw.write("\t</select>");
       bw.newLine();
       
       //根据条件查询数量
       bw.write("\t<!--根据条件查询-->");
       bw.newLine();
       bw.write("\t<select id=\"selectCountByParam\" resultType=\"int\" parameterType=\"java.util.HashMap\">");
       bw.newLine();
       bw.write("\t\tSELECT COUNT(1) FROM "+ table.getTableName() );
       bw.newLine();
       bw.write("\t\t<include refid=\""+ Constant.getFirstLower(table.getEntityName())+"_where_conditions\"/>");
       bw.newLine();
       bw.write("\t</select>");
       bw.newLine();
       
        // 删除（根据主键ID删除）
        bw.write("\t<!--删除：根据主键ID删除-->");
        bw.newLine();
        if(column!=null){
        	bw.write("\t<delete id=\"deleteBy"+Constant.getFirstUpper(column.getFiledName())+"\" parameterType=\"" + column.getAllFtype() + "\">");
        	bw.newLine();
            bw.write("\t\t DELETE FROM " + table.getTableName());
//            bw.newLine();
            bw.write(" WHERE " + column.getColumnName() + " = #{" + column.getFiledName() + "}");
            bw.newLine();
        }else{
        	bw.write("\t<delete id=\"deleteById\" parameterType=\"java.lang.Integer\">");
        	bw.newLine();
            bw.write("\t\t DELETE FROM " + table.getTableName());
//            bw.newLine();
            bw.write(" WHERE id = #{id}");
            bw.newLine();
        }
        
        
        bw.write("\t</delete>");
        bw.newLine();
        bw.newLine();
        // 删除完
        
//        // 删除（根据条件删除）
//        bw.write("\t<!--删除：根据条件删除-->");
//        bw.newLine();
//        bw.write("\t<delete id=\"deleteByParam\" parameterType=\"java.util.HashMap\">");
//        bw.newLine();
//        bw.write("\t\t DELETE FROM " + tableName);
////        bw.newLine();
////        bw.write(" WHERE " + columns.get(0) + " = #{" + processField(columns.get(0)) + "}");
//        bw.newLine();
//        bw.write("\t\t<include refid=\""+ this.processResultMapId(beanName)+"_where_conditions\"/>");
//        bw.newLine();
//        bw.write("\t</delete>");
//        bw.newLine();
//        bw.newLine();
// 
 
        // 添加insert方法
        /*bw.write("\t<!-- 添加 -->");
        bw.newLine();
        bw.write("\t<insert id=\"insert\" parameterType=\"" + processResultMapId(beanName) + "\">");
        bw.newLine();
        bw.write("\t\t INSERT INTO " + tableName);
        bw.newLine();
        bw.write(" \t\t(");
        for( int i = 0 ; i < size ; i++ ) {
            bw.write(columns.get(i));
            if( i != size - 1 ) {
                bw.write(",");
            }
        }
        bw.write(") ");
        bw.newLine();
        bw.write("\t\t VALUES ");
        bw.newLine();
        bw.write(" \t\t(");
        for( int i = 0 ; i < size ; i++ ) {
            bw.write("#{" + processField(columns.get(i)) + "}");
            if( i != size - 1 ) {
                bw.write(",");
            }
        }
        bw.write(") ");
        bw.newLine();
        bw.write("\t</insert>");
        bw.newLine();
        bw.newLine();*/
        // 添加insert完
 
 
        //---------------  insert方法（匹配有值的字段）
        bw.write("\t<!-- 添加 （匹配有值的字段）-->");
        bw.newLine();
        if(column!=null){
        	bw.write("\t<insert id=\"insertSelective\" useGeneratedKeys=\"true\" keyProperty=\""+column.getFiledName()+"\" parameterType=\"" +bean_package+"."+table.getEntityName() + "\">");	
        }else{
        	bw.write("\t<insert id=\"insertSelective\"  parameterType=\"" +bean_package+"."+table.getEntityName() + "\">");
        }
        
        bw.newLine();
        bw.write("\t\t INSERT INTO " + table.getTableName());
        bw.newLine();
        bw.write("\t\t <trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\" >");
        bw.newLine();
 
        tempField = null;
        for( int i = 0 ; i < size ; i++ ) {
        	Column ctemp = columns.get(i);
            tempField = ctemp.getFiledName();
            bw.write("\t\t\t<if test=\"" + tempField + " != null\">");
            bw.write("" +ctemp.getColumnName() + ",");
            bw.write("</if>");
            	bw.newLine();
        }
        bw.write("\t\t </trim>");
        bw.newLine();
        bw.write("\t\t <trim prefix=\"values(\" suffix=\")\" suffixOverrides=\",\" >");
        bw.newLine();
        tempField = null;
        for( int i = 0 ; i < size ; i++ ) {
        	Column ctemp = columns.get(i);
            tempField = ctemp.getFiledName();
            bw.write("\t\t\t<if test=\"" + tempField + "!=null\">");
            bw.write(" #{" + tempField + "},");
            bw.write("</if>");
            bw.newLine();
        }
 
        bw.write("\t\t </trim>");
        bw.newLine();
        bw.write("\t</insert>");
        bw.newLine();
        bw.newLine();
        //---------------  完毕
 
 
        // 修改update方法
        bw.write("\t<!-- 修 改（匹配有值得字段）-->");
        bw.newLine();
        if(column!=null){
        	bw.write("\t<update id=\"updateBy"+Constant.getFirstUpper(column.getFiledName())+"\" parameterType=\"" + bean_package+"."+table.getEntityName() + "\">");	
        }else{
        	bw.write("\t<update id=\"updateById\" parameterType=\"" + bean_package+"."+table.getEntityName() + "\">");
        }
        
        bw.newLine();
        bw.write("\t\t UPDATE " + table.getTableName());
        bw.newLine();
        bw.write(" \t\t <set> ");
        bw.newLine();
 
        tempField = null;
        for( int i = 1 ; i < size ; i++ ) {
        	Column ctemp = columns.get(i);
            tempField = ctemp.getFiledName();
            bw.write("\t\t\t<if test=\"" + tempField + " != null\">");
            bw.write("" + ctemp.getColumnName() + " = #{" + tempField + "},");
            bw.write("</if>");
            bw.newLine();
        }
        bw.write(" \t\t </set>");
        bw.newLine();
        bw.write("\t\t WHERE " + columns.get(0).getColumnName() + " = #{" + columns.get(0).getFiledName() + "}");
        bw.newLine();
        bw.write("\t</update>");
        bw.newLine();
        bw.newLine();
        // update方法完毕
 
        // ----- 修改（匹配有值的字段）
//        bw.write("\t<!-- 修 改-->");
//        bw.newLine();
//        bw.write("\t<update id=\"updateByPrimaryKey\" parameterType=\"" + processResultMapId(beanName) + "\">");
//        bw.newLine();
//        bw.write("\t\t UPDATE " + tableName);
//        bw.newLine();
//        bw.write("\t\t SET ");
// 
//        bw.newLine();
//        tempField = null;
//        for( int i = 1 ; i < size ; i++ ) {
//            tempField = processField(columns.get(i));
//            bw.write("\t\t\t " + columns.get(i) + " = #{" + tempField + "}");
//            if( i != size - 1 ) {
//                bw.write(",");
//            }
//            bw.newLine();
//        }
// 
//        bw.write("\t\t WHERE " + columns.get(0) + " = #{" + processField(columns.get(0)) + "}");
//        bw.newLine();
//        bw.write("\t</update>");
//        bw.newLine();
        bw.newLine();
    }
    
    /**
     *  构建Service文件
     *
     * @throws IOException 
     */
    private void buildController(Table table) throws IOException {
        File folder = new File(controller_path);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        Column column = table.getPK();
        String PK;
        if(column != null) {
        	PK = Constant.getFirstUpper(column.getFiledName());
        }else {
        	PK = "Id";
        }
        String beanName = table.getEntityName();
        File serviceFile = new File(controller_path, table.getControllerName() + ".java");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(serviceFile), "utf-8"));
        bw.write("package " + controller_package + ";");
        bw.newLine();
        bw.write("import " + bean_package + "." + beanName + ";");
        bw.newLine();
        bw.write("import ca.hellochat.common.StringUtil;");
        bw.newLine();
        bw.write("import ca.hellochat.common.generate.Constant;");
        bw.newLine();
        bw.write("import java.util.Map;");
        bw.newLine();
        bw.write("import java.util.List;");
        bw.newLine();
        bw.write("import java.util.HashMap;");
        bw.newLine();
        bw.write("import org.slf4j.Logger;");
        bw.newLine();
        bw.write("import org.slf4j.LoggerFactory;");
        bw.newLine();
        bw.write("import org.springframework.beans.factory.annotation.Autowired;");
        bw.newLine();
        bw.write("import org.springframework.stereotype.Controller;");
        bw.newLine();
        bw.write("import org.springframework.util.StringUtils;");
        bw.newLine();
        bw.write("import org.springframework.web.bind.annotation.RequestBody;");
        bw.newLine();
        bw.write("import org.springframework.web.bind.annotation.RequestMapping;");
        bw.newLine();
        bw.write("import org.springframework.web.bind.annotation.ResponseBody;");
        bw.newLine();
        bw.write("import org.springframework.web.bind.annotation.RequestParam;");
        bw.newLine();
        bw.write("import io.swagger.annotations.ApiOperation;");
        bw.newLine();
        bw.write("import "+business_package+"."+table.getBusinessName()+";");
        bw.newLine();
        bw.write("import "+base_package+".entity.domain.Page;");
        bw.newLine();
        bw.write("import "+base_package+".controller.utils.web.BaseController;");
        bw.newLine();
        bw.write("import "+base_package+".controller.utils.web.RespData;");
        bw = buildClassComment(bw, table.getComment() + "数据接受控制器");
        bw.newLine();
        bw.write("@Controller");
        bw.newLine();
        bw.write("@RequestMapping(\"/"+Constant.getFirstLower(table.getEntityName())+"\")");
        bw.newLine();
        bw.write("public class "+table.getControllerName()+" extends BaseController{");
        bw.newLine();
        bw.write("\tprivate Logger log = LoggerFactory.getLogger(this.getClass());");
        bw.newLine();
        bw.write("\t@Autowired");
        bw.newLine();
        bw.write("\t"+table.getBusinessName()+" "+Constant.getLostFirstAndLower(table.getBusinessName())+";");
        bw.newLine();
        bw = buildMethodComments(bw, "根据id获取" + table.getComment() + "信息", "id", table.getEntityName()+"对象");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"根据id获取" + table.getComment() +"信息\" ,notes=\"获取"+ table.getEntityName() +"信息\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/info\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData info (String id){");
        bw.newLine();
        bw.write("\t\tlog.info(\"id===\"+id);");
        bw.newLine();
        bw.write("\t\tif(StringUtils.isEmpty(id)){");
        bw.newLine();
        bw.write("\t\t\treturn super.error();");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + table.getEntityName() + " " + Constant.getFirstLower(table.getEntityName()) + " = " + Constant.getLostFirstAndLower(table.getBusinessName()) + ".selectBy"+ PK +"(id);");
        bw.newLine();
        bw.write("\t\tif(" + Constant.getFirstLower(table.getEntityName()) + "==null){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error();");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\treturn success("+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "根据条件查询" + table.getComment() + "分页列表", "Map类型的条件容器", "page对象");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"根据条件" + table.getComment() +"列表 - 分页\",notes=\"获取"+ table.getEntityName() +"列表\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/list\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData list (@RequestParam Map<String,String> params){");
        bw.newLine();
        bw.write("\t\tlog.info(\"根据条件获取" + table.getComment() +"列表列表 -分页开始\"+StringUtil.mapToString(params));");
        bw.newLine();
        bw.write("\t\tPage page = new Page(params);");
        bw.newLine();
        bw.write("\t\tList<" + table.getEntityName() + "> " + Constant.getFirstLower(table.getEntityName()) + "List = " + Constant.getLostFirstAndLower(table.getBusinessName()) + ".selectPageByParam(params, page);");
        bw.newLine();
        bw.write("\t\tif(" + Constant.getFirstLower(table.getEntityName()) + "List==null || "+Constant.getFirstLower(table.getEntityName()) + "List.size()<=0){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.success();");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\tpage.setRows("+Constant.getFirstLower(table.getEntityName())+"List);");
        bw.newLine();
        bw.write("\t\treturn success(page);");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "新增" + table.getComment(), table.getComment()+"对象", table.getComment()+"对象");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"新增" + table.getComment() +"\" ,notes=\"新增"+ table.getEntityName() +"\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/add\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData add (@RequestBody "+table.getEntityName() + " " + Constant.getFirstLower(table.getEntityName())+"){");
        bw.newLine();
        bw.write("\t\tlog.info(\"新增" + table.getComment() +" " + Constant.getFirstLower(table.getEntityName()) + "\"+ "+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t\tif(" + Constant.getFirstLower(table.getEntityName()) + "==null){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error(\"对象不能为空\");");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + Constant.getLostFirstAndLower(table.getBusinessName()) + ".insertSelective("+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t\treturn success("+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "根据id修改" + table.getComment(), table.getComment()+"对象", table.getComment()+"对象");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"修改" + table.getComment() +"\" ,notes=\"修改"+ table.getEntityName() +"\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/edit\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData edit (@RequestBody "+table.getEntityName() + " " + Constant.getFirstLower(table.getEntityName())+"){");
        bw.newLine();
        bw.write("\t\tlog.info(\"id修改" + table.getComment() +" " + Constant.getFirstLower(table.getEntityName()) + "\"+" +Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t\tif(" + Constant.getFirstLower(table.getEntityName()) + "==null){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error(\"对象不能为空\");");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + Constant.getLostFirstAndLower(table.getBusinessName()) + ".updateBy"+ PK + "("+ Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t\treturn success("+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "根据条件修改" + table.getComment(), table.getComment()+"对象", table.getComment()+"对象");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"条件修改" + table.getComment() +"\" ,notes=\"修改"+ table.getEntityName() +"\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/editByParam\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData editByParam (@RequestBody "+table.getEntityName() + " " + Constant.getFirstLower(table.getEntityName())+", Map<String,String> params){");
        bw.newLine();
        bw.write("\t\tlog.info(\"条件修改" + table.getComment() +" " + Constant.getFirstLower(table.getEntityName()) + "\"+" +Constant.getFirstLower(table.getEntityName())+"+\",params=\"+params);");
        bw.newLine();
        bw.write("\t\tif(" + Constant.getFirstLower(table.getEntityName()) + "==null || params==null){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error(\"参数不能为空\");");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + Constant.getLostFirstAndLower(table.getBusinessName()) + ".updateByParam("+ Constant.getFirstLower(table.getEntityName())+", params);");
        bw.newLine();
        bw.write("\t\treturn success("+Constant.getFirstLower(table.getEntityName())+");");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "删除" + table.getComment(), table.getComment()+"对象", "成功或失败");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"删除" + table.getComment() +"\" ,notes=\"删除"+ table.getEntityName() +"\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/delete\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData delete (String id){");
        bw.newLine();
        bw.write("\t\tlog.info(\"删除" + table.getComment() +" id= \"+id);");
        bw.newLine();
        bw.write("\t\tif(StringUtils.isEmpty(id)){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error(\"id不能为空\");");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + Constant.getLostFirstAndLower(table.getBusinessName()) + ".deleteBy" + PK + "(id);");
        bw.newLine();
        bw.write("\t\treturn success(id);");
        bw.newLine();
        bw.write("\t}");
        
        bw.newLine();
        bw = buildMethodComments(bw, "根据条件批量删除" + table.getComment(), "条件", "成功或失败");
        bw.newLine();
        bw.write("\t@ApiOperation(value = \"根据条件批量删除" + table.getComment() +"\" ,notes=\"根据条件批量删除\",httpMethod=\"POST\")");
        bw.newLine();
        bw.write("\t@RequestMapping(\"/deleteByParam\")");
        bw.newLine();
        bw.write("\t@ResponseBody");
        bw.newLine();
        bw.write("\tpublic RespData deleteByParam (Map<String,String> param){");
        bw.newLine();
        bw.write("\t\tlog.info(\"删除" + table.getComment() +" param= \"+param);");
        bw.newLine();
        bw.write("\t\tif(param == null || param.size() <= 0){ ");
        bw.newLine();
        bw.write("\t\t\treturn super.error(\"参数不能为空\");");
        bw.newLine();
        bw.write("\t\t}");
        bw.newLine();
        bw.write("\t\t" + Constant.getLostFirstAndLower(table.getBusinessName()) + ".deleteByParam(param);");
        bw.newLine();
        bw.write("\t\treturn success();");
        bw.newLine();
        bw.write("\t}");
        
        bw.write("}");
        bw.newLine();
        bw.flush();
        bw.close();
    }
 
 
   
 
 
    public void generate() throws ClassNotFoundException, SQLException, IOException,Exception {
    	
    	 List<Table> tables = getTables();
    	 System.out.println("检测到表共计："+tables.size()+"个");
    	 for( Table table : tables) {
    		 System.out.print("开始生成"+table.getTableName()+"->"+table.getEntityName());
//    		 if(",col_salecompany_sum,col_sale_info,col_sale_detail,col_recycle_detail,col_recycle_info,col_tracecode_info,col_day_sum,col_recycle_detail,col_recycle_info,".contains(","+table.getTableName()+",")){
//    			 System.out.println("--goon");
//    		 }else{
//    			 System.out.println("--return");
//    			 continue;
//    		 }
//    		//创建实体类
            buildEntityBean(table);
            //创建mapper
            buildMapper(table);
            buildService(table);
            buildServiceImpl(table);
            buildBusiness(table);
            buildBusinessImpl(table);
            buildMapperXml(table);
            buildController(table);
            Thread.sleep(333);
    	 }
 
    }
    
    
    
    
    
    
    
    public static void main( String[] args ) {
        try {
        	FileAndDirOpt.createDir(base_path);
            new EntityUtilNew().generate();
        } catch( ClassNotFoundException e ) {
            e.printStackTrace();
        } catch( SQLException e ) {
            e.printStackTrace();
        } catch(Exception e ) {
            e.printStackTrace();
        }
    }
}
