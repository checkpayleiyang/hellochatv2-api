package ca.hellochat.util;

import org.apache.commons.logging.Log;


public class LogException {
	public static void logException(Exception e,Log log){
		StackTraceElement[] ste = e.getStackTrace();
		StringBuffer st = new StringBuffer(e.getClass().getName() + ":" + e.getMessage() + "\n");
		for (int i = 0; i < ste.length; i++) {
			st.append(ste[i] + "\n");
		}
		log.error(st);
	}
	
	public static void logError(Error e,Log log){
		StackTraceElement[] ste = e.getStackTrace();
		StringBuffer st = new StringBuffer(e.getClass().getName() + ":" + e.getMessage() + "\n");
		for (int i = 0; i < ste.length; i++) {
			st.append(ste[i] + "\n");
		}
		log.error(st);
	}
}