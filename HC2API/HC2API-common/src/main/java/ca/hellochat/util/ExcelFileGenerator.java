package ca.hellochat.util;


import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.util.CellRangeAddressList;


public class ExcelFileGenerator {

	private final int SPLIT_COUNT = 200*10000; //Excel每个工作簿的行数

	private ArrayList fieldName = null; //excel标题数据集

	private ArrayList fieldData = null; //excel数据内容	

	private HSSFWorkbook workBook = null;  //POI报表的核心对象
	
	
	public ExcelFileGenerator(){}

	/*public ExcelFileGenerator(ArrayList fieldName, ArrayList fieldData) {

		this.fieldName = fieldName;
		this.fieldData = fieldData;
	}*/
	
	public void ExcelData(ArrayList fieldName, ArrayList  fieldData) {

		this.fieldName = fieldName;
		this.fieldData = fieldData;
	}
	
	public void ExcelData(ArrayList fieldName) {
		this.fieldName = fieldName;
	}
	public HSSFWorkbook createWorkbook(List<ArrayList<ArrayList<String>>> fileList) {
		workBook = new HSSFWorkbook();//创建workbook对象
		int sheetNum = fileList.size();
		for (int i = 1; i < sheetNum+1; i++) {
			HSSFSheet sheet = workBook.createSheet("Page " + i);//使用wookbook对象创建sheet对象
			HSSFRow headRow = sheet.createRow((short) 0); //使用HSSFSheet对象创建row，row的下标从0开始
			for (int j = 0; j < fieldName.size(); j++) {//循环excel的标题
				HSSFCell cell = headRow.createCell((short) j);//使用HSSFRow创建cell，cell的下标从0开始
				//添加样式
				sheet.setColumnWidth((short)j, (short)6000);//设置每一列的宽度
				//创建样式
				HSSFCellStyle cellStyle = workBook.createCellStyle();
				//设置字体
				HSSFFont font = workBook.createFont();//创建字体对象
				//将字体变为粗体
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				//将字体颜色变红色
				short color = HSSFColor.RED.index;
				font.setColor(color);
				cellStyle.setFont(font);//设置之后的字体
				
				//添加样式
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);//设置单元格的类型
			//	cell.setEncoding(HSSFCell.ENCODING_UTF_16);//设置编码格式
				if(fieldName.get(j) != null){
					cell.setCellStyle(cellStyle);
					cell.setCellValue((String) fieldName.get(j));//赋值
				}else{
					cell.setCellStyle(cellStyle);
					cell.setCellValue("-");
				}
			}
			ArrayList<ArrayList<String>> fieldData=fileList.get(i-1);
			int rows = fieldData.size();
			//将数据内容放入excel单元格
			System.out.println("rows====="+rows);
			for (int n = 0; n < rows; n++) {
				HSSFRow row = sheet.createRow(n+1);//使用HSSFSheet对象创建row，row的下标从1开始
				ArrayList<String> cList=fieldData.get(n);
				for(int c=0;c<cList.size();c++){
					HSSFCell cell = row.createCell(c);//使用HSSFRow创建cell，cell的下标从0开始
					String value=cList.get(c);
					if(value==null){
						cell.setCellValue("");
					}else{
						cell.setCellValue(value);
					}
				}
			}
		}
		return workBook;
	
	}

	public HSSFWorkbook createWorkbook() {

		workBook = new HSSFWorkbook();//创建workbook对象
		int rows = fieldData.size();
		int sheetNum = 0;

		if (rows % SPLIT_COUNT == 0) {
			sheetNum = rows / SPLIT_COUNT;
		} else {
			sheetNum = rows / SPLIT_COUNT + 1;
		}

		for (int i = 1; i <= sheetNum; i++) {
			HSSFSheet sheet = workBook.createSheet("Page " + i);//使用wookbook对象创建sheet对象
			HSSFRow headRow = sheet.createRow((short) 0); //使用HSSFSheet对象创建row，row的下标从0开始
			for (int j = 0; j < fieldName.size(); j++) {//循环excel的标题
				HSSFCell cell = headRow.createCell((short) j);//使用HSSFRow创建cell，cell的下标从0开始
				//添加样式
				sheet.setColumnWidth((short)j, (short)6000);//设置每一列的宽度
				//创建样式
				HSSFCellStyle cellStyle = workBook.createCellStyle();
				//设置字体
				HSSFFont font = workBook.createFont();//创建字体对象
				//将字体变为粗体
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				//将字体颜色变红色
				short color = HSSFColor.RED.index;
				font.setColor(color);
				cellStyle.setFont(font);//设置之后的字体
				
				//添加样式
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);//设置单元格的类型
			//	cell.setEncoding(HSSFCell.ENCODING_UTF_16);//设置编码格式
				if(fieldName.get(j) != null){
					cell.setCellStyle(cellStyle);
					cell.setCellValue((String) fieldName.get(j));//赋值
				}else{
					cell.setCellStyle(cellStyle);
					cell.setCellValue("-");
				}
			}

			for (int k = 0; k < (rows < SPLIT_COUNT ? rows : SPLIT_COUNT); k++) {//分页显示数据
				if (((i - 1) * SPLIT_COUNT + k) >= rows)
					break;
				HSSFRow row = sheet.createRow((short) (k + 1));//使用HSSFSheet对象创建row，row的下标从0开始
				//将数据内容放入excel单元格
				ArrayList rowList = (ArrayList) fieldData.get((i - 1)
						* SPLIT_COUNT + k);//循环数据集
				for (int n = 0; n < rowList.size(); n++) {
					HSSFCell cell = row.createCell((short) n);;//使用HSSFRow创建cell，cell的下标从0开始
				 //	cell.setEncoding(HSSFCell.ENCODING_UTF_16);
					
					if(rowList.get(n) != null){
						cell.setCellValue((String) rowList.get(n).toString());
					}else{
						cell.setCellValue("");
					}
				}
			}
		}
		return workBook;
	}

	public void expordExcel(OutputStream os,Map parm) throws Exception {
		workBook = createWorkbook();//创建工作簿对象excel
		workBook.write(os);//将workbook对象写到输出流
		os.close();
	}
	
	public void expordExcel(OutputStream os,Map parm,List<ArrayList<ArrayList<String>>> fileList) throws Exception {
		workBook = createWorkbook(fileList);//创建工作簿对象excel
		workBook.write(os);//将workbook对象写到输出流
		os.close();
	}
	/**
	 * 下载模板
	 * Jumi 2018年7月2日 下午4:40:08
	 * @param title	模板标题
	 * @param validity	下拉框选取数据
	 * @return
	 */
	public HSSFWorkbook downloadModelExcel(List<String> title, Map<Integer, String[]> validity) {
		//创建workbook对象
		HSSFWorkbook workBook = new HSSFWorkbook();
		//创建工作薄页1
		HSSFSheet sheet = workBook.createSheet("Sheet1");
		//创建行对象
		HSSFRow row = sheet.createRow(0);
		//设置行高
		row.setHeight((short)500);
		//创建每个单元格的样式对象;
		HSSFCellStyle cellStyle = workBook.createCellStyle();
		//居中
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		//创建字体对象
		HSSFFont font = workBook.createFont();
		//将字体变为粗体
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		//将字体颜色变红色
		font.setColor(HSSFColor.RED.index);
		//字体大小
		font.setFontHeightInPoints((short)16);
		//修改样式字体
		cellStyle.setFont(font);
		if(validity != null && validity.size() > 0) {
			Set<Integer> keySet = validity.keySet();
			for(Integer index : keySet) {
				//创建下拉列表
				CellRangeAddressList addressList = new CellRangeAddressList(1, 1000, index, index);
				//创建下拉列表数据
				DataValidationConstraint dvConstraint = DVConstraint.createExplicitListConstraint(validity.get(index));
				//绑定
				DataValidation validation = new HSSFDataValidation(addressList, dvConstraint);
				validation.setShowErrorBox(false);
				sheet.addValidationData(validation);
			}
		}
		//列总数
		int lie = title.size();
		for(int i = 0; i < lie; i++) {
			//创建单元格对象
			HSSFCell cell = row.createCell(i);
			//设置单元格的类型
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			//设置每列的宽度；
			sheet.setColumnWidth(i, 6000);
			//修改单元格样式
			cell.setCellStyle(cellStyle);
			//输入单元格值
			cell.setCellValue(title.get(i));
		}
		return workBook;
	}

}
